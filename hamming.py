def hamming(n):
    assert n >= 1
    assert isinstance(n, int)

    hammings = [1]
    candidates = [
        # (value, index_in_hamming, prime)
        (2, 0, 2),
        (3, 0, 3),
        (5, 0, 5)
    ]

    while len(hammings) < n:
        candidate = min(candidates, key=lambda t:t[0])
        candidates.remove(candidate)
        value, index, prime = candidate

        if value > hammings[-1]:
            hammings.append(value)

        new_index = index + 1
        new_value = prime * hammings[new_index]

        new_candidate = (new_value, new_index, prime)
        candidates.append(new_candidate)

    return hammings

def quick(lst):
    if lst == []:
        return []

    q = {True: [], False: []}

    for item in lst[1:]:
        q[item <= lst[0]].append(item)

    return quick(q[True]) + [lst[0]] + quick(q[False])

if __name__ == '__main__':
    # h = hamming(19)
    #
    # for i in h:
    #     print(i)

    print(quick([9,49,5,3,356,243,69,2,46,7,23,5]))
