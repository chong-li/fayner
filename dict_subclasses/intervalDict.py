import math as m

class IntervalDict:
    """IntervalDict. dict like object to map intervals to values using values as keys

    Initialization requires a list, wich will be used to partition """

    def __init__(self, intervals, default_value=None, map=None):
        assert isinstance(intervals, list), 'Intervals must be a list'

        self.intervals = [-m.inf]
        self.intervals.extend( sorted(intervals) )
        self.intervals.append(m.inf)

        self._d = {k: default_value for k in range( len(intervals) + 1)}

    def __getitem__(self, val):
        pos = self.__find_pos(val)
        print(self.intervals[pos])
        return self._d[pos]

    def __setitem__(self, interval_val, new_val):
        self._d[self.__find_pos(interval_val)] = new_val
        return self

    def __find_pos(self, val):
        max_pos = len( self.intervals ) - 1
        min_pos = 0
        pos = max_pos // 2

        while max_pos - min_pos > 1:
            low, upp = self.intervals[pos], self.intervals[pos + 1]

            if low <= val:
                min_pos = pos
            else:
                max_pos = pos

            pos = (max_pos + min_pos) // 2

        return pos


if __name__ == '__main__':
    test_IntervalDict = True
    test_IntervalMapping = False

    if test_IntervalDict:
        i = IntervalDict( list(range(11)) )

        print(i._d)
        print(i.intervals)

        print(2.5, i[2.5])
        print(3.5, i[3.5])
        print(9.0, i[9.0])
        print(10.0, i[10.0])
        print(11.0, i[11.0])
        print(-1, i[-1])

    if test_IntervalMapping:
        map = {
            (0, 1.2)  :  0,
            (1.2, 3.0):  1,
            (3.0, 10) :  2,
        }

        i = IntervalDict.from_map(map)

        print(i[2])
