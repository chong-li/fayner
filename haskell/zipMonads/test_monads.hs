import Control.Monad
import Control.Applicative

data List a = Empty | Cons a (List a)

toRealList :: List a -> [a]
toRealList Empty = []
toRealList (Cons x xs) = x : toRealList xs

instance Show a => Show (List a) where
    show = show . toRealList

instance Functor List where
    fmap f ma = ma >>= (return . f)

instance Applicative List where
    pure = return

    mf <*> ma = mf >>= (func ma)
        where func = flip fmap
    -- mf <*> ma = mf >>= (func ma)
    --     where func ma f = ma >>= (return . f)
    -- mf <*> ma = mf >>= (\f -> ma >>= (return . f))
    --     where f ma f = ma >>= (return . f)
    -- mf <*> ma = mf >>= (\f -> ma >>= (return . f))
    -- mf <*> ma = mf >>= (\f -> ma >>= (\a -> return (f a)))


instance Monad List where
    return x = Cons x Empty

    Empty >>= f = Empty
    Cons x xs >>= f = listConcat (f x) (xs >>= f)


listConcat :: List a -> List a -> List a
listConcat Empty ys = ys
listConcat xs Empty = xs
listConcat (Cons x xs) ys = Cons x (listConcat xs ys)

kleisli :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
kleisli f g = \x -> f x >>= g
-- kleisli f g = (>>= g) . f

pairUp :: a -> List a
pairUp x = Cons x (Cons x Empty)

addDot :: String -> List String
addDot s = listConcat (return s) (return ".")

addStar :: String -> List String
addStar s = listConcat (return s) (return "*")

catAB = Cons (++ "A") (Cons (++ "B") Empty)
cd = Cons "c" (Cons "d" Empty)

x = catAB <*> cd
y = cd >>= addDot
z = cd >>= (addDot `kleisli` addStar)

m = cd >>= (addDot `kleisli` (addStar `kleisli` pairUp))
n = cd >>= ((addDot `kleisli` addStar) `kleisli` pairUp)