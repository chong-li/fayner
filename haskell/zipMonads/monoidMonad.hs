{-# LANGUAGE ScopedTypeVariables #-}

newtype ZippyList a = Zippy [a] deriving (Show, Eq)


-- instance Monoid a => Semigroup (ZippyList a) where
--     Zippy xs <> Zippy ys = Zippy $ zipWith (<>) xs ys

-- instance Monoid a => Monoid (ZippyList a) where
--     mempty = Zippy [mempty]

-- bind :: Monoid b => ZippyList a -> (a -> ZippyList b) -> ZippyList b
-- bind (Zippy (x:xs)) f = (f x) <> (bind (Zippy xs) f)

instance Semigroup (ZippyList a) where
    Zippy [] <> ys = ys
    xs <> Zippy [] = xs
    Zippy xs <> Zippy ys = Zippy . concat $ zipWith f xs ys
        where
            f x y = x:y:[]

instance Monoid (ZippyList a) where
    mempty = Zippy []

instance Functor ZippyList where
    fmap f ma = ma >>= (return . f)
    -- fmap f (Zippy xs) = Zippy (fmap f xs)

instance Applicative ZippyList where
    pure = Zippy . pure

    mf <*> ma = mf >>= (<$> ma)
    -- Zippy fs <*> Zippy as = Zippy $ zipWith ($) fs as

instance Monad ZippyList where
    return = pure

    Zippy (x:xs) >>= f = f x <> ((Zippy xs) >>= f)
