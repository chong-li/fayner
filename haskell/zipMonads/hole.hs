{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad


applyF :: Monad m => m (a -> b) -> m a -> m b
applyF mf ma = mf >>= \f -> fmap f ma

applyA :: Monad m => m (a -> b) -> m a -> m b
applyA mf ma = ma >>= \a -> fmap ($ a) mf



kleisli :: forall m a b. Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
kleisli f g = \a -> f a >>= g

-- a1. (return x) >>= f ==== f x
-- a2. mx >>= return ==== mx
-- a3. (mx >>= f) >>= g ==== mx >>= (\x -> f x >>= g)

-- b1. f >=> return = f
-- b2. return >=> g = g
-- b3. (f >=> g) >=> h = f >=> (g >=> h)
