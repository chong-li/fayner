Hask:
a, b, c...
a -> b
return :: a -> [a]
f :: a -> [b]
g :: b -> [c]

Kleisli:
[a], [b], [c]...

id' :: [a] -> [a]
id' = (>>= return)
id' = concat . (fmap return)

f' :: [a] -> [b]
f' = (>>= f)
f' = concat . (fmap f)

g' :: [b] -> [c]
g' = (>>= g)
g' = concat . (fmap g)

g' . f' :: [a] -> [c]
g' . f' = (>>= (g <=< f))

(<=<) :: (b -> [c]) -> (a -> [b]) -> (a -> [c])

id' <=< f' = f'
f' <=< id' = f'
(f' <=< g') <=< h' = f' <=< (g' <=< h')
