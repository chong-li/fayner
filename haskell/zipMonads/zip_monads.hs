import Control.Monad
import Control.Applicative

data List a = Empty | Cons a (List a)

toRealList :: List a -> [a]
toRealList Empty = []
toRealList (Cons x xs) = x : toRealList xs

instance Show a => Show (List a) where
    show = show . toRealList

instance Functor List where
    -- fmap f ma = pure f <*> ma
    fmap f ma = ma >>= (return . f)

instance Applicative List where
    pure = return

    -- fs <*> xs = zipWith' ($) fs xs
    -- mf <*> ma = ma >>= \a -> fmap ($ a) mf
    mf <*> ma = mf >>= \f -> fmap f ma
    -- mf <*> ma = mf >>= (<$> ma)

-- mf <*> ma = ma >>= \a -> fmap ($ a) mf
-- [(++ "A"), (++ "B")] <*> ["c", "d"]
-- fmap ($ "c") [(++ "A"), (++ "B")] -> ["cA", "cB"]

-- mf <*> ma = mf >>= \f -> fmap f ma
-- [(++ "A"), (++ "B")] <*> ["c", "d"]
-- fmap (++ "A") ["c", "d"] -> ["cA", "dA"]


instance Monad List where
    return x = Cons x Empty

    Empty >>= f = Empty
    Cons x xs >>= f = listConcat (f x) (xs >>= f) -- (f x) ++ (xs >>= f)


listConcat :: List a -> List a -> List a
listConcat Empty ys = ys
listConcat xs Empty = xs
listConcat (Cons x xs) ys = Cons x (listConcat xs ys)

zipWith' :: (a -> b -> c) -> List a -> List b -> List c
zipWith' _ Empty _ = Empty
zipWith' _ _ Empty = Empty
zipWith' f (Cons x xs) (Cons y ys) = Cons (f x y) (zipWith' f xs ys)

zipConcat :: List a -> List a -> List a
zipConcat Empty ys = ys
zipConcat xs Empty = xs
zipConcat (Cons x xs) (Cons y ys) = listConcat (Cons x (Cons y Empty)) (zipConcat xs ys)

kleisli :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
kleisli f g = \x -> f x >>= g
-- kleisli f g = (>>= g) . f

pairUp :: a -> List a
pairUp x = Cons x (Cons x Empty)

addDot :: String -> List String
addDot s = listConcat (return s) (return ".")

addStar :: String -> List String
addStar s = listConcat (return s) (return "*")

catAB = Cons (++ "A") (Cons (++ "B") Empty) -- [(++ "A"), (++ "B")]
cd = Cons "c" (Cons "d" Empty) -- ["c", "d"]
ef = Cons "e" (Cons "f" Empty)

x = catAB <*> cd
y = cd >>= addDot
z = cd >>= (addDot `kleisli` addStar)

m = cd >>= (addDot `kleisli` (addStar `kleisli` pairUp))
n = cd >>= ((addDot `kleisli` addStar) `kleisli` pairUp)