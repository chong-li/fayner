# Blackbird

Bom, vamos tentar entender a bizarrice que é o operador Blackbird.
Ele consegue fazer a composição de funções onde a função que aplica primeiro tem mais de 1 parâmetro.
Pra não ficar muito em lenga lenga, ele é definido assim:

```haskell
(...) :: (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c
(...) = (.) (.) (.)
```

Ou seja, é a composição da função que compões funções com a função que compões funções.

Sim, é isso, mesmo.
É confuso, vamos com calma.

Indo da esquerda pra direita na assinatura da função, vemos que o primeiro parâmetro é uma função de apenas 1 parâmetro.
O segundo parâmetro é uma função de 2 parâmetros.

Agora observe que, ao aplicarmos parcialmente 2 funções com essas assinaturas obtemos uma função de 2 parâmetros.
Além disso, os parâmetros são os mesmos da segunda função e o "retorno" é do mesmo tipo da primeira função.

Então esse cara compões uma função de 1 parâmetro com uma função de 2 parâmetros!

## Começando pela composição

Vamos rever a assinatura da composição simples:

```haskell
(.) :: (b -> c) -> (a -> b) -> a -> c
```
Depois de entender a assinatura da `(...)` essa aí é moleza.

No dia a dia do Haskell é ótimo que essa função seja infixa.
Pro nosso entendimento desse operador esquisito prejudica.
Então vamos usar o truque de fazer um alias pra ela.

Aliás, vamos fazer 3.
Como o operador é uma aplicação dessa função com ela mesma 2 vezes, será útil
utilizar 3 assinaturas e nomes distintos pra evitar confusões.

```haskell
dot = (.)   :: (b -> c) -> (a -> b) -> a -> c
dot_0 = dot :: (l -> m) -> (k -> l) -> k -> m
dot_1 = dot :: (y -> z) -> (x -> y) -> x -> z
```

f :: a -> b -> c
f g :: b -> c

Daí queremos descobrir a assinatura de `dot (dot_0) (dot_1)`.

## Realizando as aplicações

Comecemos pela aplicação parcial de apenas uma das funções dot.

```haskell
dot dot_0 :: (a -> b) -> a -> c
```

Como já fizemos uma aplicação, a função que existe como primeiro parâmetro é reduzida da assinatura.
Mas isso força certos tipos em `dot_0` pra que a aplicação possa ocorrer.
Em particular, precisamos que
```haskell
b -> c = (l -> m) -> (k -> l) -> k -> m
```

Portanto, devem valer:
```haskell
b = l -> m
c = (k -> l) -> k -> m
```

Daí, conseguimos rescrever a assinatura de `dot dot_0` com base nos novos tipos.
Temos:
```haskell
dot dot_0 :: (a -> l -> m) -> a -> (k -> l) -> k -> m
```

Eu diria que "Tava bom, tava ruim, mas agora parece que piorou".

Vamos seguir com a próxima aplicação.
Novamente, teremos indução de tipos pra que possamos aplicar `dot_1` na expressão.

Em particular,
```haskell
a -> l -> m = (y -> z) -> (x -> y) -> x -> z
```
Portanto, precisam valer:
```haskell
a = y -> z
l = x -> y
m = x -> z
```

E, mais uma vez, rescrevemos a assinatura com base nas novas restrições de tipos:
```haskell
dot dot_0 dot_1 :: (y -> z) -> (k -> x -> y) -> k -> x -> z
```

Para fácil comparação, as assinaturas juntas:
```haskell
dot dot_0 dot_1 :: (y -> z) -> (k  -> x  -> y) -> k  -> x  -> z
(.)  (.)   (.)  :: (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c
```

# Encontrando o BlackBird na natureza

Vamos começar definindo uma função meio besta que inverte os primeiros k elementos de uma lista

```haskell
revFstK :: (Num a) => Int -> [a] -> a
revFstK k xs = reverse $ take k xs

-- revFstK 3 [8, 4, 2, 1, 9] == [2, 4, 8]
```

Note que aqui estamos apenas passando argumentos pra função take e passando seu resultado pra reverse.
Parece um caso clássico de composição.
Porém, nesse caso a natureza de currying do haskell está confundindo nossa intuição.
Na verdade esse caso não é um caso de composição.
Vamos observar as assinaturas das funções `reverse` e `take`.

```haskell
reverse :: [a] -> [a]
take :: Int -> [a] -> [a]
```

Continua parecendo que deveria compor.
Porém, lembre-se do currying, na verdade `take` é uma função de apenas 1 parâmetro que resulta numa outra função, no caso

```haskell
take :: Int -> ([a] -> [a])
```

Como `([a] -> [a])` não é o mesmo que `[a]`, não podemos fazer a composição.
Por outro lado, observe que a assinatura da função resultado é exatamente compatível com `reverse`.
Então podemos fazer a aplicação parcial da `take` para conseguir fazer a composição 

```haskell
-- definições equivalentes

revFstK k xs = reverse $ take k xs
revFstK k = reverse . (take k)
```

Também podemos passar o operador de composição para a forma prefixa.
E lembremos que as aplicações acontecem da esquerda pra direita, então sempre podemos adicionar parênteses dessa forma

```haskell
-- definições equivalentes

revFstK k xs = reverse $ take k xs
revFstK k = reverse . (take k)

revFstK k = (.) reverse (take k)
revFstK k = ((.) reverse) (take k)
```

Uhm, tem algo de curioso nessa última forma.
Vamos abstrair momentaneamente a primeira função dessa última forma.
Considerando `dotRev = (.) reverse` a última forma fica como

```haskell
dotRev = (.) reverse
revFstK k = dotRev (take k)
```

Mas é exatamente isso que uma composição abstrai!

```haskell
(f . g) x = f (g x)
(dotRev . take) k = dotRev (take k)
```

Bom, aí quem consegue resistir, não é mesmo!?

```haskell
-- definições equivalentes

revFstK k xs = reverse $ take k xs
revFstK k = reverse . (take k)

revFstK k = (.) reverse (take k)
revFstK k = ((.) reverse) (take k)

revFstK k = (((.) reverse) . take) k
```

Bom, mas agora não precisamos mais do k na definição!
Ah, e já que estamos aqui, podemos colocar esse outro operador na forma prefixa.

```haskell
-- definições equivalentes

revFstK k xs = reverse $ take k xs
revFstK k = reverse . (take k)

revFstK k = (.) reverse (take k)
revFstK k = ((.) reverse) (take k)

revFstK k = (((.) reverse) . take) k

revFstK = ((.) reverse) . take
revFstK = (.) ((.) reverse) take
```

Ah, olha só!
Lembrando que as aplicações são da esquerda pra direita encontramos, mais uma vez, a situação da composição!

```haskell
f (f x) = (f . f) x
(.) ((.) reverse) = ((.) . (.)) reverse
```

E aí podemos colocar todas as composições na forma prefixa

```haskell
-- definições equivalentes

revFstK k xs = reverse $ take k xs
revFstK k = reverse . (take k)

revFstK k = (.) reverse (take k)
revFstK k = ((.) reverse) (take k)

revFstK k = (((.) reverse) . take) k

revFstK = ((.) reverse) . take
revFstK = (.) ((.) reverse) take

revFstK = ((.) . (.)) reverse take
revFstK = (.) (.) (.) reverse take
```

```haskell
(f . g) x = f (g x)
(.) f g x = f (g x)

(.) (.) f x g = (f x) . g
(.) (.) f x g y = (f x) . g = (f x) (g y) = f x $ g y

(.) . (.) f g x y = f (g x y)

mapInts = map ::  (Int->Int)->[Int]->[Int]

a'''''' f is = sum $ mapInts f is
a''''' f = sum . (mapInts f)
a'''' f = ((.) sum) (mapInts f)
a''' f = (((.) sum) . mapInts) f
a'' = ((.) sum) . mapInts
a' = (.) ((.) sum) mapInts
a = ((.).(.)) sum mapInts
totalRecall = (.) (.) (.) sum mapInts

a (+2) [10,10,10]

(.) (.) :: (a -> l -> m) -> a -> (k -> l) -> k -> m

g :: k -> l
f :: a-> l -> m
x :: a
(.) (.) f x g :: k -> m
((.) . f) x g
((.) (f x)) g
(f x) . g
```

```haskell
f :: d -> e
g :: a -> b -> c -> d

ga :: b -> c -> d
ga = g a

... = (.) (.) (.)

(...) f ga =
    (...) f (g a) =
    ((...) f) (g a) =
    (((...) f) . g) a

(.) ((...) f) g =
    ((...) f)) g

(.) ((...) f) =
    (.) (((...)) f) =
    (.) ((...) f) =
    ((.) . (...)) f

(.) (.) (...)
```
