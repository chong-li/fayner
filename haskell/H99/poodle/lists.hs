-- list.hs
module Lists where

listComprehension = [(x, y, (fromInteger x)/(fromInteger y)) | x <- [0..10], y <- [0..10], y > 0, (mod x y) /= 0]

-- H99 01-28

-- 01 - last element (== last)
pLast :: [a] -> a
pLast [] = errorWithoutStackTrace "Empty list \129488"
pLast [x] = x
pLast (x:xs) = pLast xs

-- 02 - last but one
penult :: [a] -> a
penult [] = errorWithoutStackTrace "Empty list \129488"
penult [x] = errorWithoutStackTrace "Only one element \129488"
penult [x0,x1] = x0
penult (x:xs) = penult xs

-- 03 - n'th element (~ (!!))
th :: Int -> [a] -> a
th _ [] = errorWithoutStackTrace "Out of range \129488"
th 0 (x:xs) = x
th n (x:xs) = (n-1)`th` xs

-- 04 - number of elements (== length)
pLength :: [a] -> Int
pLength [] = 0
pLength (x:xs) = 1 + pLength xs

-- 05 - reverse list (== reverse)
pReverse :: [a] -> [a]
pReverse xs = r xs []
  where r [] ls = ls
        r (e:es) ls = r es (e:ls)

-- 06 - check if is palindrome
isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs = xs == pReverse xs

-- 07 - flatten nested list
data NestedList a = El a | NList [NestedList a]

flatten :: NestedList a -> [a]
flatten (NList []) = []
flatten (El x) = [x]
flatten (NList (n:ns)) = (flatten n)++(flatten (NList ns))

-- 08 - remove consecutive duplicates
squeeze :: Eq a => [a] -> [a]
squeeze (x0:x1:xs)
  | x0 == x1 = squeeze (x1:xs)
  | otherwise = x0: squeeze (x1:xs)
squeeze xs = xs

-- 09 - pack consecutive duplicates into sublists
pack :: Eq a => [a] -> [[a]]
pack [] = []
pack [x] = [[x]]
pack (x0:x1:xs)
  | x0 == x1 = let (ys:yss) = pack (x1:xs) in (x0:ys):yss
  | otherwise = [x0]:(pack (x1:xs))

-- 10 - run-length encoding. Ex.: runLength "aabbba" -> [(2,'a'),(3,'b'),(1,'a')]
rLEncode :: Eq a => [a] -> [(Int, a)]
rLEncode [] = []
rLEncode [x] = [(1, x)]
rLEncode (x0:x1:xs)
  | x0 == x1 = let ((n, _):ys) = rLEncode (x1:xs) in (n+1, x0):ys
  | otherwise = (1, x0):(rLEncode (x1:xs))

-- 11 e 13 - modified run-length. Ex.: rLEncode' "aabbba" -> [Multiple 2 'a', Multiple 3 'b', Single 'a']
-- em Prolog seria ((2 A), (3 B), A), o que faz muito mais sentido

-- 12 decode run-length encoding
rLDecode :: Eq a => [(Int, a)] -> [a]
rLDecode [] = []
rLDecode ((n, e):ts) = (rep e n)(rLDecode ts)
  where rep y i
          | i == 1 = (y:)
          | i > 1 = (y:).(rep y (i-1))
          | otherwise = id

-- 14 - duplicate elements of list
duplicate :: [a] -> [a]
duplicate [] = []
duplicate (x:xs) = x:x:(duplicate xs)

-- 15 - replicate elements of list n times
pReplicate :: [a] -> Int -> [a]
pReplicate _ 0 = []
pReplicate xs n = f xs n n
  where f [] _ _ = []
        f (y:ys) m i
          | i  > 1 = y:(f (y:ys) m (i-1))
          | otherwise = y:(f ys m m)

-- 16 - drop every nth element from list
pin :: [a] -> Int -> [a]
pin _ 0 = []
pin xs n = p xs n n
  where p [] _ _ = []
        p (y:ys) n i
          | i == 0 = p ys n n
          | otherwise = y:(p ys n (i-1))

-- 17 - split list before element in position n (~ splitAt)
splitBefore :: [a] -> Int -> ([a],[a])
splitBefore [] _ = ([],[])
splitBefore xs 0 = ([], xs)
splitBefore (x:xs) n = let (l1s,l2s) = splitBefore xs (n-1) in (x:l1s,l2s)

-- 18 - elements of list from i to j
slice :: [a] -> Int -> Int -> [a]
slice [] _ _ = []
slice (x:xs) i j
  | j < 0 = []
  | i > 0 = (slice xs (i-1) (j-1))
  | otherwise = x:(slice xs (i) (j-1))

-- 19 - rotate list n places to left
rotateL :: [a] -> Int -> [a]
rotateL [] _ = []
rotateL xs n = let m = mod n (length xs)
                   (l1,l2) = splitBefore xs m
               in l2++l1

-- 20 - return element in position k and the list without it
pickAt :: Int -> [a] -> (a, [a])
pickAt k ys
  | k < 0 || k >= length ys = errorWithoutStackTrace "Out of range \129488"
  | otherwise = p k ys
  where p 0 (x:xs) = (x, xs)
        p i (x:xs) = let (e, ls) = p (i-1) xs in (e, x:ls)

-- 21 - insert element in list at position i
insertAt :: a -> [a]-> Int -> [a]
insertAt x [] _ = [x]
insertAt x (x0:xs) i
  | i <= 0 = x:x0:xs
  | otherwise = x0:(insertAt x xs (i-1))

-- 22 - range from x to y
range :: (Ord a, Enum a) => a -> a -> [a]
range x y
  | x == y = [x]
  | x > y  = x:(range (pred x) y)
  | x < y  = x:(range (succ x) y)
-- range from x to y 
--range :: Enum a => a -> a -> [a]
--range x y
--  | i == j = [x]
--  | i > j  = x:(range (pred x) y)
--  | i < j  = x:(range (succ x) y)
--  where i = fromEnum x
--        j = fromEnum y

-- auxiliary function for 23 - 25: generate a list of pseudo-random numbers, based on Blum Blum Shub
pRnd :: Integer -> [Integer]
pRnd s
  | mod seed p == 0 || mod seed q == 0 = r ((seed + 1) * 11311)
  | mod seed p == 1 || mod seed q == 1 = r (seed * 31415)
  | otherwise = let r0:r1:r2:r3:rs = r seed in rs
  where
   seed = abs s
   p = 2879
   q = 2903
   m = p*q
   r x0 = let x1 = mod (x0^2) m in x1:(r x1)

-- 23 - n randomly selected elements from list. Ex.: rndSelect 3 [0,-963,44,25,753,-8] 131 -> [25,0,-963]
rndSelect :: Integer -> [a] -> Integer -> [a]
rndSelect n xs seed = let rnds = pRnd seed
                          len = length xs
                      in rSel n xs len rnds
  where rSel _ [] _ _ = []
        rSel 0 _ _ _ = []
        rSel n xs l (r:rs) = let (e, es) = pickAt (mod (fromInteger r) l) xs in e:(rSel (n-1) es (l-1) rs)

-- 24 - n randomly selected numbers from [1..m]. Ex.: draw 3 50 131 -> [36,1,29]
draw :: Integer -> Integer -> Integer -> [Integer]
draw n m seed = let rnds = pRnd seed
                    n1 = if n > m then m else n
                in dw n1 m rnds []
  where dw 0 _ _ _ = []
        dw n m (r:rs) es = let e = (mod r m) + 1 in if elem e es then dw n m rs es else e:(dw (n-1) m rs (e:es))

-- 25 - random permutation of list
permute :: [a] -> Integer -> [a]
permute xs seed = pmt xs rnds 1
  where rnds = pRnd seed
        pmt [] _ _ = []
        pmt (x:xs) (r:rs) i = insertAt x (pmt xs rs (i+1)) (fromInteger (mod r i))

-- 26 - all combinations of n elements from list
combinations :: Int -> [a] -> [[a]]
combinations n xs
  | n < 0 || n > length xs = []
  | otherwise = comb n xs
  where comb 0 _ = [[]]
        comb n (y:ys) = (map (y:) (comb (n-1) ys)) ++ (combinations n ys)

-- 27 - all groupings of elements of a set into disjoint subets of specified sizes
-- Ex.: group ["Alice","Bob","Pépe"] [2,1] -> [[["Alice","Bob"],["Pépe"]], [["Alice","Pépe"],["Bob"]], [["Bob","Pépe"],["Alice"]]]
--
-- Ainda não funciona!
group :: [a] -> [Int] -> [[[a]]]
group xs ns
  | ns == [] || any (< 0) ns || sum ns > length xs = []
  | otherwise = gp xs [] ns
  where gp _ _ [] = [[]]
        gp xs us (0:ns) = map ([]:) (gp (us++xs) [] ns)
        gp (x:xs) us (n:ns) = map (\(es:ess) -> (x:es):ess) (gp xs us ((n-1:ns))) ++ (group' xs (us++[x]) (n:ns))
          where group' xs us (n:ns)
                  | n > length xs = []
                  | otherwise = gp xs us ns

-- auxiliary function for 28: quick sort list of tuples (key, element) by key
quickDecorated :: Ord a => [(a,b)] -> [(a,b)]
quickDecorated [] = []
quickDecorated (t:ts) = let (ls, gs) = part ts (fst t)
                        in (quickDecorated ls) ++ (t:(quickDecorated gs))
                        where
                          -- partitioning
                          part :: Ord a => [(a,b)] -> a -> ([(a,b)],[(a,b)])
                          part [] _ = ([],[])
                          part (y:ys) k
                            | (fst y) < k = (y:lts, gts)
                            | otherwise = (lts, y:gts)
                            where (lts, gts) = part ys k

-- 28-a - sort list of lists by length
lsort :: [[a]] -> [[a]]
lsort [] = []
lsort xss = let ts = [(length xs, xs) | xs <- xss] in [rs| (_,rs) <- (quickDecorated ts)]

-- 28-b - sort list of lists by length frequency
lfsort :: [[a]] -> [[a]]
lfsort [] = []
lfsort xss = let ts = [(length xs, xs) | xs <- xss]
                 fs = freq (quickDecorated ts) 1
             in [rs | (_,rs) <- (quickDecorated fs)]
             where
               freq :: [(Int,a)] -> Int -> [(Int, a)]
               freq [] _ =  []
               freq [(_,y)] i = [(i, y)]
               freq ((k0, y0):(k1, y1):ts) i
                 | k0 == k1 = let (k,_):ls = freq ((k1, y1):ts) (i+1) in (k, y0):(k, y1):ls
                 | otherwise = (i, y0):(freq ((k1, y1):ts) 1)


-- Outros

-- split list with breaker value. Ex.: splitWith "Hello world!" ' ' -> ["Hello","world!"]
splitWith :: Eq a => [a] -> a -> [[a]]
splitWith [] _ = []
splitWith [e] b = if e == b then [] else [[e]]
splitWith (e0:e1:es) b
  | e0 == b   = (splitWith (e1:es) b)
  | e1 == b   = [e0]:(splitWith es b)
  | otherwise = let (l:ls) = splitWith (e1:es) b in (e0:l):ls

-- remove duplicate values
rmCopies :: (Eq a) => [a] -> [a]
rmCopies [] = []
rmCopies (x:xs) = x:(rmCopies (filter (/=x) xs))
