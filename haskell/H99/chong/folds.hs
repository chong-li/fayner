-- Haskell from fst principles

module GreatModules where

(...) = (.) (.) (.)

foldAnd :: [Bool] -> Bool
foldAnd = foldr (&&) True

foldOr :: [Bool] -> Bool
foldOr = foldr (||) False

-- Esse jeito pode curto-circuitar?
-- lembrar de testar com o tal do :sprint
-- |
-- >>> foldAny even [1, 3, 5]
-- False
-- >>> foldAny odd [1, 3, 5]
-- True
foldAny :: (a -> Bool) -> [a] -> Bool
foldAny = foldOr ... map

-- elem com fold sem usar o foldAny
-- |
-- >>> foldElem 4 [1..6]
-- True
-- >>> foldElem 9 [1..6]
-- False
foldElem :: Eq a => a -> [a] -> Bool
foldElem x xs = foldr (f x) False xs
    where   f :: Eq a => a -> (a -> Bool -> Bool)
            f _ _ True = True
            f x y False = x == y


-- elem com fold usando any
-- |
-- >>> foldAnyElem 4 [1..6]
-- True
-- >>> foldAnyElem 9 [1..6]
-- False
foldAnyElem :: Eq a => a -> [a] -> Bool
foldAnyElem = foldAny . (==)

-- |
-- >>> foldReverse [1..5] == reverse [1..5]
-- True
foldReverse :: [a] -> [a]
foldReverse = foldl (flip (:)) []

-- |
-- >>> (mapWithFold (+1) [1..5]) == ((+1) <$> [1..5])
-- True
mapWithFold :: (a -> b) -> [a] -> [b]
mapWithFold f = foldr ((:) . f) []

-- Daqui pra frente eu fui fazendo com where pra função que passa pro fold
-- fiz isso pra usar os tipos pra me guiar
-- depois vou querer tirar pra ficar mais fácil de ler o código

-- |
-- >>> foldFilter even (foldFilter even [1..10]) == foldFilter even [1..10]
-- True
foldFilter :: (a -> Bool) -> [a] -> [a]
foldFilter f = foldr (aux f) []
    where   aux :: (a -> Bool) -> a -> [a] -> [a]
            aux f x acc = if f x
                          then x : acc
                          else acc

-- |
-- >>> foldSquish [[1,2,3],[4,5,6],[7,8,9]]
-- [1,2,3,4,5,6,7,8,9]
foldSquish :: [[a]] -> [a]
foldSquish = foldr aux []
    where aux :: ([a] -> [a] -> [a])
          aux = (++)


-- squishMap == squish ... map
-- |
-- >>> foldSquishMap (: []) [1..9]
-- [1,2,3,4,5,6,7,8,9]
foldSquishMap :: (a -> [b]) -> [a] -> [b]
foldSquishMap f as = foldr (aux f) [] as
    where aux :: (a -> [b]) -> a -> [b] -> [b]
          aux f a bs = (f a) ++ bs

-- |
-- >>> squishAgain [[x] | x <- [1..9]]
-- [1,2,3,4,5,6,7,8,9]
squishAgain :: [[a]] -> [a]
squishAgain = foldSquishMap aux
    where aux :: [a] -> [a]
          aux = id

-- |
-- >>> foldMaximumBy compare [1..10]
-- 10
foldMaximumBy :: (a -> a -> Ordering) -> [a] -> a
foldMaximumBy compareFunc (a:as) = foldl (isGT compareFunc) a as
    where isGT :: (a -> a -> Ordering) -> a -> a -> a
          isGT f x y = if f x y == GT
                       then x
                       else y


------------------------------------------
-- 99 problems ---------------------------
------------------------------------------

-- 0: last element of list
-- not safe. Couldn't decide return value for empty list
-- |
-- >>> lastElem [1..10]
-- 10
lastElem :: [a] -> a
lastElem [a] = a
lastElem (a:as) = foldl aux a as
    where aux :: a -> a -> a
          aux _ = id

-- |
-- >>> lastElem' [1..10]
-- 10
lastElem' :: [a] -> a
lastElem' [a] = a
lastElem' as = foldl1 (snd ... (,)) as

-- 1: penultimate element of list
-- Not safe! Will crash if length list < 2
-- |
-- >>> penultimate [1..10]
-- 9
penultimate :: [a] -> a
penultimate = head . (foldr aux [])
    where aux :: a -> [a] -> [a]
          aux x [] = [x]
          aux x [y] = [x,y]
          aux _ ys = ys

-- 2: find element in k-th position
-- fst position is position 0

data CountDown a = Value a | Down Int

giveValue :: CountDown a -> a
giveValue (Value x) = x
-- ? giveValue _ = error "No Value"

-- Not Safe. Will crash if k not in [0, length xs[ interval
-- |
-- >>> kthElem [1..10] 5
-- 6
-- >>> kthElem [1..10] 0
-- 1
-- >>> kthElem [1..10] 9
-- 10
kthElem :: [a] -> Int -> a
kthElem xs n = giveValue $ foldl aux (Down n) xs
    where aux :: CountDown a -> a -> CountDown a
          aux (Down 0) x = Value x
          aux (Down n) _ = Down (n - 1)
          --aux (Value x) _ = Value x
          aux x _ = x --(test this and original with :sprint)

-- |
-- >>> kthElem2 [1..10] 5
-- 6
-- >>> kthElem2 [1..10] 0
-- 1
-- >>> kthElem2 [1..10] 9
-- 10
kthElem2 :: [a] -> Int -> a
kthElem2 xs k = head $ foldr (const tail) xs [1..k]
    --where aux :: Int -> [a] -> [a]onst
      --    aux _ = tail

-- 3: implement a length function for lists
-- |
-- >>> length' [1..10]
-- 10
length' :: [a] -> Int
length' = foldl ((+1) ... const) 0

-- 4: reverse a list
reverse' :: [a] -> [a]
reverse' = foldReverse

-- 5: test lists for palindromality
-- Can't see it with a single fold
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome = undefined

-- 6.a: flatten list of lists
flatten' :: [[a]] -> [a]
flatten' = foldr (++) []

-- 7: eliminate consecutive duplicates of list elements
-- |
-- >>> compress [1,1,1,2,2,3,4,5,5]
-- [1,2,3,4,5]
compress :: (Eq a) => [a] -> [a]
compress = foldr (aux) []
    where aux :: (Eq a) => a -> [a] -> [a]
          aux x [] = x : []
          aux x ys = if x == head ys
                     then ys
                     else x : ys

-- 8: pack duplicates in sublists
-- |
-- >>> pack [1,1,1,2,2,3,4,5,5]
-- [[1,1,1],[2,2],[3],[4],[5,5]]
pack :: (Eq a) => [a] -> [[a]]
pack = foldr (aux) []
    where aux :: (Eq a) => a -> [[a]] -> [[a]]
          aux x [] = [[x]]
          aux x (y:ys) = if x == head y
                         then (x : y) : ys
                         else [x] : y : ys

-- 9: run-length-encode. Compress list into pairs of (elem, run_count)
-- |
-- >>> encode [1,1,1,2,2,3,4,5,5]
-- [(1,3),(2,2),(3,1),(4,1),(5,2)]
encode :: (Eq a) => [a] -> [(a, Int)]
encode = foldr (aux) []
    where aux :: (Eq a) => a -> [(a, Int)] -> [(a, Int)]
          aux x [] = [(x, 1)]
          aux x (y:ys) = if x == fst y
                         then (x, 1 + snd y) : ys
                         else (x, 1) : y : ys

-- 10: run-length-encode. Keep length 1 entries as is
---- Mixed type warning. Needs custom type
data Encoded a = Multiple Int a | Single a deriving Show

toEncode :: (a, Int) -> Encoded a
toEncode (a, 1) = Single a
toEncode (a, n) = Multiple n a

-- |
-- >>> encode2 [1,1,1,2,2,3,4,5,5]
-- [Multiple 3 1,Multiple 2 2,Single 3,Single 4,Multiple 2 5]
encode2 :: (Eq a) => [a] -> [Encoded a]
encode2 = foldr (aux) []
    where aux :: (Eq a) => a -> [Encoded a] -> [Encoded a]
          aux x [] = [Single x]
          aux x (Single y : ys) = if x == y
                                  then (Multiple 2 y) : ys
                                  else (Single x) : (Single y) : ys
          aux x (Multiple n y : ys) = if x == y
                                      then (Multiple (n+1) y) : ys
                                      else (Single x) : (Multiple n y) : ys

-- 11: Decode. Inverse of encode2
-- |
-- >>> decode [Multiple 3 1,Multiple 2 2,Single 3,Single 4,Multiple 2 5]
-- [1,1,1,2,2,3,4,5,5]
decode :: (Eq a) => [Encoded a] -> [a]
decode = foldr aux []
    where aux :: (Eq a) => Encoded a -> [a] -> [a]
          aux (Single x) ys = x : ys
          aux (Multiple 2 x) ys = aux (Single x) (x : ys)
          aux (Multiple n x) ys = aux (Multiple (n-1) x) (x : ys)

-- 13: Duplicate elements of a list
-- |
-- >>> duplicate [1..3]
-- [1,1,2,2,3,3]
duplicate :: [a] -> [a]
duplicate = foldr aux []
    where aux :: a -> [a] -> [a]
          aux x ys = x : x : ys

-- 14: Replicate each element of a list
-- |
-- >>> replicateK 3 [1..3]
-- [1,1,1,2,2,2,3,3,3]
replicateK :: Int -> [a] -> [a]
replicateK k = foldr (aux k) []
    where aux :: Int -> a -> [a] -> [a]
          aux k x ys = (take k $ repeat x) ++ ys

-- 15: Drop every n-th element from list
data ValCounter a = Val a Int

getVal :: ValCounter a -> a
getVal (Val x _) = x

-- |
-- >>> dropKth 3 [1..5]
-- [1,2,3,5]
dropKth :: Int -> [a] -> [a]
dropKth k xs = getVal $ foldl (aux k) (Val [] k) xs
    where aux :: Int -> ValCounter [a] -> a -> ValCounter [a]
          aux k (Val ys 0) x = Val ys k
          aux k (Val ys n) x = Val (ys ++ [x]) (n-1)

-- |
-- >>> dropKth2 3 [1..5]
-- [1,2,3,5]
dropKth2 :: Int -> [a] -> [a]
dropKth2 k xs = fst $ foldr aux ([], xs) [0..k]
    where aux :: Int -> ([a], [a]) -> ([a], [a])
          aux _ (xs, []) = (xs, [])
          aux i (xs, y:ys) = if i > 0
                            then (xs ++ [y], ys)
                            else (xs ++ ys, [])

-- 16: split list in 2. split on index given
-- |
-- >>> splitOn 3 [1..10]
-- ([1,2,3],[4,5,6,7,8,9,10])
splitOn :: Int -> [a] -> ([a], [a])
splitOn k xs = getVal $ foldl aux (Val ([], []) k) xs
    where aux :: ValCounter ([a], [a]) -> a -> ValCounter ([a], [a])
          aux (Val (ls, rs) n) x
            | n > 0     = Val (ls ++ [x], rs) (n-1)
            | otherwise = Val (ls, rs ++ [x]) (n-1)

-- |
-- >>> splitOn2 3 [1..10]
-- ([1,2,3],[4,5,6,7,8,9,10])
-- >>> splitOn2 0 [1,2,3,4]
-- ([],[1,2,3,4])
-- >>> splitOn 10 [1,2,3,4]
-- ([1,2,3,4],[])
splitOn2 :: Int -> [a] -> ([a], [a]) --splitOn2 0 xs = ([], xs)
splitOn2 k xs = foldr aux ([], xs) [1..k]
    where aux :: Int -> ([a], [a]) -> ([a], [a])
          aux _ (xs, []) = (xs, [])
          aux _ (xs, y:ys) = (xs ++ [y], ys)

-- 17: slice a list
-- |
-- >>> slice2 [1..9] 0 3
-- [1,2,3,4]
-- >>> slice2 [1..9] 6 8
-- [7,8,9]
slice2 :: [a] -> Int -> Int -> [a]
slice2 xs low hig = getVal $ foldl (aux low hig) (Val [] 0) xs
    where aux :: Int -> Int -> ValCounter [a] -> a -> ValCounter [a]
          aux l h (Val ys n) x
            | l <= n && n <= h = Val (ys ++ [x]) (n+1)
            | otherwise        = Val ys (n+1)

-- 18: rotate list n places to the left
-- |
-- >>> rotList 3 [1..9]
-- [4,5,6,7,8,9,1,2,3]
rotList :: Int -> [a] -> [a]
rotList _ [] = []
rotList k xs = foldr aux xs [1..k]
    where aux :: Int -> [a] -> [a]
          aux _ (x:xs) = (xs ++ [x])


-- 19: extract element from list at index n
-- |
-- >>> removeAt 3 [1..9]
-- (4,[1,2,3,5,6,7,8,9])
removeAt :: Int -> [a] -> (a, [a])
removeAt n xs = valueGetter $ foldl aux (Val Nothing n) xs
    where aux :: ValCounter (Maybe (a, [a])) -> a -> ValCounter (Maybe (a, [a]))
          aux (Val Nothing n) x = Val (Just (x, [])) (n-1) 
          aux (Val (Just (y, ys)) n) x
            | n >= 0 = Val (Just (x, ys ++ [y])) (n-1)
            | n <  0 = Val (Just (y, ys ++ [x])) n
          valueGetter :: ValCounter (Maybe (a, [a])) -> (a, [a])
          valueGetter (Val (Just x) _) = x

-- |
-- >>> cleanExtractAt 3 [1..9]
-- (4,[1,2,3,5,6,7,8,9])
cleanExtractAt  :: Int -> [a] -> (a, [a])
cleanExtractAt k xs
    | k < 0     = error "Negative index"
    | otherwise = tiddy $ foldr aux ([], xs) [0..(k-1)]
    where aux _ (_, [])    = error "length list < index"
          aux _ (xs, y:ys) = (xs ++ [y], ys)
          tiddy (xs, [])   = error "length list == index"
          tiddy (xs, y:ys) = (y, xs ++ ys)

-- 20: insert element x in position n of a list
-- |
-- >>> insertAt 101 [1..9] 3
-- [1,2,3,101,4,5,6,7,8,9]
insertAt :: a -> [a] -> Int -> [a]
insertAt x ys k = (snd . getVal) $ foldl aux (Val (x, []) k) ys
    where aux :: ValCounter (a, [a]) -> a -> ValCounter (a, [a])
          aux (Val (x, xs) n) y
            | n == 0    = Val (x, xs ++ [x, y]) (-1)
            | otherwise = Val (x, xs ++ [y]) (n-1)

-- |
-- >>> insertAt2 101 [1..9] 3
-- [1,2,3,101,4,5,6,7,8,9]
insertAt2 :: a -> [a] -> Int -> [a]
insertAt2 x ys k = fst $ foldr (aux x) ([], ys) [0..k]
    where aux :: a -> Int -> ([a], [a]) -> ([a], [a])
          aux _ _ (zs, []) = (zs, [])
          aux x 0 (zs, ys) = (zs ++ [x] ++ ys, [])
          aux x k (zs, y:ys) = (zs ++ [y], ys)

-- 21: implement enumFromTo. Keep it polimorphic to Enum

-- 25: generate the list of all combinations of size k from list xs

-- For now, I'm assuming list is devoid of duplicates or duplicates aren't "identical"
-- Can't see it with a single fold
combinations' :: Int -> [a] -> [[a]]
combinations' = undefined

-- 26: get the disjoint subsets

-- partitions a list on given indices. Results in a list of the partitions 

