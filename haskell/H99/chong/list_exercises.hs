-- returns True if given value is in given list
elemChong :: (Eq a) => a -> [a] -> Bool
elemChong el xs = elemAux False el xs
  where elemAux True _ _ = True
        elemAux False _ [] = False
        elemAux False el (x:xs) = elemAux (el == x) el xs

elemSen :: (Eq a) => a -> [a] -> Bool
elemSen x []     = False
elemSen x (y:ys) = if x == y then True else elemSen x ys

elem' :: (Eq a) => a -> [a] -> Bool
elem' _ [] = False
elem' el (x:xs) = (el == x) || (elem' el xs)

-- returns lists without duplicate values
nubChong :: (Eq a) => [a] -> [a]
nubChong xs = nubAux [] xs
  where nubAux ys [] = ys
        nubAux ys (x:xs)
          | elemChong x ys = nubAux ys xs
          | otherwise      = nubAux (x:ys) xs

nubSen :: (Eq a) => [a] -> [a]
nubSen []     = []
nubSen (x:xs) =
  let ys = nubSen xs in
    if elemSen x ys  then ys else x:ys

nub' :: (Eq a) => [a] -> [a]
nub' [] = []
nub' (x:xs)
  | x `elem` xs = nub' xs
  | otherwise   = x : nub' xs

-- Seno added: returns [a] list with duplicate values removed *after first appearence*

nubSen' :: (Eq a) => [a] -> [a]
nubSen' = reverse . nubSen . reverse

reverseSen :: [a] -> [a]
reverseSen []     = []
reverseSen (x:xs) = reverseSen xs ++ [x]

-- return True if list of Ords in ascending order
isAscChong :: (Ord a) => [a] -> Bool
isAscChong [] = True
isAscChong (x:[]) = True
isAscChong (x1:x2:xs)
  | x1 > x2 = False
  | otherwise = isAscChong (x2:xs)

isAscSen :: (Ord a) => [a] -> Bool
isAscSen []     = True
isAscSen [_]    = True
isAscSen (x:xs) = x <= head xs && isAscSen xs

isAsc' :: (Ord a) => [a] -> Bool
isAsc' [] = True
isAsc' [x] = True
isAsc' (x:y:xs) = (x <= y) && isAsc' (y:xs)

------------------------------------------
-- 99 problems ---------------------------
------------------------------------------

-- 0: last element of list
-- not safe. Couldn't decide return value for empty list
lastElem :: [a] -> a
lastElem (x:[]) = x
lastElem (x:xs) = lastElem xs

-- Maybe a = Nothing | Just a

safeLast :: [a] -> Maybe a
safeLast [] = Nothing
safeLast (x:[]) = Just x
safeLast (x:xs) = safeLast xs

-- 1: penultimate element of list
-- not safe. Couldn't decide return value for edge cases
penultimate :: [a] -> a
penultimate (x1:x2:[]) = x1
penultimate (x1:x2:xs) = penultimate (x2:xs)

-- 2: find element in k-th position
-- fst position is position 0
-- not safe. Couldn't decide return value when k >= length list or negative
kthElem :: [a] -> Int -> a
kthElem (x:xs) 0 = x
kthElem (x:xs) k = kthElem xs (k-1)

-- 3: implement a length function for lists
length' :: [a] -> Int
length' xs = lengthAux 0 xs
  where lengthAux k [] = k
        lengthAux k (x:xs) = lengthAux (k+1) xs

-- 4: reverse a list
reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:[]) = [x]
reverse' (x:xs) = (reverse' xs) ++ [x]

-- 5: test lists for palindromality
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == (reverse' xs)

-- 6.a: flatten list of lists
flatten' :: [[a]] -> [a]
flatten' [] = []
flatten' (xs:xss) = xs ++ flatten' xss

-- 7: eliminate consecutive duplicates of list elements
compress :: (Eq a) => [a] -> [a]
compress xs = compressAux xs []
  where compressAux [] _ = []
        compressAux (x:[]) ys = ys ++ [x]
        compressAux (x1:x2:xs) ys
          | x1 == x2  = compressAux (x2:xs) ys
          | otherwise = compressAux (x2:xs) (ys ++ [x1])

compress' :: (Eq a) => [a] -> [a]
compress' [] = []
compress' [x] = [x]
compress' (x1:x2:xs)
  | x1 == x2 = compress' (x1:xs)
  | otherwise = x1 : (compress' (x2:xs))

-- 8: pack duplicates in sublists
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = [takeWhile (==x) (x:xs)] ++ (pack $ dropWhile (==x) xs)

-- 9: run-length-encode. Compress list into pairs of (elem, run_count)
encode :: (Eq a) => [a] -> [(a, Int)]
encode [] = []
encode (x:xs) = (x, 1 + (length $ takeWhile (==x) xs)) : encode (dropWhile (==x) xs)

-- 10: run-length-encode. Keep length 1 entries as is
---- Mixed type warning. Needs custom type
data Encoded a = Multiple Int a | Single a deriving Show

toEncode :: (a, Int) -> Encoded a
toEncode (a, 1) = Single a
toEncode (a, n) = Multiple n a

encode2 :: (Eq a) => [a] -> [Encoded a]
--encode2 xs = map toEncode $ encode xs
encode2 = (map toEncode) . encode

-- 11: Decode. Inverse of encode2
explode :: Int -> a -> [a]
explode n x = take n $ repeat x

decode :: (Eq a) => [Encoded a] -> [a]
decode [] = []
decode ((Single a) : as) = a : decode as
decode ((Multiple n a) : as) = (explode n a) ++ decode as

-- 13: Duplicate elements of a list
duplicate :: [a] -> [a]
duplicate [] = []
duplicate (x:xs) = x : x : duplicate xs

-- 14: Replicate each element of a list
replicate' :: [a] -> Int -> [a]
replicate' [] _ = []
replicate' (x:xs) n = (take n $ repeat x) ++ replicate' xs n

-- 15: Drop every n-th element from list
dropNth :: Int -> [a] -> [a]
dropNth n xs = take (n-1) xs ++ dropNth n (drop n xs)

-- 16: split list in 2. split on index given
splitOn :: [a] -> Int -> ([a], [a])
splitOn xs n = (take n xs, drop n xs)

-- 17: slice a list
slice' :: [a] -> Int -> Int -> [a]
slice' xs s f = (drop s) . (take f) $ xs

-- 18: rotate list n places to the left
rotList :: [a] -> Int -> [a]
rotList xs n = drop n xs ++ take n xs

-- 19: extract element from list at index n
removeAt :: Int -> [a] -> (a, [a])
removeAt n xs = (head $ drop (n-1) heads, take (n-1) heads ++ tails)
  where (heads, tails) = splitOn xs n

removeAt' :: Int -> [a] -> (a, [a])
removeAt' n xs = (head tails, heads ++ tail tails)
  where (heads, tails) = splitOn xs (n-1)

-- 20: insert element x in position n of a list

insertAt :: a -> [a] -> Int -> [a]
insertAt x xs n = join x $ splitOn xs n
  where join x (hs, ts) = hs ++ [x] ++ ts

insertAt' :: a -> [a] -> Int -> [a]
insertAt' x xs = (join x) . splitOn xs
  where join x (hs, ts) = hs ++ [x] ++ ts

(...) = (.) . (.)

insertAt'' :: [a] -> Int -> a -> [a]
insertAt'' = join ... splitOn
  where join (hs, ts) x = hs ++ [x] ++ ts

-- 21: implement enumFromTo. Keep it polimorphic to Enum
increasingRange' :: (Enum a, Eq a) => a -> a -> [a]
increasingRange' s e
  | s == e = [e]
  | otherwise = s: increasingRange' (succ s) e

range' :: (Enum a) => a -> a -> [a]
range' s e
  | comp == EQ = [e]
  | comp == LT = s : range' (succ s) e
  | otherwise  = s : range' (pred s) e
    where comp = compare (fromEnum s) (fromEnum e)

-- 25: generate the list of all combinations of size k from list xs

-- For now, I'm assuming list is devoid of duplicates or duplicates aren't "identical"
combinations' :: Int -> [a] -> [[a]]
combinations' _ [] = []
combinations' 0 _ = [[]]
combinations' 1 xs = [[x] | x <- xs]
combinations' k (x:xs) = [x : ys | ys <- combinations' (k-1) xs] ++ combinations' k xs

-- 26: get the disjoint subsets

-- partitions a list on given indices. Results in a list of the partitions 

