Eu dei uma olhadinha agora na tal diferença com undefined em newtype e data e fez sentido

Sejam 
```haskell
data D = D Int
newtype N = N Int
```
e considere 
```haskell
d = D undefined
n = N undefined
```
Na hora que o programa estiver rodando, o data constructor ``D`` é como se fosse uma função do tipo ``Int -> D``.

- Como Haskell é lazy, o elemento ``d`` não é o valor da função ``D`` no ponto, é uma expressão que só é reduzida se for necessário, e quando for necessário.
- Mas o data constructor ``N`` só existe na hora de compilar, então ``n`` é um ``Int``, já calculado. 

Então o n já está reduzido, ele vale undefined.

Isso é a minha compreensão do que está aqui: https://stackoverflow.com/questions/2649305/why-is-there-data-and-newtype-in-haskell/2650051#2650051