-- from: https://stackoverflow.com/questions/15800878/scoped-type-variables-require-explicit-foralls-why

{-# LANGUAGE ScopedTypeVariables #-}

-- If no Language Extension (PRAGMA), this causes error:

f :: [a] -> [a]                          -- No `forall` here ...
f (x : xs) = xs ++ [x :: a]              -- ... or here.

-- The reason is that it means:

g :: forall a. [a] -> [a]                -- With a `forall` here ...
g (x : xs) = xs ++ [x :: forall a. a]    -- ... and another one here.

-- The correct is:

h :: forall a. [a] -> [a]                -- A `forall` here ...
h (x : xs) = xs ++ [x :: a]              -- ... but not here.
