{-# LANGUAGE MonadComprehensions #-}

import Data.Char ( isDigit )

-- Senha válida: tem pelo menos um dígito

validPwd :: String -> Bool
validPwd = any isDigit

main :: IO ()
main = do
  putStrLn "Digite a senha:"
  candidate <- getLine 
  if validPwd candidate
    then
      putStrLn "Ok"
    else do
      putStrLn "Nah, precisa de pelo menos um digito"
      main

auxIf :: String -> IO () -> IO ()
auxIf candidate rec = [() | _ <- if  validPwd candidate
                            then putStrLn "Ok"
                            else putStrLn "Nah, precisa de pelo menos um digito" >> rec]

main' :: IO ()
main' = [() | _ <- putStrLn "Digite a senha:",
              candidate <- getLine, 
              _ <- if  validPwd candidate
                   then putStrLn "Ok"
                   else putStrLn "Nah, precisa de pelo menos um digito" >> main'
        ]