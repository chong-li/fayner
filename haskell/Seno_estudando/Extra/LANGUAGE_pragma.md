# Comments about the LANGUAGE pragma
From: https://typeclasses.com/extensions-intro

- The word pragma derives from the same sense of pragmatic that linguists use to talk about the way context can change the way utterances are understood, without affecting the basic semantics of the utterance.

- LANGUAGE is a file-header pragma, so language extensions are listed at the very top of the file, above the module name, and the entire file will be compiled in that context – although it is ok to turn on a language extension and then never use the syntax it enables within that file.

- Every language extension can also be turned into a command-line flag by prefixing it with -X; for example -XForeignFunctionInterface. So, for example, you can turn that on when you invoke GHC on the command line to compile your program. You can also use that to turn them on in GHCi, using GHCi’s :set command, e.g., :set -XTypeApplications.

- Some pragmas, such as INLINE, are not written at the top of a file and their scope is limited.

- Some language extensions build on others: for example, ExplicitForAll introduces the forall keyword, and ScopedTypeVariables expands upon the significance of forall. Since scoped type variables would not be useful without forall, GHC enables ExplicitForAll automatically whenever you turn on ScopedTypeVariables