{-# LANGUAGE MonadComprehensions #-}

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM mbool th el = do
  bool <- mbool
  if bool then th else el

-- >>> ifM [True, False, False] [1] [2, 3]
-- [1,1,2,3,2,3]

ifM'  :: Monad m => m Bool -> m a -> m a -> m a
ifM' mbool th el = do
  bool <- mbool
  x <- th
  y <- el
  if bool
    then return x
    else return y

-- >>> ifM' [True] [1] [2, 3]
-- [1,1]

ifM'' :: Monad m => m Bool -> m a -> m a -> m a
ifM'' mbool th el = do
  bool <- mbool
  let temp = if bool then th else el
  ans <- temp
  return ans

-- >>> ifM'' [True, False, False] [1] [2, 3]
-- [1,2,3,2,3]

ifM''' :: Monad m => m Bool -> m a -> m a -> m a
ifM''' mbool th el = [ans | bool <- mbool, ans <- if bool then th else el]

-- >>> ifM''' [True, False, False] [1] [2, 3]
-- [1,2,3,2,3]
