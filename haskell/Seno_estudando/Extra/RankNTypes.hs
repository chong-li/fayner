{-# LANGUAGE RankNTypes #-}

fchong :: (forall c. [c] -> d) -> ([a], [b]) -> (d, d)
fchong f (a, b) = (f a, f b)