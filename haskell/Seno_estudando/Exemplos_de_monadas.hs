

-- # Pragmas
-- Tirei o Prelude para poder definir as instâncias de mônadas sem problemas
-- Tb estou permitindo assinaturas na hora de definir instâncias

{-# LANGUAGE NoImplicitPrelude, InstanceSigs #-}

module Exemplos where

-- # Colocando umas coisas básicas de volta

import GHC.Num ( Num((+), (*)), Integer ) 
import Data.Bool ( Bool )
import Data.List ( concat, concatMap )
import Data.Function ( ($), (.), const )

-- # Classes de Funtor e Mônadas

class Functor functor where
  fmap :: (a -> b) -> functor a -> functor b

class Monad monad where
  return :: a -> monad a
  (>>=)  :: monad a -> (a -> monad b) -> monad b

-- # Instâncias de Funtores

instance Functor [] where
  fmap :: (a -> b) -> [a] -> [b]
  fmap f [] = []
  fmap f (x:xs) = f x : fmap f xs

instance Functor ( (,) t) where
  fmap :: (a -> b) -> (t, a) -> (t, b)
  fmap f (t, a) = (t, f a)

instance Functor ( (->) t) where
  fmap :: (a -> b) -> (t -> a) -> (t -> b)
  fmap f g = f . g

-- # Testes

l :: [Integer]
l = [2, 3, 7]

f :: Integer -> Integer
f = (+1)

data Tres = Zero | Um | Dois

g :: Tres -> Integer
g Zero = 2
g Um = 3
g Dois = 7

-- # Instâncias de Mônadas

instance Monad [] where

  return :: a -> [a]
  return x = [x]

  (>>=) :: [a1] -> (a1 -> [a2]) -> [a2]
  list >>= f = concat $ fmap f list

instance Monad ( (->) t) where

  return :: a -> (t -> a)
  return = const

  (>>=) :: (t -> a) -> (a -> (t -> b)) -> (t -> b)
  f >>= g = \x -> g ( f x ) x

-- # Testes

filter :: (a -> Bool) -> [a] -> [a]
filter p [] = []
filter p (x:xs) = if p x then x: filter' p xs else filter' p xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p l = do
  x <- l
  if p x
    then return x
    else []

filter'' :: (a -> Bool) -> [a] -> [a]
filter'' p = concatMap (\x -> if p x then return x else [] )

-- (>>=) :: [a] -> (a -> [b]) -> [b]
-- list >>= f = concat $ fmap f list
squares :: [Integer] -> [Integer]
squares l = do
  num <- l
  [num*num]-- imagine como uma funcao que recebe um unico num e devolve [num*num]
           -- (uma lista de um unico elemento)
           -- (\num -> [num*num])
f_sq num = [num*num]
-- |
-- >>> squares [1,2,3]
-- [1,4,9]
-- >>> fmap f_sq [1,2,3]
-- [[1],[4],[9]]
-- >>> concat $ fmap f_sq [1,2,3]
-- [1,4,9]


-- (>>=) :: [a] -> (a -> [b]) -> [b]
-- list >>= f = concat $ fmap f list
prods :: [Integer] -> [Integer] -> [Integer]
prods l1 l2 = do
  x   <- l1
  y   <- l2 -- imagine como uma funcao que recebe um unico x e devolve [x*y | y]
  [x*y]     -- (uma lista de varios elementos)
            -- (\x -> map (x*) l2)
f_prod a b = [a*b]
-- |
-- >>> prods [1,2] [1,2]
-- [1,2,2,4]

prods2 :: [Integer] -> [Integer] -> [Integer]
prods2 l1 l2 = l1 >>= (\x -> l2 >>= (\y -> [x*y]))
--           = l1 >>= (\x -> concat $ fmap (\y -> [x*y]) l2)
--           = l1 >>= (\x -> fmap (\y -> x*y) l2)
--           = concat $ fmap (\x -> fmap (\y -> x*y) l2) l1



-- |
-- >>> prods2 [1,2] [1,2]
-- [1,2,2,4]



cart :: [a] -> [b] -> [(a, b)]
cart l1 l2 = do
  x <- l1
  y <- l2
  return (x, y)




-- |
-- >>> cart [1,2] ["ba","na"]
-- [(1,"ba"),(1,"na"),(2,"ba"),(2,"na")]

cart' :: [a] -> [b] -> [(a, b)]
cart' l1 l2 = [(x, y) | x <- l1, y <- l2]

add :: (t -> Integer) -> (t -> Integer) -> (t -> Integer)
add f g x = f x + g x

add' :: (t -> Integer) -> (t -> Integer) -> (t -> Integer)
add' f g = do
  y <- f
  z <- g
  return ( y + z )

add'' :: (t -> Integer) -> (t -> Integer) -> (t -> Integer)
add'' f g = f >>= \y -> g >>= \z -> return ( y + z )