{-# LANGUAGE InstanceSigs #-}

import Data.List ( nub )
import Control.Monad ( ap, liftM ) 
import Control.Monad.Constrained

newtype FinDist x = FinDist { toList :: [(x, Rational)]} deriving Show

combine :: Eq x => FinDist x -> FinDist x
combine dist = FinDist $ do
  let list = toList dist
  let elems = nub $ fst <$> list
  x <- elems
  let allX = filter ( (== x) . fst ) list
  let p = sum $ snd <$> allX
  return (x, p)

instance Functor FinDist where
  fmap :: (x -> y) -> FinDist x -> FinDist y
  fmap = liftM

instance Applicative FinDist where
  pure = return
  (<*>) = ap
  

instance Monad FinDist where

  return :: x -> FinDist x
  return x = FinDist [(x, 1)]

  (>>=) :: FinDist x -> (x -> FinDist y) -> FinDist y
  dist >>= f
    = FinDist $ concatMap listOnPoint $ toList dist
    where
      listOnPoint (x, p) = map (fmap (*p)) $ toList (f x)

dice :: FinDist Int
dice = FinDist $ zip [1..6] (repeat (1/6))

sumOfDice :: FinDist Int
sumOfDice = combine $ do
  x <- dice
  y <- dice
  return (x+y)

sumOfDice' :: FinDist Int
sumOfDice' = combine $ dice >>= \x -> dice >>= \y -> return (x+y)