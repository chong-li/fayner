-- # Pragmas
-- Tirei o Prelude para poder definir as instâncias de mônadas sem problemas
-- Tb estou permitindo assinaturas na hora de definir instâncias

{-# LANGUAGE NoImplicitPrelude, InstanceSigs #-}

-- # Colocando umas coisas básicas de volta

import GHC.Num 
import GHC.Real
import GHC.Float
import Data.Bool ( Bool )
import Data.List ( concat, concatMap )
import Data.Function ( ($), (.), const )
import GHC.Err ( undefined )

-- # Classes de Funtor e Mônadas

class Functor functor where
  fmap :: (a -> b) -> functor a -> functor b

class Monad monad where
  return :: a -> monad a
  (>>=)  :: monad a -> (a -> monad b) -> monad b

-- # Instâncias de Funtores

-- ## Listas

instance Functor [] where
  fmap :: (a -> b) -> [a] -> [b]
  fmap f [] = []
  fmap f (x:xs) = f x : fmap f xs

-- ## Pares
-- Mais precisamente, produto cartesiano ``t x _``, com ``t`` fixo

instance Functor ( (,) t) where
  fmap :: (a -> b) -> (t, a) -> (t, b)
  fmap f (t, a) = (t, f a)

-- ## Funções com domínio fixo ``t``

instance Functor ( (->) t) where
  fmap :: (a -> b) -> (t -> a) -> (t -> b)
  fmap f g = f . g

-- # Testes

l :: [Integer]
l = [2, 3, 7]

f :: Integer -> Integer
f = (+1)

-- >>> fmap f l

--

data Tres = Zero | Um | Dois

g :: Tres -> Integer
g Zero = 2
g Um = 3
g Dois = 7

-- # Instâncias de Mônadas

-- ## Mônada das listas:

instance Monad [] where

  return :: a -> [a]
  return x = [x]

  (>>=) :: [a] -> (a -> [b]) -> [b]
  list >>= f = concatMap f list
  -- Em outras palavras, (>>=) = flip concatMap,
  --           ou ainda, (=<<) = concatMap

-- ## Mônada das funções com domínio ``t``:

instance Monad ( (->) t) where

  return :: a -> (t -> a)
  return = const

--(>>=) :: monad a  -> (a -> monad b)  -> monad b
  (>>=) :: (t -> a) -> (a -> (t -> b)) -> (t -> b)
  f >>= g = \t -> g (f t) t

-- Gosto de pensar nessa mônada com exemplos de física:

-- Digamos que temos uma função ``f :: time -> space`` dando a posicao de uma
-- particula no espaço, variando no tempo, e uma função
-- ``g :: space -> time -> temperature`` que dá a temperatura de cada posição
-- no espaço, variando no tempo. 
-- Então ``f >>= g`` dá a temperatura da posição onde a partícula está em cada
-- momento. 

type Space       = Double 
type Time        = Double 
type Temperature = Double

s :: Time -> Space
s t = t^2 + 2 * t - 1

h :: Time -> Temperature
h = do
  x <- s
  return (x * 2)

-- (>>=) :: (t -> a) -> (a -> (t -> b)) -> (t -> b)
-- f >>= g = \t -> g (f t) t

h' :: Time -> Space
h' = s >>= \x -> return (x * 2)
-- = s >>= \x -> const (x * 2)
-- = \t -> (\x -> const (x * 2)) (s t) t
-- = \t -> (const ((s t) * 2)) t
-- = \t -> (s t) * 2

-- h' t = (s t) * 2

-- # Testes

filter :: (a -> Bool) -> [a] -> [a]
filter p [] = []
filter p (x:xs) = if p x
                    then x : filter' p xs
                    else filter' p xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p l = do
  x <- l
  if p x
    then return x
    else []

filter'' :: (a -> Bool) -> [a] -> [a]
filter'' p = concatMap (\x -> if p x then return x else [] )

--

cart :: [a] -> [b] -> [(a, b)]
cart l1 l2 = do
  x <- l1
  y <- l2
  return (x, y)

cart' :: [a] -> [b] -> [(a, b)]
cart' l1 l2 = [(x, y) | x <- l1, y <- l2]

-- Vamos fazer as contas do ``cart``, para "tirar a mônada":
-- cart l1 l2 == l1 >>= \x -> (l2 >>= \y -> return (x, y))
--            == l1 >>= \x -> (l2 >>= \y -> [(x, y)])
--            == l1 >>= \x -> (concatMap (\y -> [(x, y)]) l2)
--            == l1 >>= \x -> (map (\y -> (x, y)) l2)           -- simplificação: retirei o concat e o []
--            == concatMap (\x -> (map (\y -> (x, y)) l2)) l1
--            == concat $ map (\x -> (map (\y -> (x, y)) l2)) l1
--
-- Nessa última linha: a gente substitui cada entrada x em l1 pela lista [(x,y)],
-- onde y varia por l2. Depois a gente concatena.

--

add :: (t -> Integer) -> (t -> Integer) -> (t -> Integer)
add f g x = f x + g x

add' :: (t -> Integer) -> (t -> Integer) -> (t -> Integer)
add' k h = do
  y <- k
  -- Integer -> (t -> Integer)
  z <- h
  return ( y + z ) -- t -> Integer

add'' :: (t -> Integer) -> (t -> Integer) -> (t -> Integer)
add'' k h = k >>= ( \y -> h >>= \z -> return ( y + z ) )

-- (>>=) :: (t -> a) -> (a -> (t -> b)) -> (t -> b)
-- f >>= g = \t -> g (f t) t

-- Fazendo symbol pushing, vamos simplificar a add'':

-- return ( y + z )                             == const ( y + z )
-- \z -> return ( y + z )                       == \z -> const ( y + z )
-- h >>= \z -> return ( y + z )                 == \t -> (\z -> const ( y + z )) ( h t ) t
--                                              == \t -> const ( y + h t ) t
--                                              == \t -> y + h t
-- \y -> h >>= \z -> return ( y + z )           == \y t -> y + h t
-- k >>= ( \y -> h >>= \z -> return ( y + z ) ) == \w -> (\y t -> y + h t) ( k w ) w
--                                              == \w -> k w + h w

-- Eu não entendi nada além de "funciona mesmo" fazendo isso.

-- O exemplo acima é de somar funções. Talvez ajude um exemplo de multiplicar a função por escalar:

mult :: (Num a) => a -> (t -> a) -> (t -> a)
mult scalar function = do
  valueOnPoint <- function
  return (scalar * valueOnPoint)

mult' :: (Num a) => a -> (t -> a) -> (t -> a)
mult' scalar function = function >>= \valueOnPoint -> return (scalar * valueOnPoint)

-- return (scalar * valueOnPoint)                  == const (scalar * valueOnPoint)
-- \valueOnPoint -> return (scalar * valueOnPoint) == \valueOnPoint -> const (scalar * valueOnPoint)
--
-- function >>= \valueOnPoint -> return (scalar * valueOnPoint)
--                                 == \x -> (\valueOnPoint -> const (scalar * valueOnPoint)) ( function x ) x
--                                 == \x -> const (scalar * function x) x
--                                 == \x -> scalar * function x

-- Tá, mais fácil mas ainda não tá claro.

-- g :: space -> time -> temperature
-- f :: time -> space
-- f >>= g :: time -> temperature

-- g s = g s >>= return 

-- Vamos voltar pra analogia de física. Só pra ter uma multiplicação por escalar,
-- vamos trocar a temperatura pela aceleração da gravidade. Suponhamos que no
-- ponto 0 temos um corpo com massa 1/G que, portanto, causa uma aceleração de
-- -1/r^2 num segundo corpo que esteja na posição r. Esse campo é constante no
-- tempo. Então temos:

-- type force = space
-- g :: space -> time -> force
-- g r

-- Vai ficar estranho, mas...
-- space = a
-- time = t
-- temperature = a
-- f = function
-- g = \valueOnPoint -> return (scalar * valueOnPoint)

-- Pra se livrar desse lambda:
-- g :: (Num a) => a -> t -> a
-- g valueOnPoint = const (scalar * valueOnPoint)
-- ou ainda
-- g valueOnPoint _ = scalar * valueOnPoint

-- A nossa function representa a posição variando no tempo, e a nossa g representa
-- a temperatura para cada posição no espaço e momento no tempo. Só que ela é
-- constante no tempo. 


-- A nossa posição no tempo é a function,
-- e a nossa temperatura é ``\valueOnPoint -> return (scalar * valueOnPoint)``.
-- Em outras palavras, vamos supor ``scalar`` sendo fixo e, lembrando que
-- return = const, definimos:

-- g :: (Num a) => a -> t -> a
-- g valueOnPoint = const (scalar * valueOnPoint)
-- ou ainda
-- g valueOnPoint _ = scalar * valueOnPoint

-- A analogia está confusa, mas nesse casa o ``space``


-- Vamos pensar em casos triviais.

-- A gente "sempre" termina com return. Um caso bem bobinho seria:
bobo1 :: (t -> a) -> (t -> a)
bobo1 f = do
  valueOnPoint <- f
  return valueOnPoint

-- Claro que a gente quer que bobo1 f seja igual a f. 
-- Eu acho engraçado, pq a última linha é um return de algo, ou seja, a última
-- linha é uma função constante!
-- Mas o resultado tem que ser f, que pode não ser constante. 
-- Vamos a isso:

-- bobo1 f == f >>= (\valueOnPoint -> return valueOnPoint)
-- bobo1 f == \x -> (\valueOnPoint -> return valueOnPoint) (f x) x
-- bobo1 f == \x -> (return (f x)) x
-- bobo1 f == \x -> (const (f x)) x
-- bobo1 f == \x -> f x
-- bobo1 f == f

-- Quer dizer, a gente não está "jogando" a f numa função constante, a gente ta
-- jogando f em \valueOnPoint -> return valueOnPoint, que é uma função que, pra 
-- cada ponto, retorna uma função constante. Isso faz mais sentido. 

-- Então a gente fica com a função que retorna a função constante igual à f calculada
-- naquele mesmo ponto, e pra se livrar dessa "segunda camada" de funções, a gente
-- calcula essa função constante num ponto qualquer (no caso, o mesmo x).

-- (Veja que a gente sempre tem essa "segunda camada" de mônada. Nas listas a gente
-- fez um concat pra se livrar da segunda camada. No Maybe a gente se livra de um
-- dos Just.)

-- (Vale comentar: \valueOnPoint -> return valueOnPoint nada mais é do que return,
-- então a 1a linha do bloco de contas é bobo1 f == f >>= return. Tinha que ser algo
-- bobo mesmo.)

-- Acho que vale a pena pensar agora num caso que não termina com return, ie,
-- que não termina com uma função constante. Pra manter simples, vamos fazer 
-- a f ser constante.
-- Lembremos que a assinatura do (>>=) é (t -> a) -> (a -> (t -> b)) -> (t -> b).

bobo2 :: a -> (a -> (t -> b)) -> (t -> b)
bobo2 a g = const a >>= g

-- Isso é o mesmo que
-- bobo2 a g = return a >>= g



-- Outro caso bobo é quando o primeiro argumento é uma função constante, ie, f = return a para algum a:
-- return a >>= g == \x -> g ( (return a) x) x
--                == \x -> g ( (const a) x) x
--                == \x -> g a x
--                == g a

-- Usando do, isso seria:
-- return a >>= g = do
--   value <- return a
--   g value

-- Parece justo que o resultado seja g a mesmo.