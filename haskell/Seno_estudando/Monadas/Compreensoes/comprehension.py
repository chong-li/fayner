# Comprehensions com fors: 

## Listas independentes:

comp1 = [(x,y) for x in [1,2] for y in [3,4]]

comp2 = []
for x in [1,2]:
  for y in [3,4]: 
    comp2.append((x,y))

assert comp1 == comp2

## Listas dependentes:

xss = [[1,2],[3],[4,5,6]]
comp3 = [x for xs in xss for x in xs]

comp4 = []
for xs in xss:
  for x in xs:
    comp4.append(x)

assert comp3 == comp4

# Comprehensions com fors e maps:

## Apenas uma lista:

comp5 = [x**2 for x in [1,2,3]]

comp6 = []
for x in [1,2,3]:
  comp6.append(x**2)

comp7 = list(map(lambda x: x**2, [1,2,3]))

assert comp5 == comp6
assert comp5 == comp7

## Para simplifcar a notação:

def fmap (func, xs):
  return list(map(func,xs))

def concat(xss):
  return [x for xs in xss for x in xs]

## Revisitando as comp1 e comp2:

comp1 = [(x,y) for x in [1,2] for y in [3,4]]

comp2 = []
for x in [1,2]:
  for y in [3,4]: 
    comp2.append((x,y))

comp8_linha = []
for x in [1,2]:
  temp = fmap(lambda y: (x,y), [3,4])
  comp8_linha.append(temp)
comp8 = concat(comp8_linha)

assert comp1 == comp8

### Seguindo a lógica das comp6 e comp7:
comp9_linha = fmap (lambda x: fmap(lambda y: (x,y), [3,4]), [1,2])
### podemos ler como: para cada x em [1,2], aplique a função
### `lambda y: (x,y)` em todos os elementos de [3,4]

comp9 = concat(comp9_linha)

assert comp1 == comp9

### Seguindo a lógica das comp5 e comp7, podemos reescrever o `comp9_linha` como
comp10_linha = [fmap(lambda y: (x,y), [3,4]) for x in [1,2]]
### e, repetindo a mesma lógica, como
comp11_linha = [[(x,y) for y in [3,4]] for x in [1,2]]

comp10 = concat(comp10_linha)
comp11 = concat(comp11_linha)

assert comp1 == comp10
assert comp1 == comp11

## Revisitando as comp3 e comp4:
xss = [[1,2],[3],[4,5,6]]
comp3 = [x for xs in xss for x in xs]

comp4 = []
for xs in xss:
  for x in xs:
    comp4.append(x)

comp12_linha = []
for xs in xss:
  temp = fmap(lambda x: x, xs)
  comp12_linha.append(temp)
comp12 = concat(comp12_linha)
### sim... é bem redundante, mas é para ver que a lógica funciona mesmo

assert comp3 == comp12

comp13_linha = fmap(lambda xs: fmap(lambda x: x, xs), xss)
comp13 = concat(comp13_linha)

assert comp3 == comp13

comp14_linha = [fmap(lambda x: x, xs) for xs in xss]
comp15_linha = [[x for x in xs] for xs in xss]

comp14 = concat(comp14_linha)
comp15 = concat(comp15_linha)

assert comp3 == comp14
assert comp3 == comp15