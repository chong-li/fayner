{-# LANGUAGE MonadComprehensions #-}

import qualified Data.Map as Map
import Control.Monad

type UserName = String
type GamerId = Int
type PlayerCredits = Int

-- Given tables:

userNameDB :: Map.Map GamerId UserName
userNameDB = Map.fromList [(1,"nYarlathoTep")
  ,(2,"KINGinYELLOW")
  ,(3,"dagon1997")
  ,(4,"rcarter1919")
  ,(5,"xCTHULHUx")
  ,(6,"yogSOThoth")]

creditsDB :: Map.Map UserName PlayerCredits
creditsDB = Map.fromList [("nYarlathoTep",2000)
  ,("KINGinYELLOW",15000)
  ,("dagon1997",300)
  ,("rcarter1919",12)
  ,("xCTHULHUx",50000)
  ,("yogSOThoth",150000)]

-- Desired function:
-- creditsFromId :: GamerId -> Maybe PlayerCredits

lookupUserName :: GamerId -> Maybe UserName
lookupUserName id = Map.lookup id userNameDB

lookupCredits :: UserName -> Maybe PlayerCredits
lookupCredits username = Map.lookup username creditsDB

-- Solution 1:
creditsFromId1 :: GamerId -> Maybe PlayerCredits
creditsFromId1 id = lookupUserName id >>= lookupCredits

-- Solution 2:
creditsFromId2 :: GamerId -> Maybe PlayerCredits
creditsFromId2 = lookupUserName >=> lookupCredits -- Monad composition!

-- Solution 3:
creditsFromId3 :: GamerId -> Maybe PlayerCredits
creditsFromId3 id = [ credits | userName <- lookupUserName id, credits <- lookupCredits userName]

-- Solution 4:
creditsFromId4 :: GamerId -> Maybe PlayerCredits
creditsFromId4 id = do
  userName <- lookupUserName id
  credits <- lookupCredits userName
  return credits

-- Solution 5:
creditsFromId5 :: GamerId -> Maybe PlayerCredits
creditsFromId5 id = do
  userName <- lookupUserName id
  lookupCredits userName