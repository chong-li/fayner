{-# LANGUAGE MonadComprehensions #-}

import Control.Monad ( join )

numero :: IO Int 
numero = [read str | _ <- putStrLn "Digite um número:", str <- getLine]

outroNumero :: IO Int
outroNumero = join [ [read str | str <- getLine] :: IO Int | _ <- putStrLn "Digite um número:"]

quadrado :: IO Int
quadrado = [ x^2 | x <- numero]

main :: IO ()
main = [ x | _ <- putStrLn "Qual é o seu nome?", name <- getLine, x <- print ("Carai, " ++ name ++ ", que coisa!")]