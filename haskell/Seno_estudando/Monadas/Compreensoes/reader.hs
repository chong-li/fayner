{-# LANGUAGE MonadComprehensions #-}

import Control.Monad
import Control.Monad.Reader
import qualified Data.Map as Map

-- Reader é exatamente idêntico a monada das funções, só com um newtype ao redor
-- Também é chamada de "Enviorement Monad"
-- Pode ser pensada como "computações com constantes globais"

-- Digamos que o raio da terra é uma constante.

type Radius  = Double
type LatLong = (Double,Double)

locationDB :: Map.Map String LatLong
locationDB = Map.fromList [("Sao Paulo",  (-23.5505, -46.6333 ))
                          ,("Americana",  (-22.7510, -47.3332 ))
                          ,("Belem do PA",( -1.4549, -48.5022 ))
                          ,("Macapa",     (  0.0346, -51.0640 ))
                          ,("Oiapoque",   (  3.8435, -51.8156 ))
                          ,("Vancouver",  ( 49.2827, -123.1207))
                          ,("Victoria",   ( 48.4278, -123.3651))
                          ,("St. John's", ( 47.5872, -52.6941 ))]

-- Haversine formula

earthRadius :: Radius
earthRadius = 6357.5 -- km

toRadians :: Double -> Double
toRadians degrees = degrees * pi / 180

latLongToRads :: LatLong -> (Double,Double)
latLongToRads (lat,long) = (rlat,rlong)
  where
    rlat = toRadians lat
    rlong = toRadians long

distSphere :: LatLong -> LatLong -> Radius -> Double
distSphere coords1 coords2 radius = radius * diffAngle
  where
    (rlat1,rlong1) = latLongToRads coords1
    (rlat2,rlong2) = latLongToRads coords2
    dlat = rlat2 - rlat1
    dlong = rlong2 - rlong1
    a = sin (dlat/2) ^2 + cos rlat1 * cos rlat2 * sin (dlong/2) ^2
    diffAngle = 2 * atan2 (sqrt a) (sqrt (1-a))

----

a = [x + y * z| x <- (^2), y <- id, z <- const 1] 3

----

-- Qual é a maior distância?

distCities :: String -> String -> Maybe Double
distCities a b = [distSphere coord1 coord2 earthRadius | coord1 <- Map.lookup a locationDB, coord2 <- Map.lookup b locationDB]

-- maisLonge :: String -> String -> String -> String
-- maisLonge a b c = if dist1 > dist2 then b else c
--   where

energiaTotal m v h = [energiaCinetica + energiaPotencial | energiaCinetica <- pure (m*v^2/2), energiaPotencial <- \g -> m*g*h]
    