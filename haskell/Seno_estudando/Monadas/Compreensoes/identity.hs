{-# LANGUAGE MonadComprehensions #-}

import Data.Functor.Identity
import Control.Monad
-- https://hackage.haskell.org/package/base-4.15.0.0/docs/Data-Functor-Identity.html#t:Identity

a :: Identity Int
a = Identity 2

b :: Identity Int
b = Identity 10

z :: Identity Int
z = [y `quot` 3 | x <- a, y <- fmap (x*) b] -- Parece um where

w :: Identity Int
w = join [ [y `quot` 3 | y <- fmap (x*) b] | x <- a ] -- Parece com where's encadeados
                                                      -- (mas where com setinha)