import Data.Monoid

list = [1, 3, 6, 21, 100]

soma = getSum $ foldr (<>) mempty . map Sum $ list

pairs = do
  a <- list
  b <- list
  return (a,b)

filter' p list =
  do
    a <- list
    if p a then return a else []