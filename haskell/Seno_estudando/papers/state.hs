newtype State s a = S (s -> (a, s))

instance Monad (State s) where
  return a = S (\mem -> (a, mem))

  (S f) >>= g = S h
    where
      h s1 = (a2, s3)
        where
          (a1, s2) = f s1
          S f2 = g a1
          (a2, s3) = f2 s2              

instance Applicative (State s) where
  pure = return
  (<*>) = undefined 

instance Functor (State s) where
  fmap = undefined 

acumulador :: [String] -> State Int [String]
acumulador lista = S f
  where
    f mem = (paridade : lista, mem +1)
      where
        paridade = if even mem
                  then "par"
                  else "impar"

acumule3vezes :: State Int [String]
acumule3vezes = return [] >>= acumulador >>= acumulador >>= acumulador

f :: Int -> ([String], Int)
S f = acumule3vezes

-- >>> f 131
-- (["impar","par","impar"],134)
-- >>> f 42
-- (["par","impar","par"],45)
