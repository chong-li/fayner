# Uma motivação para Mônadas - Waddler92

* Esse paper me parece ser mais próximo do paper original do Moggi, que introduziu mônadas (oficialmente) na programação funcional.

## intro

* Primeiramente, fora Temer. Mas depois vamos esquecer o que a gente sabe sobre mônadas, para ter uma folha em branco onde começar a pensar. Acima de tudo, vamos esquecer a mônada das listas.

* Vcs lembram dá metáfora da caixinha para type constructors, onde a gente punha o elemento dentro da caixinha? A minha metáfora informal e imprecisa para mônadas é parecida. Uma mônada é um computadorzinho, onde a gente põe valores mas ele também pode computar coisas, conforme suas próprias regras. Seguindo o paper, uma mônada é uma tripla (M, unitM, bindM), onde M é um type constructor, unitM é uma função polimorfa `a -> M a` e bindM é uma função polimorfa `M a -> (a -> M b) -> M b`. Então M é a máquina, e unitM e bindM são maneiras de interagir com o seu jeito de computar.

Claro que `unitM` é o `return`. E a ideia é que
```
unitM 2 :: M Int
```
significa codificar o número 2 na memória da máquina. Em geral, um `M Int` é algo na memória da máquina que a gente não pode ler diretamente (talvez se a gente souber os detalhes da implementação da máquina, mas estou falando "em geral"), mas que a gente sabe que é algo que a máquina interpreta como um inteiro.

Assim, escrever `return 2` é algo que faz sentido. A gente está "retornando" o valor 2 para a máquina, seja lá o que ela for fazer com ele!

* Voltando um pouco no paper, vamos colocar um contexto: a gente vai escrever alguns interpretadores de um lambda cálculo simplezinho, que só sabe lidar com Int e +, além de fazer abstrações lambda. Já vamos ver o código pra ver o que isso significa. A expressão que a gente vai usar de teste é `((\x -> x + x) (10 + 11))`, que na máquina usual deve ser reduzida para `42`.

A ideia é que a gente vai ter um código que a gente quer mudar só um pouquinho cada vez para rodar em diferentes máquinas. Cada uma faz uma coisa a mais além da execução vanilla. Uma lida com erros, como se a linguagem tivesse exceptions. Outra conta os passos de execução, como se a linguagem tivesse variáveis globais mutáveis. Outra ainda adiciona instruções novas como se a linguagem tivesse side effects.

* Para lidar com essa máquinas diferentes, a gente vai precisar que o valor final de cada função esteja na linguagem das máquinas. Sendo mais preciso, a gente vai substituir funções do tipo `a -> b` por funções do tipo `a -> m b`. Ou invés de uma função `Term -> Environment -> Value`, a gente vai querer uma função `Term -> Environment -> m Value`. Faz sentido: as funções leem coisas no código normal, mas executam tudo dentro da maquininha que a gente estiver usando.

Um exemplo interessante é ver o que acontece com a função identidade. Ao invés de a gente usar `id :: a -> a`, a gente vai usar `return :: a -> m a`, que traduz um valor normal pra linguagem da maquininha, mas "sem alterar ele". Bem, pelo menos essa é a ideia informal.

* Enquanto o return/unitM joga os elementos dentro da maquininha, o bindM é um jeito de usar as maquininhas sem ter que ler o valores. É um jeito de evitar sujar as mãos. E funciona como uma espécie de `let`. O exemplo do paper é a composição: se f :: a -> b e g :: b -> c, a gente pode definir
```
g . f = \a -> let b = f a in g b
```
Da mesma forma, a gente pode compor uma f :: a -> m b com uma g :: b -> m c por
```
g <=< f = \a -> f a `bindM` (\b -> g b)
```
Esse ponto é interessante, mas não me convenceu muito ainda. Mas é algo como "se rolasse ler o valor de `f a` como um `b` ao invés de `m b`, a gente chamaria ele de `b` e então aplicaria `g`".

* Dito tudo isso, vamos para o código (pg. 4). Depois a gente vê o `term0` na página 3 e a interpretação de uma constante, também na pg. 3.


* Em geral não rola ler o valor dentro das maquininhas, mas para as que a gente vai usar temos uma função ``ShowM :: m Value -> String``.


## Maquininhas

* Identity
* Error Messages
* Error Messages with positions
* State
* Output
* Non-deterministic choice
* Backwards state

