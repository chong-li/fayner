{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_LearnParsers (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "C:\\Users\\caiod\\Documents\\fayner\\haskell\\Seno_lendo_o_livro\\Chap24\\LearnParsers\\.stack-work\\install\\8e615599\\bin"
libdir     = "C:\\Users\\caiod\\Documents\\fayner\\haskell\\Seno_lendo_o_livro\\Chap24\\LearnParsers\\.stack-work\\install\\8e615599\\lib\\x86_64-windows-ghc-8.8.4\\LearnParsers-0.1.0.0-52mLiWa0mq61HMv8XP3UXc-LearnParsers"
dynlibdir  = "C:\\Users\\caiod\\Documents\\fayner\\haskell\\Seno_lendo_o_livro\\Chap24\\LearnParsers\\.stack-work\\install\\8e615599\\lib\\x86_64-windows-ghc-8.8.4"
datadir    = "C:\\Users\\caiod\\Documents\\fayner\\haskell\\Seno_lendo_o_livro\\Chap24\\LearnParsers\\.stack-work\\install\\8e615599\\share\\x86_64-windows-ghc-8.8.4\\LearnParsers-0.1.0.0"
libexecdir = "C:\\Users\\caiod\\Documents\\fayner\\haskell\\Seno_lendo_o_livro\\Chap24\\LearnParsers\\.stack-work\\install\\8e615599\\libexec\\x86_64-windows-ghc-8.8.4\\LearnParsers-0.1.0.0"
sysconfdir = "C:\\Users\\caiod\\Documents\\fayner\\haskell\\Seno_lendo_o_livro\\Chap24\\LearnParsers\\.stack-work\\install\\8e615599\\etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "LearnParsers_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "LearnParsers_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "LearnParsers_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "LearnParsers_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "LearnParsers_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "LearnParsers_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "\\" ++ name)
