# Monoids

- Monoid now depends on Semigroup, the book is not updated.
- Mono = 1, ie, it has identity
- Semigroup has only the associative binary operation.

## Classes:

```
class Semigroup a where
        (<>) :: a -> a -> a
```

```
class Semigroup a => Monoid a where
        mempty  :: a
        mappend :: a -> a -> a
        mappend = (<>)
        mconcat :: [a] -> a
        mconcat = foldr mappend mempty
```

- Eu deliberadamente retirei coisas não essenciais mas que fazem parte da
definição da classe Semigroup

## As in the book:
```
instance Monoid [a] where
  mempty = []
  mappend = (++)
```
## 15.6 Why Integer doesn’t have a Monoid

```
Prelude> let x = 1 :: Integer
Prelude> let y = 3 :: Integer
Prelude> mappend x y
<interactive>:6:1: error:
• No instance for (Monoid Integer)
arising from a use of ‘mappend’
• In the expression: mappend x y
In an equation for ‘it’:
it = mappend x y
```

- To resolve the conflict, we have the Sum and Product newtypes
to wrap numeric values

```
Prelude> mappend (Sum 1) (Sum 5)
Sum {getSum = 6}
Prelude> mappend (Product 5) (Product 5)
Product {getProduct = 25}
Prelude> mappend (Sum 4.5) (Sum 3.4)
Sum {getSum = 7.9}
```

```
newtype Sum a = Sum { getSum :: a }

instance Num a => Semigroup (Sum a) where
        (Sum a) <> (Sum b) = Sum (a + b)

instance Num a => Monoid (Sum a) where
        mempty = Sum 0
```

- It’s worth pointing out here that numbers aren’t the only
sets that have more than one possible monoid. Lists have
more than one possible monoid, although for now we’re only
working with concatenation (we’ll look at the other list monoid
in another chapter).

## Why newtype?
- Use of a newtype can be hard to justify or explain to people that
don’t yet have good intuitions for how Haskell code gets com-
piled and the representations of data used by your computer
in the course of executing your programs.

- go to pg 897

## 15.7 Why bother?


- Funny examples:
 - Semigroup a => Monoid 
 ```
 instance Semigroup a => Semigroup (Maybe a) where
        Nothing <> x = x
        x <> Nothing = x
        (Just a) <> (Just b) = Just (a <> b)

 instance Semigroup a => Monoid (Maybe a) where
        mempty = Nothing
 ```

 - https://hackage.haskell.org/package/base-4.15.0.0/docs/Data-Monoid.html#t:First
 -  (Ord a, Bounded a) => Monoid (Max a)
```haskell
instance Ord a => Semigroup (Max a) where
  (<>) (Max a) (Max b) = Max $ max a b

instance (Ord a, Bounded a) => Monoid (Max a) where
  mempty = Max minBound
```
        
 -  Monoid b => Monoid (a -> b)
```haskell
instance Semigroup b => Semigroup (a -> b) where
  f <> g = \x -> (f x) <> (g x)

instance Monoid b => Monoid (a -> b) where
  mempty :: a -> b
  mempty = const (mempty :: b)
```

## Comment about foldable:

https://hackage.haskell.org/package/base-4.14.1.0/docs/Data-Foldable.html