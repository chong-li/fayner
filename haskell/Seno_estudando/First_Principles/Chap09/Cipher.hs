module Cipher where

import Data.Char (ord, chr)

caesar :: Int -> String -> String 
caesar n = map cipher
  where
    cipher ' ' = ' '
    cipher char = chr $ ((ord char - ord 'a' + n) `mod` 26) + ord 'a'