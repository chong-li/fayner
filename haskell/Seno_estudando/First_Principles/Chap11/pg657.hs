-- data types:

data OperatingSystem
  = GnuPlusLinux
  | OpenBSDPlusNevermindJustBSDStill
  | Mac
  | Windows
  deriving (Eq, Show)

data ProgLang
  = Haskell
  | Agda
  | Idris
  | PureScript
  deriving (Eq, Show)

data Programmer = Programmer
  { os :: OperatingSystem,
    lang :: ProgLang
  }
  deriving (Eq, Show)

-- Lists:

allOperatingSystems :: [OperatingSystem]
allOperatingSystems =
  [ GnuPlusLinux,
    OpenBSDPlusNevermindJustBSDStill,
    Mac,
    Windows
  ]

allLanguages :: [ProgLang]
allLanguages =
  [Haskell, Agda, Idris, PureScript]

type FoldingMap = OperatingSystem -> [Programmer] -> [Programmer]

allProgrammers :: [Programmer]
allProgrammers = cartesianProduct allOperatingSystems allLanguages
  where
    cartesianProduct [] _ = []
    cartesianProduct (x : xs) ys = map (Programmer x) ys ++ cartesianProduct xs ys

allProgrammers' :: [Programmer]
allProgrammers' = foldr f [] allOperatingSystems
  where
    f x y = map (Programmer x) allLanguages ++ y

    -- f x = (map (Programmer x) allLanguages ++)
        -- = (++) $ map (Programmer x) allLanguages
        -- = (++) $ flip map allLanguages $ Programmer x

      -- f = (++) . flip map allLanguages . Programmer