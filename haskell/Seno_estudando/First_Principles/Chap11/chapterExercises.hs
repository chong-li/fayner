import Data.Char ( ord, chr, toUpper )

zipSpace :: String -> String -> [(Char, Char)]
zipSpace [] ms = []
zipSpace ks [] = []
zipSpace (k:ks) (' ':ms) = (' ', ' ') : zipSpace (k:ks) ms
zipSpace (k:ks) (m:ms) = (k, m) : zipSpace ks ms

vigenere :: String -> String -> String 
vigenere key message
  = map f pairs
  where
    pairs = zipSpace (cycle key) message
    f (_, ' ') = ' '
    f (k, m) = chr $ ((ord m - ord 'a' + ord k - ord 'a') `mod` 26) + ord 'a'

isSubseqOf :: Eq a => [a] -> [a] -> Bool 
isSubseqOf []     y  = True
isSubseqOf (x:xs) [] = False 
isSubseqOf t@(x:xs) (y:ys)
  | x == y    = isSubseqOf xs ys
  | otherwise = isSubseqOf t  ys

capitalizeWords :: String -> [(String, String)]
capitalizeWords string = zip listOfWords capWords
  where
    listOfWords = words string
    capWords = map cap listOfWords
    cap (x:xs) = toUpper x:xs

capitalizeWord :: String -> String
capitalizeWord [] = []
-- capitalizeWord (' ':xs) = ' ': capitalizeWord xs
capitalizeWord (x:xs) = toUpper x:xs

capitalizeParagraph :: String -> String
capitalizeParagraph = cap True
  where
    cap :: Bool -> String -> String
    cap _ [] = []
    cap True (' ':xs) = ' ' : cap True xs
    cap True (x:xs) = toUpper x : cap False xs
    cap False ('.':xs) = '.' : cap True xs
    cap False (x:xs) = x : cap False xs

--

data Expr
  = Lit Integer
  | Add Expr Expr

eval :: Expr -> Integer
eval (Lit n) = n
eval (Add x y) = (+) (eval x) (eval y)

printExpr :: Expr -> String 
printExpr (Lit n) = show n
printExpr (Add x y) = printExpr x ++ " + " ++ printExpr y

--

type Digit = Char
type Presses = Int

validButtons :: [Digit]
validButtons = "1234567890*#"

data Symbol = Capitalize | Symbol String
newtype DaPhone = DaPhone [(Digit, Symbol)]

phone :: DaPhone
phone = DaPhone $ zip validButtons correspondent
  where
    correspondent = (Symbol <$> ["", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv",
                                "wxyz", " "])
                    ++ [Capitalize] ++ (Symbol <$> [".,"])

reverseTaps :: DaPhone -> Char-> [(Digit, Presses)]
reverseTaps = undefined

