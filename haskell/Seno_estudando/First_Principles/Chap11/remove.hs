{-# LANGUAGE ScopedTypeVariables #-}

remove :: forall a. Eq a => a -> [a] -> [a]
remove x = foldr f []
  where
    f :: Eq a => a -> [a] -> [a]
    f y = if x == y then id else (y :)