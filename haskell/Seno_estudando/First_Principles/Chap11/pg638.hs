-- data Fiction = Fiction deriving (Show)

-- data Nonfiction = Nonfiction deriving (Show)

data BookType
  = Fiction
  | Nonfiction
  deriving (Show)

type AuthorName = String

-- Not in Normal Form:
data Author = Author AuthorName BookType

-- Normal form is given by distributive:
data Author'
  = Fiction' AuthorName
  | Nonfiction' AuthorName
  deriving (Show)
