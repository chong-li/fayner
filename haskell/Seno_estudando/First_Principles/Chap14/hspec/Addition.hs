module Addition where

import Test.Hspec
import Test.QuickCheck

-- hspec    :: Spec -> IO ()
-- describe :: String -> SpecWith a -> SpecWith a
-- it       :: ()                                       -- Must be wrong
-- shouldBe :: (Show a, Eq a) => a -> a -> Expectation

test1 :: IO ()
test1 = hspec $ do
  describe "Addition" $ do
    it "1 + 1 is greater than 1" $ do
      (1 + 1) > (1 :: Integer) `shouldBe` True -- (1 :: Integer) to avoid WARNING

--- // ---

test2 :: IO ()
test2 = hspec $ do
  describe "Addition" $ do
    it "1 + 1 is greater than 1" $ do
      (1 + 1) > (3 :: Integer) `shouldBe` True 
    it "2 + 2 is equal to 4" $ do
      (2 + 2) `shouldBe` (4 :: Integer)
      (3 + 4) `shouldBe` 5

--- // ---

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
  where
    go n d count
      | n < d = (count, n)
      | otherwise = go (n - d) d (count + 1)

test3 :: IO ()
test3 = hspec $ do
  describe "Division" $ do
    it "15 divided by 3 is 5" $ do
      dividedBy (15 :: Int) 3 `shouldBe` (5, 0)
    it "22 divided by 5 is\
      \ 4 remainder 2" $ do
    dividedBy (22 :: Integer) 5 `shouldBe` (4, 2)

--- // ---

times :: (Eq a, Num a) => a -> a -> a
times m n
  | m == 0 = 0
  | otherwise = n + times (m - 1) n

-- >>> 2 `times` 3

-- >>> 2 + 2

test4 :: IO ()
test4 = hspec $ do
  describe "Addition" $ do
    it "5 times 3 is 15" $ do
      (5 :: Integer) `times` 3 `shouldBe` 15
    it "22 times 5 is\
      \ 110" $ do
      (22 :: Integer) `times` 5 `shouldBe` 110

--- // ---

-- QuickTest

-- property :: Testable prop => prop -> Property

test5 :: IO ()
test5 = hspec $ do
  describe "Addition" $ do
    it "x + 1 is always greater than x" $ do
      property $ \x -> x + 1 > (x :: Integer)

--- // ---

genBool :: Gen Bool
genBool = choose (False, True)

genBool' :: Gen Bool
genBool' = elements [False, True]

genOrdering :: Gen Ordering
genOrdering = elements [LT, EQ, GT]

genChar :: Gen Char
genChar = elements ['a'..'z']

-- >>> sample' $ choose (1, 7::Int)
-- [7,4,5,5,5,1,4,3,7,6,2]

prop_additionGreater :: Int -> Bool
prop_additionGreater x = (x + 10)^2 - 1 > x + 10
-- quickCheck :: Testable prop => prop -> IO ()

runQc :: IO ()
runQc = quickCheck prop_additionGreater


