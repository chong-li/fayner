# Testing
## 14.1 Testing
- Well-specified types can enable
programmers to avoid many obvious and tedious tests that
might otherwise be necessary to maintain in untyped program-
ming languages

## 14.2
- For the sake of simplicity, we’ll say there are two broad cate-
gories of testing: unit testing and property testing.
- We’ll focus on specification testing with the hspec library in
this chapter, but HUnit is also available.
- Property tests test the formal properties of programs without
requiring formal proofs by allowing you to express a truth-
valued, universally quantified (that is, will apply to all cases)
function — usually equality — which will then be checked
against randomly generated inputs.

## 14.3 Conventional testing
- create project

### Truth according to Hspec
- basic bla bla bla about Hspec

### Our first Hspec test
- Here, we’re nesting multiple do blocks. The types of the do
blocks passed to hspec, describe, and it aren’t IO () but some-
thing more specific to hspec. 
- We haven’t covered monads
yet, and this works fine without understanding precisely how
it works, so let’s just roll with it for now.
- shouldBe :: (Show a, Eq a) => a -> a -> Expectation
- examples done

### Intermission: Short Exercise

## 14.4 QuickCheck
- test with x + 1 > (x :: Int)

### Arbitrary instances

- arbitrary :: Arbitrary a => Gen a

- sample :: Show a => Gen a -> IO ()

- sample (arbitrary :: Gen Int)
0
-1
0
3
-7
-3
-11
-10
0

- The IO is necessary because it’s using a global resource of
random values to generate the data.

- If you run sample arbitrary directly in GHCi without speci-
fying a type, it will default the type to () and give you a very
nice list of empty tuples.

- We can specify our own data for generating Gen values. In
this example, we’ll specify a trivial function that always returns
a 1 of type Int:

- sample (return 1)
1
1
1
1
1

- 


