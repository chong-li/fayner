import Data.Bifunctor (first, second)
import Data.Maybe (mapMaybe)

theToA :: String -> String
theToA (' ' : cs) = ' ' : theToA cs
theToA "the" = "a"
theToA x = x

replaceThe :: String -> String
replaceThe [] = []
replaceThe string = theToA hw ++ replaceThe rem
  where
    (hw, rem) = headWord True string
    headWord :: Bool -> String -> (String, String)
    headWord _ [] = ([], [])
    headWord True (' ' : cs) = first (' ' :) $ headWord True cs
    headWord False w@(' ' : cs) = ("", w)
    headWord _ (c : cs) = first (c :) $ headWord False cs

----

countVowels :: String -> Integer
countVowels x = toInteger $ length y
  where
    y = mapMaybe f x
    f c
      | c `elem` "aeiouAEIOU" = Just c
      | otherwise = Nothing

----

data Nat
  = Zero
  | Succ Nat
  deriving (Eq, Show)

natToInteger :: Nat -> Integer
natToInteger n = case n of
  Zero -> 0
  (Succ m) -> succ $ natToInteger m

integerToNat :: Integer -> Maybe Nat
integerToNat n
  | n < 0 = Nothing
  | n == 0 = Just Zero
  | otherwise = Succ <$> integerToNat (n -1)

---

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust (Just _) = True

isNothing :: Maybe a -> Bool
isNothing = not . isJust

---

mayybee :: b -> (a -> b) -> Maybe a -> b
mayybee b f (Just a) = f a
mayybee b f Nothing = b

---

fromMaybe :: a -> Maybe a -> a
fromMaybe a = mayybee a id

---

listToMaybe :: [a] -> Maybe a
listToMaybe [] = Nothing
listToMaybe (x : xs) = Just x

---

maybeToList :: Maybe a -> [a]
maybeToList Nothing = []
maybeToList (Just x) = [x]

---

catMaybes :: [Maybe a] -> [a]
catMaybes = foldr f []
  where
    f Nothing = id
    f (Just x) = (x :)

---

flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe = foldr f (Just [])
  where
    f _ Nothing = Nothing
    f Nothing _ = Nothing
    f (Just x) (Just y) = Just $ x : y

lefts' :: [Either a b] -> [a]
lefts' = foldr f []
  where
    f (Right y) = id
    f (Left x) = (x :)

rights' :: [Either a b] -> [b]
rights' = foldr f []
  where
    f (Right y) = (y :)
    f (Left x) = id

-- Stopped pg. 741

partitionEithers' :: [Either a b] -> ([a], [b])
partitionEithers' = foldr f ([], [])
  where
    f (Left a) = first (a :)
    f (Right b) = second (b :)

--

eitherMaybe' ::
  (b -> c) ->
  Either a b ->
  Maybe c
eitherMaybe' f x = case x of
  Left a -> Nothing
  Right b -> Just $ f b

either' ::
  (a -> c) ->
  (b -> c) ->
  Either a b ->
  c

either' f g x =
  case x of
    Left a -> f a
    Right b -> g b

eitherMaybe'' :: (b -> c)
  -> Either a b
  -> Maybe c

eitherMaybe'' h = either' f g
  where
    f = const Nothing 
    g = Just . h


---

myIterate :: (a -> a) -> a -> [a]
myIterate f a
  = a : myIterate f (f a)

---

myUnfoldr :: (b -> Maybe (a, b))
  -> b
  -> [a]
myUnfoldr f b
  = case f b of
      Nothing -> []
      Just (a, b') -> a : myUnfoldr f b'

betterIterate :: (a -> a) -> a -> [a]
betterIterate f = myUnfoldr (\x -> Just (x, f x))

-- 

data BinaryTree a =
  Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

unfold :: (a -> Maybe (a,b,a)) -> a -> BinaryTree b

unfold f a
  = case f a of
      Nothing -> Leaf
      Just (a1, b, a2) -> Node (unfold f a1) b (unfold f a2)

--

treeBuild :: Integer -> BinaryTree Integer
treeBuild n = unfold (\x -> if x >= n then Nothing else Just (x+1, x, x+1)) 0
