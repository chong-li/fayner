# Signaling adversity

## 12.1 Signaling adversity

## 12.2

- Explain to use Maybe when there is nothing sensible to return

### Smart constructors for datatypes

- Use Maybe as a alternative to the data constructor to impose restrictions:

```haskell
type Name = String
type Age = Integer

mkPerson :: Name -> Age -> Maybe Person
mkPerson name age
  | name /= "" && age >= 0 = Just $ Person name age
  | otherwise = Nothing
```

- `mkPerson` is what we call a *smart constructor*.

## 12.3 Bleating either

- Using either as a version of Maybe with more information

```haskell
data PersonInvalid = NameEmpty
                   | AgeTooLow
                   deriving (Eq, Show)

mkPerson :: Name -> Age -> Either PersonInvalid Person
```

- Case expressions and pattern
matching *will work* without an Eq instance, but guards using
(==) will not.

```haskell
mkPerson name age
  | name /= "" && age >= 0 = Right $ Person name age
  | name == "" = Left NameEmpty
  | otherwise = Left AgeTooLow
```

- We can signal the case of many errors:

```haskell
type ValidatePerson a = Either [PersonInvalid] a

data PersonInvalid = NameEmpty
  | AgeTooLow
  deriving (Eq, Show)

ageOkay :: Age -> Either [PersonInvalid] Age
ageOkay age = case age >= 0 of
  True -> Right age
  False -> Left [AgeTooLow]

nameOkay :: Name -> Either [PersonInvalid] Name
nameOkay name = case name /= "" of
  True -> Right name
  False -> Left [NameEmpty]

mkPerson :: Name -> Age -> ValidatePerson Person
  mkPerson name age =
  mkPerson' (nameOkay name) (ageOkay age)

mkPerson' :: ValidatePerson Name
            -> ValidatePerson Age
            -> ValidatePerson Person

mkPerson' (Right nameOk) (Right ageOk) =
  Right (Person nameOk ageOk)

mkPerson' (Left badName) (Left badAge) =
  Left (badName ++ badAge)

mkPerson' (Left badName) _ = Left badName
mkPerson' _ (Left badAge) = Left badAge
```

- Later in the book, we’ll be able to replace mkPerson
and mkPerson' with the following:

```haskell
mkPerson :: Name -> Age -> Validation [PersonInvalid] Person
mkPerson name age =
  liftA2 Person (nameOkay name) (ageOkay age)
```

- We use Left as our invalid or error constructor for a couple
of reasons. It is conventional to do so in Haskell, but that con-
vention came about for a reason. The reason has to do with the
ordering of type arguments and application of functions. Nor-
mally it is your error or invalid result that is going to cause a
stop to whatever work is being done by your program. Functor
will not map over the left type argument because it has been
applied away. (...)

- Tá mais para tem razão para *alguma* conven,ão do que para *essa* convenção

## 12.4 Kinds, a thousand stars in your types

- Acho que está só nesse capítulo pq Maybe é um exemplo de  * -> * e Either de * -> * -> *.
Tem mais exemplos de higher kinded types na pg 720.

- Mais uma coisa mal escrita, meio nota de rodapé,
mas potencialmente importante pro futuro:

"**Lifted and unlifted types** To be precise, kind * is the kind of
all standard lifted types, while types that have the kind # are
unlifted. A lifted type, which includes any datatype you could
define yourself, is any that can be inhabited by bottom. Lifted
types are represented by a pointer and include most of the
datatypes we’ve seen and most that you’re likely to encounter
and use. 
*Unlifted types are any type which cannot be inhabited by bottom.*
Types of kind # are often native machine types
and raw pointers."

"Newtypes are a special case in that they are
kind *, but are unlifted because their representation is identical
to that of the type they contain, so the newtype itself is not
creating any new pointer beyond that of the type it contains.
That fact means that the newtype itself cannot be inhabited
by bottom, only the thing it contains can be, so newtypes are
unlifted. The default kind of concrete, fully-applied datatypes
in GHC is kind *."

- Prelude> :k Maybe Maybe
Expecting one more argument to ‘Maybe’
The first argument of ‘Maybe’ should have kind ‘*’,
but ‘Maybe’ has kind ‘* -> *’

- Prelude> data Example a = Blah | RoofGoats | Woot a
Prelude> :k Maybe Example
Expecting one more argument to ‘Example’
The first argument of ‘Maybe’ should have kind ‘*’,
but ‘Example’ has kind ‘* -> *’

- "Note that the list type constructor [] is also kind * -> * and
otherwise unexceptional save for the bracket syntax that lets
you type [a] and [Int] instead of [] a and [] Int."

- Prelude> :k Maybe []
Expecting one more argument to ‘[]’
The first argument of ‘Maybe’ should have kind ‘*’,
but ‘[]’ has kind ‘* -> *’

Prelude> :k Maybe [Bool]
Maybe [Bool] :: *

### Data constructors are functions

- Não sei pq o livro falou isso agora, pra mim já era óbvio.

- Prelude> fmap Just [1, 2, 3]
[Just 1,Just 2,Just 3]

- Prelude> data Unary a = Unary a deriving Show
Prelude> :t Unary
Unary :: a -> Unary a

- And again, this works just like a function, except the type
of the argument can be whatever we want.

Bah, isso pode acontecer com funções tb

- Prelude> :info Unary
data Unary a = Unary a
instance Show a => Show (Unary a)

- Prelude> :t (Unary id)
(Unary id) :: Unary (t -> t)
-- id doesn't have a Show instance
Prelude> show (Unary id)
<interactive>:53:1:
No instance for (Show (t0 -> t0))

- Another thing to keep in mind is that you can’t ordinarily
hide polymorphic types from your type constructor, so the
following is invalid:

```
Prelude> data Unary = Unary a deriving Show
Not in scope: type variable ‘a’
```

In order for the type variable a to be in scope, we usually
need to introduce it with our type constructor. There are ways
around this, but they’re rarely necessary 

## 12.5 Chapter Exercises

### Unfolds

- While the idea of catamorphisms is still relatively fresh in our
minds, let’s turn our attention to their dual: anamorphisms.

- This may have given you a mild headache, but you may
also see that this same principle of abstracting out common
patterns and giving them names applies as well to unfolds as
it does to folds.

Mostre os exemplos, quero ver!