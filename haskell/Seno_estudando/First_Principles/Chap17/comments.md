
* context vs container


* liftA2
A minimal complete definition must include implementations of pure and of either <*> or liftA2. If it defines both, then they must behave the same as their default definitions:

```haskell
(<*>) = liftA2 id
liftA2 f x y = f <$> x <*> y
```

```
liftA2 (+) [1,2,3] [3,0] = [4, 1, 5, 2, 6, 3]
liftA2 f l1 l2 = pure f <*> l1 <*> l2

Objetivo: definir o <*> apartir do liftA2
[(+1), (+2), (+3)] <*> [3, 0] = [4, 1, 5, 2, 6, 3]


Just (+2) <*> Just 3 = Just 5
liftA2 id (Just (+2)) (Just 3) = 
pure id <*> Just (+2) <*> Just 3 = 
Just id <*> Just (+2) <*> Just 3 = 
Just (+2) <*> Just 3

Nothing <*> _ = Nothing 
_ <*> Nothing = Nothing 

Just f <*> Just x = 

```

Why not ``(<*>) = liftA2 ($)``?

https://hackage.haskell.org/package/base-4.15.0.0/docs/Control-Applicative.html#t:Applicative

* liftA
https://hackage.haskell.org/package/base-4.15.0.0/docs/Control-Applicative.html#v:liftA

* Applicative for the two-tuple

```haskell
instance Monoid a => Applicative ((,) a)
instance (Monoid a, Monoid b) => Monoid (a, b)

("Woo", (+1)) <*> (" Hoo!", 0) == ("Woo Hoo!", 1)
```

* Applicative for list:

```haskell
(,) <$> [1, 2] <*> [3, 4] == [(1,3),(1,4),(2,3),(2,4)] == liftA2 (,) [1, 2] [3, 4]

max <$> [1, 2] <*> [1, 4] == [1,4,2,4]
```

* ``lookup`` function
lookup :: Eq a => a -> [(a, b)] -> Maybe b

* Indentity type

The Identity type here is a way to introduce structure without
changing the semantics of what you’re doing.

Prelude> let xs = [1, 2, 3]
Prelude> let xs' = [9, 9, 9]
Prelude> const <$> xs <*> xs'
[1,1,1,2,2,2,3,3,3]
Prelude> let mkId = Identity
Prelude> const <$> mkId xs <*> mkId xs'
Identity [1,2,3]

* Constant type

However, it is also something that,
like Identity has real use cases, and you will see it in other
people’s code.

And here are some examples of how it works. These are,
yes, a bit contrived, but showing you real code with this in it
would probably make it much harder for you to see what’s
going on:

Prelude> let f = Constant (Sum 1)
Prelude> let g = Constant (Sum 2)
Prelude> f <*> g
Constant {getConstant = Sum {getSum = 3}
Prelude> Constant undefined <*> g
Constant (Sum {getSum =
*** Exception: Prelude.undefined
Prelude> pure 1
1
Prelude> pure 1 :: Constant String Int
Constant {getConstant = ""}