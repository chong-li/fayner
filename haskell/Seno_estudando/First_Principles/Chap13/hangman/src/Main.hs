module Main where

import Control.Monad (forever) 
import Data.Char (toLower) 
import Data.Maybe (isJust, fromMaybe) 
import Data.List (intersperse) 
import System.Exit (exitSuccess) 
import System.Random

type WordList = [String]

allWords :: IO WordList
allWords = do
  dict <- readFile "data/dict.txt"
  return (lines dict)

minWordLength :: Int
minWordLength = 5

maxWordLength :: Int
maxWordLength = 9

gameWords :: IO WordList
gameWords = filter gameLength <$> allWords
  where
    gameLength w
      = let l = length w in
        minWordLength <= l && l < maxWordLength

randomWord :: WordList -> IO String
randomWord wl = (wl !!) <$> randomRIO ( 0, length wl - 1 )

randomWord' :: IO String
randomWord' = gameWords >>= randomWord

data Puzzle = Puzzle String [Maybe Char] [Char]

instance Show Puzzle where
  show (Puzzle _ discovered guessed) =
    intersperse ' '
    (fmap renderPuzzleChar discovered)
    ++ " Guessed so far: " ++ guessed

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar = fromMaybe '_'

freshPuzzle :: String -> Puzzle
freshPuzzle word = Puzzle word (map (const Nothing) word) []

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle word _ _) c = c `elem` word

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ _ guessed) c = c `elem` guessed






main :: IO ()
main = do
  print =<< randomRIO (0:: Integer, 100)
  putStrLn "hello world"
