module Exercise where

wax0n :: Integer
wax0n = x * 5
  where 
    x = y ^ 2 
    y = z + 8 
    z = 7

times :: Integer -> Integer -> Integer
times x n = x * n

triple :: Num a => a -> a
triple x = x * 3

f :: Monoid a => (a, a) -> a
f (x, y) = x <> y

wax0ff :: Num a => a -> a
wax0ff = triple

data Union = I Integer | S String 
