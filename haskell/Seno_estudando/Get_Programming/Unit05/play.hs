{-# LANGUAGE InstanceSigs #-}

-- # Efeitos colaterais 101
-- Vamos manter um log
type Log = [String]
data Annotated x = Annotated Log x deriving Show

-- A ideia é que um ``Annotate Int`` seja um número junto com um log (lista de mensagens) de tudo que aconteceu até ele ser calculado.

a :: Annotated Int 
a = Annotated ["New number"] 2

-- Primeiramente, ``Annotated`` é claramente um funtor:
instance Functor Annotated where
  fmap f (Annotated log x) = Annotated log (f x)

-- O fmap simplesmente ignora o log. A gente precisa de um jeito de anotar o que aconteceu:
addToLog :: String -> Annotated x -> Annotated x
addToLog msg (Annotated log x) = Annotated (log ++ [msg]) x
-- annotate :: String -> x -> Annotated x
-- annotate msg x = Annotated [msg] x

b :: Annotated Int
b = addToLog "Find the triple" $ fmap (3*) a

-- Agora vamos dar uma instância de Applicative para ``Annotated``, i.e, transformar num "funtor para várias variáveis":
instance Applicative Annotated where
  pure x = Annotated [] x
  -- liftA2 f (Annotated log x) (Annotated log' y) = Annotated (log ++ log') (f x y)
  (Annotated log_func f) <*> (Annotated log_arg x) = Annotated (log_arg ++ log_func) (f x)

c :: Annotated Int
c = addToLog "New number" (pure 10)

add :: Annotated (Int -> Int -> Int)
add = addToLog "Add" (pure (+))

d :: Annotated Int
d = add <*> a <*> c

-- Finalmente, vamos dar uma instância de Mônada. Nesse caso temos que dar uma interpretação/simplificação de "anotação da anotação" como "apenas uma anotação"

join :: Annotated (Annotated x) -> Annotated x
join (Annotated log0 (Annotated log1 x)) = Annotated (log0 ++ log1) x

instance Monad Annotated where
  (Annotated log0 x) >>= f
    = case f x of
      Annotated log1 y -> Annotated (log0 ++ log1) y