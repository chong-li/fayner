import Text.Read ( readMaybe )
import Data.Functor.Compose

data User = User
  { name :: String
  , gamerId :: Int
  , score :: Int
  } deriving Show

data User2 = User2
  { gamerId2 :: Int
  , score2 :: Int
  } deriving Show

serverUsername :: Maybe String
serverUsername = Just "Sue"

serverGamerId :: Maybe Int
serverGamerId = Just 1337

serverScore :: Maybe Int
serverScore = Just 9001

maybeUser :: Maybe User
maybeUser = User <$> serverUsername <*> serverGamerId <*> serverScore

-----------------------------------
-- https://hackage.haskell.org/package/base-4.15.0.0/docs/Data-Functor-Compose.html

readInt :: IO (Maybe Int)
readInt = readMaybe <$> getLine

readIntC :: Compose IO Maybe Int
readIntC = Compose $ readMaybe <$> getLine

main :: IO ()
main = do
  putStrLn "Enter a username, gamerId and score"
  user <- getCompose $ User2 <$> readIntC <*> readIntC
  print user