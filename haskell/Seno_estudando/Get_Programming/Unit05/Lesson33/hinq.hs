{-# LANGUAGE ApplicativeDo #-}

import Control.Applicative
import Control.Monad

-- data types

import Distribution.PackageDescription.FieldGrammar (validateTestSuite)
import GHC.Base (Applicative)
import Control.Applicative (Alternative)
data Name = Name
  { firstName ::String
  , lastName :: String }

instance Show Name where
  show (Name first last) = mconcat [first," ",last]

data GradeLevel = Freshman
  | Sophmore
  | Junior
  | Senior deriving (Eq,Ord,Enum,Show)

data Student = Student
  { studentId :: Int
  , gradeLevel :: GradeLevel
  , studentName :: Name } deriving Show

data Teacher = Teacher
  { teacherId :: Int
  , teacherName :: Name } deriving Show

-- tables

students :: [Student]
students = [
   Student 1 Senior (Name "Audre" "Lorde")
  ,Student 2 Junior (Name "Leslie" "Silko")
  ,Student 3 Freshman (Name "Judith" "Butler")
  ,Student 4 Senior (Name "Guy" "Debord")
  ,Student 5 Sophmore (Name "Jean" "Baudrillard")
  ,Student 6 Junior (Name "Julia" "Kristeva")]

teachers :: [Teacher]
teachers = [Teacher 100 (Name "Simone" "De Beauvior")
           ,Teacher 200 (Name "Susan" "Sontag")]

data Course = Course
  { courseId :: Int
  , courseTitle :: String
  , teacher :: Int } deriving Show

courses :: [Course]
courses = [Course 101 "French" 100
  ,Course 201 "English" 200]
-- functions

_select :: Functor m => (a -> b) -> m a -> m b
_select = fmap

test :: Alternative m => (a -> Bool) -> m a -> m a
test cond a = 
  do
    val <- a
    guard True
    pure val

guard' :: Alternative f => (a -> Bool) -> a -> f a
guard' cond val = if cond val then pure val else empty



_where :: (Alternative m, Monad m) => (a -> Bool) -> m a -> m a
-- _where = filter
_where cond table = do
  val <- table
  guard (cond val)
  pure val

startsWith :: Char -> String -> Bool
startsWith char string = char == head string

--

_join :: Eq c => [a] -> [b] -> (a -> c) -> (b -> c) -> [(a,b)]
_join table1 table2 col1 col2 = do
  entry1 <- table1
  entry2 <- table2
  guard (col1 entry1 == col2 entry2)
  return (entry1, entry2)


