newtype Constant a b =
  Constante { getConstant :: a }
  deriving (Eq, Ord, Show)

instance Functor (Constant a) where
  -- f :: b -> c
  -- Constant a :: * -> *
  -- Constante azinho :: Constant azao bzao 
  fmap f (Constante azinho) = Constante azinho

-- Parece que rola um aplicativo sem pure:
-- instance ApplicativeWithNoPure (Constant azao) where
--   (Constante x) <*> (Constante y) = Constante x

instance Monoid azao => Applicative (Constant azao) where
  (Constante x) <*> (Constante y) = Constante (x <> y)
  pure x = Constante mempty