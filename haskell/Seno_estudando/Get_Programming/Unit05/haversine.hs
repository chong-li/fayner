import qualified Data.Map as Map

type LatLong = (Double,Double)
locationDB :: Map.Map String LatLong
locationDB = Map.fromList [("Arkham",(42.6054,-70.7829))
                          ,("Innsmouth",(42.8250,-70.8150))
                          ,("Carcosa",(29.9714,-90.7694))
                          ,("New York",(40.7776,-73.9691))]

-- Haversine formula

toRadians :: Double -> Double
toRadians degrees = degrees * pi / 180

latLongToRads :: LatLong -> (Double,Double)
latLongToRads (lat,long) = (rlat,rlong)
  where
    rlat = toRadians lat
    rlong = toRadians long

haversine :: LatLong -> LatLong -> Double
haversine coords1 coords2 = earthRadius * c
  where
    (rlat1,rlong1) = latLongToRads coords1
    (rlat2,rlong2) = latLongToRads coords2
    dlat = rlat2 - rlat1
    dlong = rlong2 - rlong1
    a = sin (dlat/2) ^2 + cos rlat1 * cos rlat2 * sin (dlong/2) ^2
    c = 2 * atan2 (sqrt a) (sqrt (1-a))
    earthRadius = 6357.5

printDistance :: Maybe Double -> IO ()
printDistance Nothing = putStrLn "Error, invalid city entered"
printDistance (Just distance) = putStrLn (show distance ++ " kilometers")

-- With no applicative:

haversineMaybe :: Maybe LatLong -> Maybe LatLong -> Maybe Double
haversineMaybe Nothing _ = Nothing
haversineMaybe _ Nothing = Nothing
haversineMaybe (Just val1) (Just val2) = Just (haversine val1 val2)

-- Could we use <$>?

-- The <*> operator (pronounced app):


main :: IO ()
main = do
  putStrLn "Enter the starting city name:"
  startingInput <- getLine
  -- Map.lookup :: String -> Map.Map String LatLong -> Maybe LatLong
  let startingCity = Map.lookup startingInput locationDB
  putStrLn "Enter the destination city name:"
  destInput <- getLine
  let destCity = Map.lookup destInput locationDB
  let distance = haversine <$> startingCity <*> destCity -- haversineMaybe!!
  printDistance distance
  
-- You just wrote a pro-
-- gram that handles missing values well, but not once did you have to check whether a
-- value was null using conditionals, or worry about exception handling. Even better, you
-- could write the core functionality of your program, haversine, as though nothing in the
-- world might go wrong.

-- Comment about other languages:

-- Haskell’s type system makes it impossible for you to accidentally pass a Maybe LatLong to
-- haversine. In nearly every other programming language, even those with static types such
-- as Java and C#, there’s no way to ensure that a null value doesn’t sneak into a function.
-- Functor and Applicative complement this safety by making it easy to mix regular functions
-- such as haversine with Maybe types or IO types, without compromising that safety.