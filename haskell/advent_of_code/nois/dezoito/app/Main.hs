module Main where

import Data.List.Split

evaluate :: String -> Int 
evaluate [] = error "No string"

data Token = Value Int | Add | Mult | OP | CP deriving Show

insertSpaces :: String -> String
insertSpaces ('(':xs) = "( " ++ insertSpaces xs
insertSpaces (')':xs) = " )" ++ insertSpaces xs
insertSpaces [] = []
insertSpaces (x:xs) = x : insertSpaces xs

tokenize :: String -> [Token]
tokenize = map translate . splitOn " " . insertSpaces
  where
    translate :: String -> Token
    translate "+" = Add
    translate "*" = Mult
    translate "(" = OP
    translate ")" = CP
    translate x   = Value $ read x

execute :: [Token] -> Int
execute [] = error "lista vazia"
execute [ Value x ] = x
execute (Value x : Add : Value y : remaining) = execute $ Value (x + y) : remaining
execute (Value x : Mult : Value y : remaining) = execute $ Value (x * y) : remaining
execute _ = error "Expressao mal formada"

execute1 :: [Token] -> Int
execute1 [] = error "lista vazia"
execute1 [ Value x ] = x
execute1 xs = execute1 $ execute2 xs

execute2 :: [Token] -> [Token]

execute2 (Value x : Add : Value y : remaining) = Value (x + y) : remaining
execute2 (Value x : Add : OP : remaining) = Value x : Add : execute2 (OP : remaining)

execute2 (Value x : Mult : Value y : remaining) = Value (x * y) : remaining
execute2 (Value x : Mult : OP : remaining) = Value x : Mult : execute2 (OP : remaining)

execute2 (OP : Value x : CP : remaining) =  Value x : remaining
execute2 (OP : remaining) = OP : execute2 remaining
execute2 (Value x : CP : remaining) = Value x : CP : remaining
-- execute2 x = x

expression = "3 + 2 * (4 + 2 + (1 * 2)) + 2"

-- 3 + 2 * ( 4 + 2 + (1 * 2)) + 2
-- 5 * ( 4 + 2 + (1 * 2)) + 2
--     ( 4 + 2 + (1 * 2)) + 2
--       4 + 2 + (1 * 2)) + 2
--       6 + (1 * 2)) + 2
-- 5 * (6 + (1 * 2)) + 2
--           (1 * 2)) + 2
--            1 * 2)) + 2
--            2)) + 2
--           (2)) + 2
--           2) + 2
--       6 + 2) + 2
--       8) + 2
--     ( 8) + 2
-- 5 * 8 + 2
-- 40 + 2
-- 42


-- 2 * (4 + 1)  



main :: IO ()
main = print . execute1 $ tokenize expression