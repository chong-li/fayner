import Data.List.Split

evaluate :: String -> Int 
evaluate [] = error "No string"

expression = "9 + 4 * 9 * 4 + 3 + 17 * 8"

data Token = Value Int | Add | Mult deriving Show

tokenize :: String -> [Token]
tokenize = map translate . splitOn " "
  where
    translate :: String -> Token
    translate "+" = Add
    translate "*" = Mult
    translate x   = Value $ read x

