main = interact $ (++ "\n") . show . solve . const input


inputTest = (5764801, 17807724)
input = (9789649, 3647239)

param = 20201227
inv = 7^(param-2) `mod` param :: Integer

transf :: Integer -> Integer
transf x = (x * inv) `mod` param

solve (a, b) = iter 0 (a, b)
  where
      iter count (1, y) = b^count `mod` param
      iter count (x, 1) = a^count `mod` param
      iter count (x, y) = iter (count+1) (transf x, transf y)