-- compila com:
-- stack ghc -- 03.hs -o zero 3
--
-- depois passa a string pelo bash rodando o programa compilado:
-- cat input03 | ./zero3


input_test = ["..##.......",
              "#...#...#..",
              ".#....#..#.",
              "..#.#...#.#",
              ".#...##..#.",
              "..#.##.....",
              ".#.#.#....#",
              ".#........#",
              "#.##...#...",
              "#...##....#",
              ".#..#...#.#"
              ]

test_len = length $ head input_test

slope_list = [(1,1), (3,1), (5,1), (7,1), (1,2)]


slopes css r d = [
    ((r*n `div` d) `mod` (length $ head css), n `mod` d, cs) | (n, cs) <- (zip [0..] css)
    ]

checkTree (r, d, cs)
    | d == 0 && cs !! r == '#'   = True
    | otherwise                  = False

trees css (r, d) = length $ filter (==True) (checkTree <$> slopes css r d)

--xs = (trees input_test) <$> slope_list

--foldr (*) 1 xs

solve slope_list file_text = tiddy $ foldr (*) 1 trees_in_path
    where trees_in_path = (trees $ words file_text) <$> slope_list
          tiddy n = (show n) ++ "\n"
          

main = interact (solve slope_list)