main = interact $ output . solve . input

---- data --------

data SeatState = Empty | Occup | Floor deriving Show
data Seat = Seat SeatState Bool deriving Show

---- input -------
input :: String -> [(Int, Int, Seat)]
input = index . getSeatsMap . lines

toSeat :: Char -> Seat
toSeat '#' = Seat Occup True
toSeat 'L' = Seat Empty True
toSeat '.' = Seat Floor True
toSeat _ = error "no seat for u"

rowsToSeats :: String -> [Seat]
rowsToSeats = map toSeat

getSeatsMap :: [String] -> [[Seat]]
getSeatsMap = map rowsToSeats

enumerate :: [a] -> [(Int, a)]
enumerate = zip [0..]

insertToTriple :: a -> (b, c) -> (a, b, c)
insertToTriple r (c, a) = (r, c, a)

index :: [[a]] -> [(Int, Int, a)]
index = concat . (map insertRowIndex) . enumerate . (map enumerate)
    where -- insertRowIndex :: (Int, [(Int, a)]) -> [(Int, Int, a)]
        insertRowIndex (r, xs) = map (insertToTriple r) xs

---- solve -------
solve :: [(Int, Int, Seat)] -> Int
solve = countOccup . (fixedPoint anySwitched (newSeat 5)) . (filter isNotFloor)

----- solve1 : (newSeat 4) and occupAdj = occupAdj1
----- solve2 : (newSeat 5) and occupAdj = occupAdj2

occupAdj = occupAdj2

isAdj :: (Int, Int, a) -> (Int, Int, a) -> Bool
isAdj (r0, c0, _) (r1, c1, _)
    | r0 == r1 && c0 == c1 = False
    | otherwise            = (abs (r1-r0) <= 1) && (abs (c1-c0) <= 1)

isNotFloor :: (Int, Int, Seat) -> Bool
isNotFloor (_, _, Seat Floor _) = False
isNotFloor _                    = True

isOccup :: (Int, Int, Seat) -> Bool
isOccup (_, _, Seat Occup _) = True
isOccup _                    = False

countOccup :: [(Int, Int, Seat)] -> Int
countOccup xs = length $ filter isOccup xs

switchedState :: (Int, Int, Seat) -> Bool
switchedState (_, _, Seat _ b) = b

anySwitched :: [(Int, Int, Seat)] -> Bool
anySwitched = any switchedState

getAdj :: (Int, Int, a) -> [(Int, Int, a)] -> [(Int, Int, a)]
getAdj seat = filter (isAdj seat)

getAdj2 :: (Int, Int, a) -> [(Int, Int, a)] -> [Maybe (Int, Int, a)]
getAdj2 (r, c, _) xs = [fromLine r c xs | fromLine <- fstFromFuncs]

fstFromFuncs = maybeFstFromLine <$> [fstFromUL,
                                     fstFromUU,
                                     fstFromUR,
                                     fstFromRR,
                                     fstFromDR,
                                     fstFromDD,
                                     fstFromDL,
                                     fstFromLL]

maybeFstFromLine
    :: (Int -> Int -> Maybe (Int,(Int,Int,a))-> [(Int,Int,a)] -> Maybe (Int,Int,a))
    -> Int
    -> Int
    -> [(Int,Int,a)]
    -> Maybe (Int, Int, a)
maybeFstFromLine fstFrom r c xs = fstFrom r c Nothing xs

fstFromUL
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromUL _ _ Nothing [] = Nothing
fstFromUL _ _ (Just (_, x)) [] = Just x
fstFromUL r c Nothing ((r1,c1,s):xs)
    | c-c1 == k && k > 0           = fstFromUL r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromUL r c Nothing xs
    where k = r-r1
fstFromUL r c (Just (k0, t0)) ((r1,c1,s):xs)
    | c-c1 == k && k > 0 && k < k0 = fstFromUL r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromUL r c (Just (k0, t0)) xs
    where k = r-r1

fstFromUU
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromUU _ _ Nothing [] = Nothing
fstFromUU _ _ (Just (_, x)) [] = Just x
fstFromUU r c Nothing ((r1,c1,s):xs)
    | c1 == c && k > 0           = fstFromUU r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromUU r c Nothing xs
    where k = r-r1
fstFromUU r c (Just (k0, t0)) ((r1,c1,s):xs)
    | c1 == c && k > 0 && k < k0 = fstFromUU r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromUU r c (Just (k0, t0)) xs
    where k = r-r1

fstFromUR
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromUR _ _ Nothing [] = Nothing
fstFromUR _ _ (Just (_, x)) [] = Just x
fstFromUR r c Nothing ((r1,c1,s):xs)
    | c1-c == k && k > 0           = fstFromUR r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromUR r c Nothing xs
    where k = r-r1
fstFromUR r c (Just (k0, t0)) ((r1,c1,s):xs)
    | c1-c == k && k > 0 && k < k0 = fstFromUR r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromUR r c (Just (k0, t0)) xs
    where k = r-r1

fstFromRR
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromRR _ _ Nothing [] = Nothing
fstFromRR _ _ (Just (_, x)) [] = Just x
fstFromRR r c Nothing ((r1,c1,s):xs)
    | r1 == r && k > 0           = fstFromRR r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromRR r c Nothing xs
    where k = c1-c
fstFromRR r c (Just (k0, t0)) ((r1,c1,s):xs)
    | r1 == r && k > 0 && k < k0 = fstFromRR r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromRR r c (Just (k0, t0)) xs
    where k = c1-c

fstFromDR
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromDR _ _ Nothing [] = Nothing
fstFromDR _ _ (Just (_, x)) [] = Just x
fstFromDR r c Nothing ((r1,c1,s):xs)
    | c1-c == k && k > 0           = fstFromDR r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromDR r c Nothing xs
    where k = r1-r
fstFromDR r c (Just (k0, t0)) ((r1,c1,s):xs)
    | c1-c == k && k > 0 && k < k0 = fstFromDR r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromDR r c (Just (k0, t0)) xs
    where k = r1-r

fstFromDD
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromDD _ _ Nothing [] = Nothing
fstFromDD _ _ (Just (_, x)) [] = Just x
fstFromDD r c Nothing ((r1,c1,s):xs)
    | c1 == c && k > 0           = fstFromDD r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromDD r c Nothing xs
    where k = r1-r
fstFromDD r c (Just (k0, t0)) ((r1,c1,s):xs)
    | c1 == c && k > 0 && k < k0 = fstFromDD r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromDD r c (Just (k0, t0)) xs
    where k = r1-r

fstFromDL
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromDL _ _ Nothing [] = Nothing
fstFromDL _ _ (Just (_, x)) [] = Just x
fstFromDL r c Nothing ((r1,c1,s):xs)
    | c-c1 == k && k > 0           = fstFromDL r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromDL r c Nothing xs
    where k = r1-r
fstFromDL r c (Just (k0, t0)) ((r1,c1,s):xs)
    | c-c1 == k && k > 0 && k < k0 = fstFromDL r c (Just (k, (r1,c1,s))) xs
    | otherwise                    = fstFromDL r c (Just (k0, t0)) xs
    where k = r1-r

fstFromLL
    :: Int 
    -> Int
    -> Maybe (Int,(Int,Int,a))
    -> [(Int, Int, a)]
    -> Maybe (Int, Int, a)
fstFromLL _ _ Nothing [] = Nothing
fstFromLL _ _ (Just (_, x)) [] = Just x
fstFromLL r c Nothing ((r1,c1,s):xs)
    | r1 == r && k > 0           = fstFromLL r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromLL r c Nothing xs
    where k = c-c1
fstFromLL r c (Just (k0, t0)) ((r1,c1,s):xs)
    | r1 == r && k > 0 && k < k0 = fstFromLL r c (Just (k, (r1,c1,s))) xs
    | otherwise                  = fstFromLL r c (Just (k0, t0)) xs
    where k = c-c1

occupAdj1 :: (Int, Int, Seat) -> [(Int, Int, Seat)] -> Int
occupAdj1 s = countOccup . (getAdj s)

occupAdj2 :: (Int, Int, Seat) -> [(Int, Int, Seat)] -> Int
occupAdj2 s xs = length $ filter (==True) $ map (maybe False isOccup) (getAdj2 s xs)

newSeat :: Int -> (Int, Int, Seat) -> [(Int, Int, Seat)] -> (Int, Int, Seat)
newSeat _ (r, c, Seat Floor _) _               = (r, c, Seat Floor False)
newSeat i (r, c, Seat Occup b) seats
    | occupAdj (r, c, Seat Occup b) seats >= i = (r, c, Seat Empty True)
    | otherwise                                = (r, c, Seat Occup False)
newSeat _ (r, c, Seat Empty b) seats
    | occupAdj (r, c, Seat Empty b) seats == 0 = (r, c, Seat Occup True)
    | otherwise                                = (r, c, Seat Empty False)

nextState :: (a -> [a] -> a) -> [a] -> [a]
nextState f xs = map (`f` xs) xs

fixedPoint :: ([a] -> Bool) -> (a -> [a] -> a) -> [a] -> [a]
fixedPoint ended transit xs
    | ended ys = fixedPoint ended transit ys
    | otherwise      = ys
    where ys = nextState transit xs

---- output ------

output = (++ "\n") . show

---- test --------

test = (solve . input) inputTest


inputTest ="L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL"