--- test
inputTest = "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576"

out = (output . (solve 5) . input) inputTest

------ input --------
input :: String -> [Integer]
input = (map readInteger) . words
    
readInteger i = (read i :: Integer)

------ solve -------------------

solve = solve2

solve1 :: Int -> [Integer] -> Integer
solve1 i = findFalse . incNothing . (splitAt i) -- 258585477

solve2 i = findContiguos . (incFake i)          -- 36981213

incNothing :: ([Integer], [Integer]) -> ([Integer], [Integer], Maybe Integer)
incNothing (x, y) = (x, y, Nothing)

isSum :: (Num a, Eq a) => [a] -> a -> Bool
isSum [] _ = False
isSum xs y = case [True | x <- xs, (y-x) `elem` xs && (y-x) /= x] of
    [] -> False
    _  -> True

processOne :: ([Integer], [Integer], Maybe Integer) -> ([Integer], [Integer], Maybe Integer)
processOne (xs, [], m) = (xs, [], m)
processOne (xs, (y:ys), m)
    | isSum xs y = ((tail xs)++[y], ys, m)
    | otherwise  = (xs, (y:ys), Just y)

findFalse :: ([Integer], [Integer], Maybe Integer) -> Integer
findFalse t = case processOne t of
    (_, _, Just y) -> y
    (xs, ys, m)    -> findFalse (xs, ys, m)

incFake :: Int -> [Integer] -> (Integer, [Integer], [Integer], Integer)
incFake i (xs) = (0, [], xs, solve1 i xs)

findContiguos :: (Integer, [Integer], [Integer], Integer) -> Integer
findContiguos (_, [], (x0:x1:xs), y) = findContiguos (x0+x1, [x0, x1], xs, y)
findContiguos (_, [x0], (x1:xs), y)  = findContiguos (x0+x1, [x0, x1], xs, y)
findContiguos (s, cs, xs, y)
    | s == y = minimum cs + maximum cs
    | s <  y = findContiguos (s + head xs, cs ++ [head xs], (tail xs), y)
    | s >  y = findContiguos (s - head cs, tail cs, xs, y)
findContiguos _ = error "unknown error"

------ output ------------------

output = (++ "\n") . show

------ main -------------------

main = interact $ output . (solve 25) . input