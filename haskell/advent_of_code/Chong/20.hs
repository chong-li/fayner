import Data.Maybe

main = interact $ output . solve . input

----- helper functions -----

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

splitOnDelim :: Eq a => a -> [a] -> [[a]]
splitOnDelim x ys = 
  case splitOnFst x ys of
    (f, [])    -> [f]
    ([], s:ss) -> splitOnDelim x ss
    (f, s:ss)  -> f : splitOnDelim x ss

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

----- input -----------

input = map toRawTile . splitOnDelim "" . lines

toRawTile :: [String] -> (Int,[[Bool]])
toRawTile (idStr:gridStr) = (idInt,gridBool)
  where
      [_,n] = words $ init idStr
      idInt = read n
      gridBool = map (map (== '#')) gridStr


----- solve -----------

solve = multiplyIds . getCorners . process . map toTile

data Tile a = Tile {tile    :: Int
                   ,state   :: Int
                   ,edges   :: [[a]]
                   ,matches :: [[(Int,Int)]] -- by edge: (tile, state)
                   } deriving (Show)

toTile :: (Int,[[a]]) -> Tile a
toTile (t,bss) = Tile t 0 (getEdges bss) $ replicate 4 []
  where
      top = head bss
      right = map last bss
      bottom = last bss
      left =  map head bss
      getEdges bss = [top,right,bottom,left]

--- Talvez tenha que rotacionar os matches também
nextState :: Tile a -> Tile a
nextState (Tile t s es ms)
    | s `mod` 4 == 3 = Tile t ((s+1) `mod` 8) (flipEdges es) ms
    | otherwise      = Tile t ((s+1) `mod` 8) (rotateEdges es) ms

allStates :: Tile a -> [Tile a]
allStates x = take 8 $ x : allStates (nextState x)

-- Tá cagado aqui!!!
rotateEdges :: [[a]] -> [[a]]
rotateEdges xs = [l,u,r,d]
    where
        [d,r,u,l] = flipEdges xs

flipEdges :: [[a]] -> [[a]]
flipEdges [u,r,d,l] = [d, reverse r, u, reverse l]

match :: Eq a => Tile a -> Tile a -> Maybe (Tile a, Tile a)
match x y = case dropWhile isNothing $ map (matchAsIs x) $ allStates y of
    [] -> Nothing
    ms -> head ms

matchAsIs :: Eq a => Tile a -> Tile a -> Maybe (Tile a, Tile a)
matchAsIs x@(Tile tx sx esx msx) y@(Tile ty sy esy msy)
    | upx == doy = Just (Tile tx sx esx [(ty,sy):mux,mrx,mdx,mlx]
                        ,Tile ty sy esy [muy,mry,(tx,sx):mdy,mly])
    | upy == dox = Just (Tile tx sx esx [mux,mrx,(ty,sy):mdx,mlx]
                        ,Tile ty sy esy [(tx,sx):muy,mry,mdy,mly])
    | rix == ley = Just (Tile tx sx esx [mux,(ty,sy):mrx,mdx,mlx]
                        ,Tile ty sy esy [muy,mry,mdy,(tx,sx):mly])
    | riy == lex = Just (Tile tx sx esx [mux,mrx,mdx,(ty,sy):mlx]
                        ,Tile ty sy esy [muy,(tx,sx):mry,mdy,mly])
    | otherwise  = Nothing
  where
      [upx,rix,dox,lex] = esx
      [mux,mrx,mdx,mlx] = msx
      [upy,riy,doy,ley] = esy
      [muy,mry,mdy,mly] = msy

matchHeadAsIs :: Eq a => [Tile a] -> [Tile a]
matchHeadAsIs []     = []
matchHeadAsIs (t:ts) = aux t [] ts
  where
      aux x xs [] = x:xs
      aux x xs (y:ys) = let (x',y') = fromMaybe (x,y) $ matchAsIs x y in
          aux x' (y':xs) ys
          
process' :: Eq a => [Tile a] -> [Tile a] -> [Tile a] -> [Tile a] -> [Tile a]
process' ps [] [] []         = ps
process' [] [] [] (u:us)     = process' [] [u] [] us
process' ps (l:ls) fs []     = process' (l:ps) (matchHeadAsIs ls) [] fs
process' ps (l:ls) fs (u:us) = case match l u of
    Nothing       -> process' ps (l:ls) (u:fs) us
    Just (l', u') -> process' ps (l':u':ls) fs us

process = process' [] [] []

countNeighbors :: Tile a -> Int
countNeighbors t = length $ filter (/=[]) $ matches t

getCorners :: [Tile a] -> [Tile a]
getCorners = filter (\x -> countNeighbors x == 2)

multiplyIds :: [Tile a] -> Int
multiplyIds = product . map tile

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "Tile 2311:\n..##.#..#.\n##..#.....\n#...##..#.\n####.#...#\n##.##.###.\n##...#.###\n.#.#.#..##\n..#....#..\n###...#.#.\n..###..###\n\nTile 1951:\n#.##...##.\n#.####...#\n.....#..##\n#...######\n.##.#....#\n.###.#####\n###.##.##.\n.###....#.\n..#.#..#.#\n#...##.#..\n\nTile 1171:\n####...##.\n#..##.#..#\n##.#..#.#.\n.###.####.\n..###.####\n.##....##.\n.#...####.\n#.##.####.\n####..#...\n.....##...\n\nTile 1427:\n###.##.#..\n.#..#.##..\n.#.##.#..#\n#.#.#.##.#\n....#...##\n...##..##.\n...#.#####\n.#.####.#.\n..#..###.#\n..##.#..#.\n\nTile 1489:\n##.#.#....\n..##...#..\n.##..##...\n..#...#...\n#####...#.\n#..#.#.#.#\n...#.#.#..\n##.#...##.\n..##.##.##\n###.##.#..\n\nTile 2473:\n#....####.\n#..#.##...\n#.##..#...\n######.#.#\n.#...#.#.#\n.#########\n.###.#..#.\n########.#\n##...##.#.\n..###.#.#.\n\nTile 2971:\n..#.#....#\n#...###...\n#.#.###...\n##.##..#..\n.#####..##\n.#..####.#\n#..#.#..#.\n..####.###\n..#.#.###.\n...#.#.#.#\n\nTile 2729:\n...#.#.#.#\n####.#....\n..#.#.....\n....#..#.#\n.##..##.#.\n.#.####...\n####.#.#..\n##.####...\n##..#.##..\n#.##...##.\n\nTile 3079:\n#.#.#####.\n.#..######\n..#.......\n######....\n####.#..#.\n.#...#.##.\n#.#####.##\n..#.###...\n..#.......\n..#.###..."