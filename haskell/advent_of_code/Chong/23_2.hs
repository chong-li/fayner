import Data.IntMap.Strict (insert, (!))
import qualified Data.IntMap.Strict as M

import Data.Char (digitToInt)
-- usar do Data.Char a func digitToInt

main = interact $ output . solve . input

type Map = M.IntMap Int
data Cups = Cups {curr :: Int
                 ,dest :: Int
                 ,trip :: Int
                 ,size :: Int
                 ,iMap :: Map
                 } deriving Show

--- helper funcs ------

detach3From :: Int -> Map -> Map
detach3From k m = insert k3 k1 (insert k k4 m)
  where
    k1 = m ! k
    k3 = (m !) $ m ! k1
    k4 = m ! k3

getDetached :: Int -> Map -> [Int]
getDetached k m = getDetached' [k] (m ! k) m
  where
    getDetached' ds k' m
      | k' `elem` ds = ds
      | otherwise    = getDetached' (k':ds) (m ! k') m

reJoinCycles :: Int -> Int -> Map -> Map
reJoinCycles d p m = insert d p (insert x y m)
  where
    x = (m !) $ m ! p -- next of next of p
    y = m ! d

----- input -----------

input = map digitToInt

----- solve -----------

globPar = 10^6

solve = prod . reaply (10*globPar) play . toCups globPar . toTup . engorge globPar

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

engorge :: Int -> [Int] -> [Int]
engorge k xs = xs ++ [10..k]

toTup :: [Int] -> [(Int, Int)]
toTup l@(x:xs) = zip l (xs ++ [x])

toCups :: Int -> [(Int, Int)] -> Cups
toCups s l@((c,_):xs) = Cups {curr = c
                             ,dest = 0
                             ,trip = 0
                             ,size = s
                             ,iMap = M.fromList l
                             }

play = newCup . reatach . destination . pickup

pickup :: Cups -> Cups
pickup cup = cup {trip = m ! c, iMap = detach3From c m}
  where
    c = curr cup
    m = iMap cup

destination :: Cups -> Cups
destination cup = cup {dest = destination' $ procPrev $ curr cup}
  where
    p = trip cup
    m = iMap cup
    detached = getDetached p m
    procPrev 1 = size cup
    procPrev i = i-1
    destination' i
      | i `notElem` detached = i
      | otherwise = destination' (procPrev i)

reatach :: Cups -> Cups
reatach cup = cup {dest = 0, trip = 0, iMap = reJoinCycles d p m}
  where
    d = dest cup
    p = trip cup
    m = iMap cup

newCup :: Cups -> Cups
newCup cup = cup {curr = iMap cup ! curr cup}

prod :: Cups -> Int
prod cup = x * y
  where
    m = iMap cup
    x = m ! 1
    y = m ! x

----- output ----------

output = (++ "\n") . show

----- test ------------

solveTest = prod . reaply 100 play . toCups 9 . toTup
test = (solveTest . input) inputTest

inputTest = "974618352"
outputSeq = "75893264"

-- OutputSeq é a saída pra parte 1. O resultado final tem que ser 7*5 == 35