import os

class Rule:
    """Container for both rules and their minimum length"""
    def __init__(self, parsed_rule):
        self.rule = parsed_rule
        self.max_len = 1 if parsed_rule in [[['a']], [['b']]] else None
        self.min_len = min([len(l) for l in parsed_rule])

        assert self.min_len >= 1

    def __repr__(self):
        return f'({self.min_len} - {self.max_len}): {self.rule}'

    def __len__(self):
        return self.min_len

    def update_len(self, rules_dict):
        """Updates min_len and max_len attributes from one step down rule tree"""
        max_flag = all([
           all([rules_dict[sub_rule].max_len for sub_rule in and_rule])
           for and_rule in self.rule
        ])
        if max_flag:
            self.max_len = max([
               sum([rules_dict[sub_rule].max_len for sub_rule in and_rule])
               for and_rule in self.rule
        ])
        self.min_len = min([
           sum([rules_dict[sub_rule].min_len for sub_rule in and_rule])
           for and_rule in self.rule
        ])


def is_valid_and_rule(rules_dict, and_rule, message):
    """Checks validity of message against general OR rule"""

    if len(and_rule) == 1:
        if and_rule[0].isnumeric():
            next_rule = rules_dict[and_rule[0]]
            return is_valid_rule(rules_dict, next_rule.rule, message)

        return message == and_rule[0]

    fst_ref, *tail_rule = and_rule
    fst_rule = rules_dict[fst_ref]

    assert tail_rule != []

    max_len = min(len(message), 1+fst_rule.max_len) if fst_rule.max_len else len(message)
    min_len = fst_rule.min_len
    if min_len >= max_len:
        return False

    for i in range(min_len, max_len):
        head_message, tail_message = message[:i], message[i:]

        assert head_message != ''
        assert tail_message != ''

        if (    is_valid_rule(rules_dict, fst_rule.rule, head_message)
            and is_valid_and_rule(rules_dict, tail_rule, tail_message)
            ):
            return True

    return False

def is_valid_rule(rules_dict, or_rule, message):
    """True if message conform to the or_rule"""

    for and_rule in or_rule:
        if is_valid_and_rule(rules_dict, and_rule, message):
            return True

    return False

def warm_up_sizes(rules_dict, times):
    """run loads of updates on len for rules"""
    for _ in range(times):
        for rle in rules_dict.values():
            rle.update_len(rules_dict)

    return rules_dict

path = os.path.join(os.getcwd(), 'inputs', 'input19')

with open(path) as f:
    rules_str, messages = f.read().split("\n\n")

rules = {}

for line in rules_str.split('\n'):
    k, v = line.split(': ')
    rule = Rule([and_rule.replace('"','').split() for and_rule in v.split(' | ')])
    rules[k] = rule

rules['a'] = Rule([['a']])
rules['b'] = Rule([['b']])

rules['8'] = Rule([['42'],['42', '8']])
rules['11'] = Rule([['42', '31'],['42', '11', '31']])

rules = warm_up_sizes(rules, 7)
rule_0 = rules['0'].rule

valids = {}
for message in messages.split():
    valids[message] = is_valid_rule(rules, rule_0, message)

print(len([v for v in valids.values() if v]))
