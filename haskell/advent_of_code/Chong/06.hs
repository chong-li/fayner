main = interact $ output . solve . input

-- input -------------------------------------
input = joinGroup . lines

--lines :: String -> [String]

joinGroup :: [String] -> [String]
joinGroup [x0] = [x0]
joinGroup (x0:"":xs) = x0 : joinGroup xs
joinGroup (x0:x1:xs) = joinGroup $ (x0 ++ " " ++ x1) : xs

-- solve -------------------------------------

solve = solve2

solve1 = sum . map (length . getGroupUnion . words)        -- 6939
solve2 = sum . map (length . getGroupIntersection . words) -- 3585

-- map words :: [String] -> [[String]]

intersect :: Eq a => [a] -> [a] -> [a] -- a == Char, [a] == String
intersect xs = filter (`elem` xs)

getGroupIntersection :: [String] -> String
getGroupIntersection = foldr1 intersect

unite :: Eq a => [a] -> [a] -> [a]
unite = foldr addNew
    where addNew :: Eq a => a -> [a] -> [a]
          addNew x ys
            | x `elem` ys = ys
            | otherwise   = x:ys

getGroupUnion :: [String] -> String
getGroupUnion = foldr unite ""

-- output ------------------------------------

output = (++ "\n") . show