main = interact $ output . solve . input

----- definitions --------------
data Op = Nop | Acc | Jmp | End 
data Status = Succ | Loop | Run deriving Show
-- Instruction operation value index
data Inst = Inst Op Int Int
-- Tape lefts (head:rights)
data Tape = Tape [Inst] [Inst]
-- Program Status accumulator Tape executionStackIndices
data Prog = Prog Status Int Tape [Int]

instance Show Prog where
    show (Prog s i _ _) = show s ++ " " ++ show i 

----- input ---------------------
input :: String -> [Inst]
input = getInsts . (map words) . lines . (++ "\nend +0")

getOp :: String -> Op
getOp "nop" = Nop
getOp "acc" = Acc
getOp "jmp" = Jmp
getOp "end" = End

readInt x = (read x :: Int)

getVal :: String -> Int
getVal ('-':xs) = negate (readInt xs)
getVal ('+':xs) = readInt xs

getOpVal :: Int -> [String] -> Inst
getOpVal i (op:val:[]) = Inst (getOp op) (getVal val) i

getInsts :: [[String]] -> [Inst]
getInsts xs = foldr f [] (zip [0..] xs)
    where f (i, x) ins = (getOpVal i x):ins

----- solve ---------------------
solve :: [Inst] -> Prog
solve = solve2

solve1 = execProg . makeProg                        -- Loop 1941
solve2 instrs = fixProg (solve1 instrs, emptyProg)  -- Succ 2096

------ Tape funcs --------------
makeTape :: [Inst] -> Tape
makeTape xs = Tape [] xs

getHead :: Tape -> Inst
getHead (Tape _ []) = error "no more tape: getHead"
getHead (Tape _ (r:rs)) = r

next :: Tape -> Tape
next (Tape _ []) = error "no more tape: next"
next (Tape ls (r:rs)) = Tape (r:ls) rs

prev :: Tape -> Tape
prev (Tape [] _) = error "no more tape: prev"
prev (Tape (l:ls) rs) = Tape ls (l:rs)

jmp :: Int -> Tape -> Tape
jmp i t
    | i > 0  = jmp (i-1) (next t)
    | i < 0  = jmp (i+1) (prev t)
    | i == 0 = t

rw :: Tape -> Tape
rw (Tape [] rs)     = (Tape [] rs)
rw (Tape (l:ls) rs) = rw (Tape ls (l:rs))

moveToIndex :: Int -> Tape -> Tape
moveToIndex k (Tape ls ((Inst op v i):rs))
    | k == i = Tape ls ((Inst op v i):rs)
    | k >  i = Tape ((Inst op v i):ls) rs
    | k <  i = Tape (tail ls) ((head ls):(Inst op v i):rs)

-------- execution funcs -------

makeProg :: [Inst] -> Prog
makeProg xs = (Prog Run 0 (makeTape xs) [])

emptyProg = Prog Loop 0 (Tape [] []) []

execInst :: Prog -> Prog
execInst (Prog Run a t es) = case getHead t of
    Inst End _ _ -> Prog Succ a t es
    Inst Nop v i -> if i `elem` es
                    then Prog Loop a t es
                    else Prog Run a (next t) (i:es)
    Inst Acc v i -> if i `elem` es
                    then Prog Loop a t es
                    else Prog Run (a+v) (next t) (i:es)
    Inst Jmp v i -> if i `elem` es
                    then Prog Loop a t es
                    else Prog Run a (jmp v t) (i:es)

execProg :: Prog -> Prog
execProg (Prog Run a t es) = (execProg . execInst) (Prog Run a t es)
execProg p = p

resetProg :: Prog -> Prog
resetProg (Prog _ _ t _) = Prog Run 0 (rw t) []

flipOneInst :: Inst -> Inst
flipOneInst (Inst Nop v i) = (Inst Jmp v i)
flipOneInst (Inst Jmp v i) = (Inst Nop v i)
flipOneInst inst = inst

flipHead :: Prog -> Prog
flipHead (Prog s a (Tape ls (r:rs)) es) = Prog s a (Tape ls ((flipOneInst r):rs)) es
flipHead _                              = error "can't flip empty list"

unstack :: Prog -> Prog
unstack (Prog s a t [])     = Prog s a t []
unstack (Prog s a t (e:es)) = case moveToIndex e t of
    Tape ls ((Inst Acc v k):rs) -> unstack (Prog s (a-v) (Tape ls ((Inst Acc v k):rs)) es)
    Tape ls rs                  -> Prog s a (Tape ls rs) es

fixProg :: (Prog, Prog) -> Prog
fixProg (p, Prog Succ a t es) = Prog Succ a t es
fixProg (p, _) = fixProg (p2, (execProg . resetProg . flipHead) p2)
    where p2 = unstack p

-------- output ---------------
output x = (show x) ++ "\n"