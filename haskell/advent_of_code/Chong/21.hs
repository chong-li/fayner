import Data.Map (Map, findWithDefault, elems)
import qualified Data.Map as M

import Data.Set (Set, intersection, union, unions, (\\))
import qualified Data.Set as S

main = interact $ output . solve . input

----- helper functions ---

tupApply :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
tupApply f g (x, y) = (f x, g y)

fstApply :: (a -> b) -> (a, c) -> (b, c)
fstApply f = tupApply f id

sndApply :: (c -> d) -> (a, c) -> (a, d)
sndApply = tupApply id

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

----- input -----------

input = map (tupApply words tidyAllergens . splitOnFst '(') . lines

tidyAllergens :: String -> [String]
tidyAllergens = map init . words . drop 10

----- solve -----------

solve = length . getSafes. fstApply (unions . elems) . getAllerMap

type Allergen   = String
type Ingredient = String
type IngSet    = Set Ingredient
type AllerMap    = Map Allergen IngSet

addToAllerMap :: ([Ingredient], [Allergen]) -> AllerMap -> AllerMap
addToAllerMap(_ , [])      m = m
addToAllerMap(is, al:als)  m = addToAllerMap (is, als) (addToMap m al is)

-- findWithDefault :: Ord k => a -> k -> Map k a -> a
-- M.insert :: Ord k => k -> a -> Map k a -> Map k a
addToMap :: AllerMap -> Allergen -> [Ingredient] -> AllerMap
addToMap m al is = M.insert al (alSet `intersection` iSet) m
  where
      iSet = S.fromList is
      alSet = findWithDefault iSet al m

-- addToIngSet :: ([Ingredient], [a]) -> IngSet -> IngSet
-- addToIngSet (is,_) iSet = iSet `union` S.fromList is

addToIngList :: ([Ingredient], [a]) -> [Ingredient] -> [Ingredient]
addToIngList (is,_) is' = is ++ is'

getAllerMap :: [([Ingredient], [Allergen])] -> (AllerMap,[Ingredient])
getAllerMap = foldr aggrFunc (M.empty,[])
  where
      aggrFunc t (m,is) = (addToAllerMap t m, addToIngList t is)

getSafes :: (IngSet, [Ingredient]) -> [Ingredient]
getSafes (als, is) = filter (`S.notMember` als) is

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)"

-- readable input

-- mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
-- trh fvjkl sbzzf mxmxvkd (contains dairy)
-- sqjhc fvjkl (contains soy)
-- sqjhc mxmxvkd sbzzf (contains fish)