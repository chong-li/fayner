{-# LANGUAGE FlexibleInstances #-}

main = interact $ output . solve . input

----- data declarations --------


data Complex a = a :+ a deriving Show

instance Num a => Num (Complex a) where
    (x :+ y) + (x' :+ y') = (x+x') :+ (y+y')
    (x :+ y) - (x' :+ y') = (x-x') :+ (y-y')
    (x :+ y) * (x' :+ y') = (x*x'-y*y') :+ (x*y'+y*x')
    negate (x :+ y)       = (negate x) :+ (negate y)
    abs    (x :+ y)       = (abs x + abs y) :+ 0
    signum (x :+ y)       = (signum x) :+ (signum y)
    fromInteger n         = (fromInteger n) :+ 0

data Compass = E | S | W | N deriving (Show, Eq)

-- Boat wayPoint point
data Boat = Boat (Complex Int) (Complex Int) deriving Show

data Instruction = Move Compass Int | Rotate Int | Forward Int deriving Show

----- input --------------------

input = toInstructions . words

readInt x = (read x :: Int)

getRot :: Int -> Int
getRot x = x `div` 90 `mod` 4

toInstructions :: [String] -> [Instruction] 
toInstructions = map getInst

getInst :: String -> Instruction
getInst ('F':int) = Forward (readInt int)
getInst ('E':int) = Move E  (readInt int)
getInst ('S':int) = Move S  (readInt int)
getInst ('W':int) = Move W  (readInt int)
getInst ('N':int) = Move N  (readInt int)
getInst ('R':int) = Rotate  ((getRot . readInt) int)
getInst ('L':int) = Rotate  ((negate . getRot . readInt) int)
getInst unk       = error $ "unknown instruction: " ++ unk


----- solve --------------------
solve = abs . getPoint . (`route` (Boat (10 :+ 1) (0 :+ 0)))

getPoint :: Boat -> (Complex Int)
getPoint (Boat _ p) = p

fromCompass :: Compass -> (Complex Int)
fromCompass E = 1    :+ 0
fromCompass S = 0    :+ (-1)
fromCompass W = (-1) :+ 0
fromCompass N = 0    :+ 1

forward :: Int -> Boat -> Boat
forward n (Boat w p) = Boat w ((n :+ 0)* w + p)

move :: Compass -> Int -> Boat -> Boat
move c n (Boat w p) = Boat w' p
  where
    w' = (fromCompass c) * (n :+ 0) + w

rotate :: Int -> Boat -> Boat
rotate 0 b = b
rotate n (Boat w p)
    | n > 0 = rotate (n-1) (Boat ((0 :+ (-1)) * w) p)
    | n < 0 = rotate (n+1) (Boat ((0 :+ 1) * w) p)

goOneInst :: Instruction -> Boat -> Boat
goOneInst (Forward n) b = forward n b
goOneInst (Move c  n) b = move c n b
goOneInst (Rotate  n) b = rotate n b

route :: [Instruction] -> Boat -> Boat
route []     b = b
route (x:xs) b = route xs (goOneInst x b)

testRoute :: [Instruction] -> [Boat] -> [Boat]
testRoute [] bs = bs
testRoute (x:xs) bs = testRoute xs (bs ++ [goOneInst x $ last bs])

----- output -------------------

output = (++ "\n\n") . show

----- test ---------------------

testOut = (solve . input) inputTest

instrs = input inputTest
--t = zip (testRoute instrs [Boat (WayP 10 1) 0 0]) (instrs ++ [Rotate 0])


inputTest = "F10\nN3\nF7\nR90\nF11\n"