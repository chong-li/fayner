main = interact $ output . solve . input

----- input -----------

input = lines

----- solve -----------

solve = id

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = ""