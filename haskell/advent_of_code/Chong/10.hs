------ test ---------
inputTest = "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3"

inputSmall = "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4"

out = (output . solve . input) inputTest
------ input --------
input :: String -> [Integer]
input = map readInteger . words
    
readInteger i = read i :: Integer


------ solve --------
solve = solve2

solve1 = multiplyTuple . countDiffs . sort  -- 1625
solve2 = snd . head . procCounts . includePair 0 1 . includeCounts . sort

sort :: Ord a => [a] -> [a]
sort []     = []
sort (x:xs) = sort [y | y <- xs, y <= x] ++ [x] ++ sort [y | y <- xs, y > x]

countDiffs :: [Integer] -> (Int, Int)
countDiffs []     = (0, 1)
countDiffs xs = foldr f (0, 1) (zip (0:xs) xs)
    where f :: (Integer, Integer) -> (Int, Int) -> (Int, Int)
          f (low, hig) (c0, c3) = case hig - low of
              1 -> (1 + c0, c3)
              3 -> (c0, 1 + c3)
              _ -> error "invalid input"

multiplyTuple :: Num a => (a, a) -> a
multiplyTuple (x, y) = x * y

includePair :: a -> b -> [(a, b)] -> [(a, b)]
includePair i c xs = (i, c):xs

includeCounts :: (Num a, Num b) => [a] -> [(a, b)]
includeCounts = foldr (`includePair` 0) []

addCounts :: (Num a, Ord a, Num b) => (a, b) -> (a, b) -> (a, b)
addCounts (a0, b0) (a1, b1)
    | a1 - a0 <= 3 = (a1, b1 + b0)
    | otherwise    = (a1, b1)

procCounts :: (Num a, Ord a, Num b) => [(a, b)] -> [(a, b)]
procCounts []     = error "nothing to count"
procCounts [x]    = [x]
procCounts (x:xs) = procCounts $ [addCounts x y | y <- take 3 xs] ++ drop 3 xs

------ output -------

output = (++ "\n") . show

------ main ---------

main = interact $ output . solve . input