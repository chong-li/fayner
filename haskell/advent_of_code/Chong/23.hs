main = interact $ output . solve . input

data Cups = Cups Int Int [Int] [Int]

instance Show Cups where
    show (Cups cup dest cups picks) =
        concat [ "(", show cup, ") . ", show dest, " . ",show cups, " . ", show picks]

----- input -----------

input = splitInts . stringify

stringify :: String -> [String]
stringify cs = [[c] | c <- cs]

splitInts :: [String] -> [Int]
splitInts = map read

----- solve -----------

solve = init . cycleTo 1 . getCups . reaply 100 play . toCups

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

toCups :: [Int] -> Cups
toCups xs = Cups (head xs) 0 xs []

getCups :: Cups -> [Int]
getCups (Cups _ _ xs _) = xs

cycleTo :: Int -> [Int] -> [Int]
cycleTo k xs = take (length xs) $ tail $ dropWhile (/=k) $ cycle xs

pickUp :: Cups -> Cups
pickUp (Cups cup dest (x:xs) _) = Cups cup dest (x:rest) pick
  where
      rest = drop 3 xs
      pick = take 3 xs

destination :: Cups -> Cups
destination (Cups cup _ xs pick) = Cups cup (dest (cup-1)) xs pick
  where
      dest i
        | i `elem` xs = i
        | otherwise = dest $ (i-1) `mod` 10

reinsert :: Cups -> Cups
reinsert (Cups cup dest xs pick) = Cups cup 0 (pick ++ cycleTo dest xs) []

newCup :: Cups -> Cups
newCup (Cups cup 0 xs []) = Cups (head xs') 0 xs' []
  where
      xs' = cycleTo cup xs

play :: Cups -> Cups
play = newCup . reinsert . destination . pickUp


----- output ----------

-- output = (++ "\n") . joinInts
output = joinInts

joinInts = concatMap show 
----- test ------------

test = (solve . input) inputTest

inputTest = "974618352"