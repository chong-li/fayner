import Data.Set (empty, union, (\\), intersection, size)
import qualified Data.Set as S

import Data.List (sort)

main = interact $ output . solve . input

----- helper funcs ----

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

----- input -----------

input = map splitInstr . lines

splitInstr :: String -> [String]
splitInstr ('e':xs)     = "e" : splitInstr xs
splitInstr ('w':xs)     = "w" : splitInstr xs
splitInstr ('s':'e':xs) = "se" : splitInstr xs
splitInstr ('s':'w':xs) = "sw" : splitInstr xs
splitInstr ('n':'e':xs) = "ne" : splitInstr xs
splitInstr ('n':'w':xs) = "nw" : splitInstr xs
splitInstr []           = []
splitInstr _            = error "input out of spec"

----- solve -----------

-- after removePairs we have a list of black tiles
solve = size . reaply 100 nextDay. day0

newtype Hex = Hex (Integer, Integer, Integer) deriving (Show, Eq, Ord)

instance Num Hex where
    Hex (x,y,z) + Hex (x',y',z') = Hex (x+x',y+y',z+z')
    Hex (x,y,z) - Hex (x',y',z') = Hex (x-x',y-y',z-z')
    Hex (x,y,z) * Hex (x',y',z') = Hex (x*x',y*y',z*z')
    abs (Hex (x,y,z))            = Hex (abs x, abs y, abs z)
    signum (Hex (x,y,z))         = Hex (signum x, signum y, signum z)
    fromInteger x                = Hex (x,x,x)

toHexDir :: String -> Hex
toHexDir "e"  = Hex ( 1, 1, 0)
toHexDir "w"  = Hex (-1,-1, 0)
toHexDir "se" = Hex ( 1, 0, 1)
toHexDir "sw" = Hex ( 0,-1, 1)
toHexDir "ne" = Hex ( 0, 1,-1)
toHexDir "nw" = Hex (-1, 0,-1)
toHexDir _    = error "Invalid direction"

hexDirs = [Hex (1,1,0),Hex (-1,-1,0),Hex (1,0,1),Hex (0,-1,1),Hex (0,1,-1),Hex (-1,0,-1)]

getTile :: [String] -> Hex
getTile = sum . map toHexDir

removePairs :: Eq a => [a] -> [a]
removePairs []  = []
removePairs [x] = [x]
removePairs (x:x':xs)
    | x == x'   = removePairs xs
    | otherwise = x : removePairs (x':xs)

day0 :: [[String]] -> S.Set Hex
day0 = S.fromList . removePairs . sort . map getTile

neighbors :: Hex -> S.Set Hex
neighbors h = S.map (+h) $ S.fromList hexDirs

blackNeighbors :: Hex -> S.Set Hex -> Int
blackNeighbors h sh = size $ neighbors h `intersection` sh

allNeighbors :: S.Set Hex -> S.Set Hex
allNeighbors = foldr addNeighbors empty
  where
      addNeighbors :: Hex -> S.Set Hex -> S.Set Hex
      addNeighbors h sh = sh `union` neighbors h

keepBlacks :: S.Set Hex -> S.Set Hex
keepBlacks bs = S.filter (blackToBlack bs) bs

blackToBlack :: S.Set Hex -> Hex -> Bool
blackToBlack bs h = n == 1 || n == 2
  where
      n = blackNeighbors h bs

turnBlacks :: S.Set Hex -> S.Set Hex -> S.Set Hex
turnBlacks bs = S.filter (whiteToBlack bs)

whiteToBlack :: S.Set Hex -> Hex -> Bool
whiteToBlack bs h = blackNeighbors h bs == 2

nextDay :: S.Set Hex -> S.Set Hex
nextDay bs = keepBlacks bs `union` turnBlacks bs ws
  where
      ws = allNeighbors bs \\ bs

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "sesenwnenenewseeswwswswwnenewsewsw\nneeenesenwnwwswnenewnwwsewnenwseswesw\nseswneswswsenwwnwse\nnwnwneseeswswnenewneswwnewseswneseene\nswweswneswnenwsewnwneneseenw\neesenwseswswnenwswnwnwsewwnwsene\nsewnenenenesenwsewnenwwwse\nwenwwweseeeweswwwnwwe\nwsweesenenewnwwnwsenewsenwwsesesenwne\nneeswseenwwswnwswswnw\nnenwswwsewswnenenewsenwsenwnesesenew\nenewnwewneswsewnwswenweswnenwsenwsw\nsweneswneswneneenwnewenewwneswswnese\nswwesenesewenwneswnwwneseswwne\nenesenwswwswneneswsenwnewswseenwsese\nwnwnesenesenenwwnenwsewesewsesesew\nnenewswnwewswnenesenwnesewesw\neneswnwswnwsenenwnwnwwseeswneewsenese\nneswnwewnwnwseenwseesewsenwsweewe\nwseweeenwnesenwwwswnew"

-- readable input

-- sesenwnenenewseeswwswswwnenewsewsw
-- neeenesenwnwwswnenewnwwsewnenwseswesw
-- seswneswswsenwwnwse
-- nwnwneseeswswnenewneswwnewseswneseene
-- swweswneswnenwsewnwneneseenw
-- eesenwseswswnenwswnwnwsewwnwsene
-- sewnenenenesenwsewnenwwwse
-- wenwwweseeeweswwwnwwe
-- wsweesenenewnwwnwsenewsenwwsesesenwne
-- neeswseenwwswnwswswnw
-- nenwswwsewswnenenewsenwsenwnesesenew
-- enewnwewneswsewnwswenweswnenwsenwsw
-- sweneswneswneneenwnewenewwneswswnese
-- swwesenesewenwneswnwwneseswwne
-- enesenwswwswneneswsenwnewswseenwsese
-- wnwnesenesenenwwnenwsewesewsesesew
-- nenewswnwewswnenesenwnesewesw
-- eneswnwswnwsenenwnwnwwseeswneewsenese
-- neswnwewnwnwseenwseesewsenwsweewe
-- wseweeenwnesenwwwswnew