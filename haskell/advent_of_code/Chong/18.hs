main = interact $ output . solve . input

----- types -----------

data Op   = S | P deriving Show
data Brac = OB | CB deriving Show
data Calc = Val Int | Op Op | Brac Brac deriving Show

----- input -----------

input = (map parseInput) . lines

parseInput :: String -> [Char]
parseInput = filter (/=' ')

----- solve -----------

solve = sum . (map (getVal . calc . toCalcList))

toCalc :: Char -> Calc
toCalc x
    | x == '('  = Brac OB
    | x == ')'  = Brac CB
    | x == '+'  = Op S
    | x == '*'  = Op P
    | otherwise = Val (read [x])

toCalcList :: String -> [Calc]
toCalcList = map toCalc

perfOper :: Op -> Int -> Int -> Int
perfOper S a b = a + b
perfOper P a b = a * b

consume :: [Calc] -> Calc -> [Calc]
consume ((Op op):(Val b):calcs) (Val a) = consume calcs (Val (perfOper op a b))
consume ((Val a):(Brac OB):calcs) (Brac CB) = consume calcs (Val a)
consume cs c                                = c:cs

calc :: [Calc] -> Calc
calc = head . (foldl consume [])

[] [2, +, (, 1, *, 1, )]
[2] [ +, (, 1, *, 1, )]
[+, 2] [(, 1, *, 1, )]
[(, +, 2] [ 1, *, 1, )]
[1, (, +, 2] [ *, 1, )]
[*, 1, (, +, 2] [ 1, )]
(consume [(, +, 2] 1 ) [ )]
[1, (, +, 2] [ )]
consume [+, 2] 1
consume [] 3
[3]

getVal :: Calc -> Int
getVal (Val x) = x

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "1 + (2 * 3) + (4 * (5 + 6))\n((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"
ouputTest = [51, 13632]