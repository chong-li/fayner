main = interact $ output . solve . input

----- helper functions ---

tupApply :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
tupApply f g (x, y) = (f x, g y)

fstApply :: (a -> b) -> (a, c) -> (b, c)
fstApply f = tupApply f id

sndApply :: (c -> d) -> (a, c) -> (a, d)
sndApply g = tupApply id g

tupAnd :: (Bool, Bool) -> Bool
tupAnd (True, True) = True
tupAnd _            = False

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

splitOnDelim :: Eq a => a -> [a] -> [[a]]
splitOnDelim x ys = 
  case splitOnFst x ys of
    (f, [])    -> [f]
    ([], s:ss) -> splitOnDelim x ss
    (f, s:ss)  -> f : (splitOnDelim x ss)

----- input -----------

input = (map (readIntList . tail)) . (splitOnDelim "") . lines

readInt a = (read a :: Int)
readIntList = map readInt

----- solve -----------

solve = score . play . toTup

toTup :: [[a]] -> ([a], [a])
toTup [x,y] = (x,y)
toTup []    = error "can't tuple single element list"
toTup _     = error "can't tuple more then two elements"

play :: Ord a => ([a], [a]) -> ([a], [a])
play (x:xs, y:ys) =
    case compare x y of
        GT -> play (xs++[x,y], ys)
        LT -> play (xs, ys++[y,x])
        EQ -> error "draw!"
play t           = t

score' :: [Int] -> Int
score' xs = foldr (+) 0 (zipWith (*) ws xs)
  where
      l = length xs
      ws = enumFromThenTo l (l-1) 1

score :: ([Int], [Int]) -> Int
score ([], ys) = score' ys
score (xs, []) = score' xs

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10"

-- readable input
-- Player 1:
-- 9
-- 2
-- 6
-- 3
-- 1

-- Player 2:
-- 5
-- 8
-- 4
-- 7
-- 10

-- end state:
-- Player 2 wins (obviously)
-- 3, 2, 10, 6, 8, 5, 9, 4, 7, 1