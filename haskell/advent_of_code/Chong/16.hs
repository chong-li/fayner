import Data.Maybe

main = interact $ output . solve . input

----- types -----------
type Rule   = (String, [Range]) -- This probably has to change
type Ticket = [Int]
type Range  = (Int, Int)

----- input -----------

--- ideia de como fazer essa ingestão:
---
---  * fazer 3 funções de ingestão separadas, 1 pra cada parte
---  * fazer uma função que separa as 3 partes usando o mesmo truque das linhas
---         vazias

input :: String -> ([Rule], Ticket, [Ticket])
input = applyParsers . splitParts . lines

readInt a = (read a :: Int)

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn el = reverse . (splitOn' el [[]])

splitOn' :: Eq a => a -> [[a]] -> [a] -> [[a]]
splitOn' el xs     []     = xs
splitOn' el (x:xs) (y:ys) =
  case el == y of
    True  -> splitOn' el ([]:x:xs) ys
    False -> splitOn' el ((x ++ [y]):xs) ys

splitParts :: [String] -> [[String]]
splitParts = splitOn ""

applyParsers :: [[String]] -> ([Rule], Ticket, [Ticket])
applyParsers (a:b:c:[]) = (parseRules a, (head . parseTickets) b, parseTickets c)

parseTickets :: [String] -> [Ticket]
parseTickets = (map parseTicket) . tail

parseTicket :: String -> Ticket
parseTicket xs = map readInt $ splitOn ',' xs

parseRules :: [String] -> [Rule]
parseRules = map parseRule

parseRule :: String -> Rule
parseRule xs = (name, parseRanges ranges)
  where
    (name:ranges:[]) = splitOn ':' xs

parseRanges :: String -> [Range]
parseRanges (' ':xs) = parseRanges xs
parseRanges xs       = map parseRange $ filter (/="or") $ words xs

parseRange :: String -> Range
parseRange xs = pairs $ map readInt $ splitOn '-' xs
  where
    pairs (a:b:[]) = (a, b)

----- solve -----------

solve :: ([Rule], Ticket, [Ticket]) -> Int
solve (rs, _, ts) = sum $ catMaybes $ map (`maybeScanError` rs) (concat ts)

inRange :: Int -> Range -> Ordering
inRange x (a, b)
    | a <= x && x <= b = EQ
    | x < a            = LT
    | x > b            = GT
    | otherwise        = error "lower range bound greater then upper range bound"

satisfiesRule :: Int -> Rule -> Bool
satisfiesRule _ (_, [])     = False
satisfiesRule v (_, (range:ranges)) =
  case v `inRange` range of
    LT -> False
    EQ -> True
    GT -> satisfiesRule v ("", ranges)

satisfiesSomeRule :: Int -> [Rule] -> Bool
satisfiesSomeRule v []           = False
satisfiesSomeRule v (r:rs) = v `satisfiesRule` r || v `satisfiesSomeRule` rs

maybeScanError :: Int -> [Rule] -> Maybe Int
maybeScanError v rs =
  case v `satisfiesSomeRule` rs of
    True  -> Nothing
    False -> Just v


----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest
raw = input inputTest

inputTest = "class: 1-3 or 5-7\nrow number: 6-11 or 33-44\nseat: 13-40 or 45-50\n\nyour ticket:\n7,1,14\n\nnearby tickets:\n7,3,47\n40,4,50\n55,2,20\n38,6,12"

-- readable inputTest:
-- class: 1-3 or 5-7
-- row number: 6-11 or 33-44
-- seat: 13-40 or 45-50

-- your ticket:
-- 7,1,14

-- nearby tickets:
-- 7,3,47
-- 40,4,50
-- 55,2,20
-- 38,6,12