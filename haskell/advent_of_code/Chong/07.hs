main = interact $ output . solve . input

---- input -------------------

input = (map $ getMatrioska . words) . lines

data Bag = Bag String String deriving (Show, Eq)
data Matrioska = Matrioska Bag [(Int, Bag)] deriving Show

readInt i = read i :: Int

matrProc :: [(Int, Bag)] -> [String] -> [(Int, Bag)]
matrProc _ ("no":_) = []
matrProc ibs (num:adj:c:noun:[]) = (((readInt num),(Bag adj c)):ibs)
matrProc ibs (num:adj:c:noun:xs) = matrProc (((readInt num),(Bag adj c)):ibs) xs

getMatrioska :: [String] -> Matrioska
getMatrioska (adj:c:"bags":"contain":xs) = Matrioska (Bag adj c) (matrProc [] xs)
getMatrioska xs = Matrioska (Bag "bad bag" (concat xs)) []


---- solve --------------------

solve = solve2

solve1 = (\x->x-1) .length . shinyBags -- 226

shinyBags :: [Matrioska] -> [Bag]
shinyBags ms = fixedPoint (innerBags ms) [Bag "shiny" "gold"]

fixedPoint :: Eq a => (a -> a) -> a -> a
fixedPoint f inp
    | out == inp = out
    | otherwise  = fixedPoint f out
    where out = f inp

innerBags :: [Matrioska] -> [Bag] -> [Bag]
innerBags ms bs = foldr f bs (newBags ms bs)
    where f :: Bag -> [Bag] -> [Bag]
          f b bs = if b `elem` bs
                   then bs
                   else b:bs

newBags :: [Matrioska] -> [Bag] -> [Bag]
newBags ms bs = [getMatrioskaBag m | b <- bs, m <- ms, b `elemInnerBags` m]
            
getMatrioskaBag :: Matrioska -> Bag
getMatrioskaBag (Matrioska b _) = b

elemInnerBags :: Bag -> Matrioska -> Bool
elemInnerBags b (Matrioska _ []) = False
elemInnerBags b (Matrioska m ((_,ib):ibs)) = b == ib || b `elemInnerBags` (Matrioska m ibs)


solve2 ms = addBags (getMatrGivenBag (Bag "shiny" "gold") ms) ms -- 9569

addBags :: Matrioska -> [Matrioska] -> Int
addBags (Matrioska _ []) _ = 0
addBags (Matrioska bag ibs) ms = foldr (f ms) 0 ibs
    where f :: [Matrioska] -> (Int, Bag) -> Int -> Int
          f ms (i, b) acc = acc + i + i * (addBags (getMatrGivenBag b ms) ms)

getMatrGivenBag :: Bag -> [Matrioska] -> Matrioska
getMatrGivenBag b (m:ms) = if b == getMatrioskaBag m
                           then m
                           else getMatrGivenBag b ms

---- output -------------------

output = (++ "\n") . show