main = interact $ output . solve . input

----- definitions --------------
data Op = Nop | Acc | Jmp | End 
data Status = Succ | Loop | Run deriving Show
data Inst = Inst Op Int Int
data Tape = Tape [Inst] [Inst]
data Prog = Prog Status Int Tape [Int]

instance Show Prog where
    show (Prog s i _ _) = show s ++ " " ++ show i 

----- input ---------------------
input :: String -> [Inst]
input = getInsts . (map words) . lines . (++ "\nend +0")

getOp :: String -> Op
getOp "nop" = Nop
getOp "acc" = Acc
getOp "jmp" = Jmp
getOp "end" = End

readInt x = (read x :: Int)

getVal :: String -> Int
getVal ('-':xs) = negate (readInt xs)
getVal ('+':xs) = readInt xs

getOpVal :: Int -> [String] -> Inst
getOpVal i (op:val:[]) = Inst (getOp op) (getVal val) i

getInsts :: [[String]] -> [Inst]
getInsts xs = foldr f [] (zip [0..] xs)
    where f (i, x) ins = (getOpVal i x):ins

----- solve ---------------------
solve :: [Inst] -> Prog
solve = solve2

solve1 = execProg . makeProg        -- Loop 1941
solve2 = (fixProg (-1)) . makeProg  -- Succ 2096

------ Tape funcs --------------
makeTape :: [Inst] -> Tape
makeTape xs = Tape [] xs

getHead :: Tape -> Inst
getHead (Tape _ []) = error "no more tape: getHead"
getHead (Tape _ (r:rs)) = r

next :: Tape -> Tape
next (Tape _ []) = error "no more tape: next"
next (Tape ls (r:rs)) = Tape (r:ls) rs

prev :: Tape -> Tape
prev (Tape [] _) = error "no more tape: prev"
prev (Tape (l:ls) rs) = Tape ls (l:rs)

jmp :: Int -> Tape -> Tape
jmp i t
    | i > 0  = jmp (i-1) (next t)
    | i < 0  = jmp (i+1) (prev t)
    | i == 0 = t

rw :: Tape -> Tape
rw (Tape [] rs)     = (Tape [] rs)
rw (Tape (l:ls) rs) = rw (Tape ls (l:rs))

-------- execution funcs -------

makeProg :: [Inst] -> Prog
makeProg xs = (Prog Run 0 (makeTape xs) [])

execInst :: Prog -> Prog
execInst (Prog Run a t es) = case getHead t of
    Inst End _ _ -> Prog Succ a t es
    Inst Nop v i -> if i `elem` es
                    then Prog Loop a t es
                    else Prog Run a (next t) (i:es)
    Inst Acc v i -> if i `elem` es
                    then Prog Loop a t es
                    else Prog Run (a+v) (next t) (i:es)
    Inst Jmp v i -> if i `elem` es
                    then Prog Loop a t es
                    else Prog Run a (jmp v t) (i:es)


execProg :: Prog -> Prog
execProg (Prog Run a t es) = (execProg . execInst) (Prog Run a t es)
execProg p = p

flipOneInst :: Inst -> Inst
flipOneInst (Inst Nop v i) = (Inst Jmp v i)
flipOneInst (Inst Jmp v i) = (Inst Nop v i)
flipOneInst inst = inst

flipInst :: [Int] -> Inst -> Inst
flipInst ks (Inst op v i) = if i `elem` ks
                            then flipOneInst (Inst op v i)
                            else (Inst op v i)

mFlip :: [Int] -> Tape -> Tape
mFlip ks (Tape [] rs) = Tape [] (map (flipInst ks) rs)
mFlip ks t            = mFlip ks (rw t)

fixProg :: Int -> Prog -> Prog
fixProg i p = case execProg p of
    Prog Loop a t es -> fixProg (i+1) (Prog Run 0 (mFlip [i, i+1] t) [])
    Prog Succ a t es -> Prog Succ a t es

-------- output ---------------
output x = (show x) ++ "\n"