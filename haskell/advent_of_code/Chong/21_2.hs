import Data.Map (Map, findWithDefault, elems)
import qualified Data.Map as M

import Data.Set (Set, intersection, union, unions, (\\), size)
import qualified Data.Set as S

main = interact $ output . solve . input

----- helper functions ---

tupApply :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
tupApply f g (x, y) = (f x, g y)

fstApply :: (a -> b) -> (a, c) -> (b, c)
fstApply f = tupApply f id

sndApply :: (c -> d) -> (a, c) -> (a, d)
sndApply = tupApply id

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

----- input -----------

input = map (tupApply words tidyAllergens . splitOnFst '(') . lines

tidyAllergens :: String -> [String]
tidyAllergens = map init . words . drop 10

----- solve -----------

solve = converge . getAllerMap'

type Allergen   = String
type Ingredient = String
type IngSet     = Set Ingredient
type AllerMap   = Map Allergen IngSet

addToAllerMap :: ([Ingredient], [Allergen]) -> AllerMap -> AllerMap
addToAllerMap(_ , [])      m = m
addToAllerMap(is, al:als)  m = addToAllerMap (is, als) (addToMap m al is)

-- findWithDefault :: Ord k => a -> k -> Map k a -> a
-- M.insert :: Ord k => k -> a -> Map k a -> Map k a
addToMap :: AllerMap -> Allergen -> [Ingredient] -> AllerMap
addToMap m al is = M.insert al (alSet `intersection` iSet) m
  where
      iSet = S.fromList is
      alSet = findWithDefault iSet al m

getAllerMap :: [([Ingredient], [Allergen])] -> AllerMap
getAllerMap = foldr addToAllerMap M.empty

getAllerMap' :: [([Ingredient], [Allergen])] -> (AllerMap, AllerMap)
getAllerMap' t = (M.empty, foldr addToAllerMap M.empty t)

decided :: IngSet -> Bool
decided iSet = size iSet == 1

getDecided :: (AllerMap, AllerMap) -> (AllerMap, AllerMap)
getDecided t@(dec, undec) = fstApply (dec `M.union`) $ M.partition decided undec

update :: (AllerMap, AllerMap) -> (AllerMap, AllerMap)
update (dec, undec) = (dec, M.map (\\ decSet) undec)
  where
    decSet = unions $ elems dec

converge :: (AllerMap, AllerMap) -> [Ingredient]
converge t@(dec, undec) =
  if M.null undec
  then concatMap S.toAscList $ elems dec
  else converge $ update $ getDecided t

----- output ----------

output = (++ "\n") . concatWith ','

concatWith :: a -> [[a]] -> [a]
concatWith _   [] = []
concatWith _   [x] = x
concatWith sep (x:xs) = x ++ [sep] ++ concatWith sep xs



----- test ------------

test = (solve . input) inputTest

inputTest = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)"

-- readable input

-- mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
-- trh fvjkl sbzzf mxmxvkd (contains dairy)
-- sqjhc fvjkl (contains soy)
-- sqjhc mxmxvkd sbzzf (contains fish)