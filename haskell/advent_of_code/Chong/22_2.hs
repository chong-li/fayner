import Data.Maybe

main = interact $ output . solve . input

type Game = [([Int],[Int])]
data Winner = You | Crab deriving Show

----- helper functions ---

tupApply :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
tupApply f g (x, y) = (f x, g y)

fstApply :: (a -> b) -> (a, c) -> (b, c)
fstApply f = tupApply f id

sndApply :: (c -> d) -> (a, c) -> (a, d)
sndApply = tupApply id

tupAnd :: (Bool, Bool) -> Bool
tupAnd (True, True) = True
tupAnd _            = False

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

splitOnDelim :: Eq a => a -> [a] -> [[a]]
splitOnDelim x ys = 
  case splitOnFst x ys of
    (f, [])    -> [f]
    ([], s:ss) -> splitOnDelim x ss
    (f, s:ss)  -> f : splitOnDelim x ss

----- input -----------

input = map (readIntList . tail) . splitOnDelim "" . lines

readInt a = read a :: Int
readIntList = map readInt

----- solve -----------

solve = score . play . toList . toTup

toTup :: [[a]] -> ([a], [a])
toTup [x,y] = (x,y)
toTup []    = error "can't tuple single element list"
toTup _     = error "can't tuple more then two elements"

toList :: ([a],[a]) -> [([a],[a])]
toList x = [x]

winningHand :: Game -> Maybe ([Int], Winner)
winningHand (([],ys):gs) = Just (ys, Crab)
winningHand ((xs,[]):gs) = Just (xs, You)
winningHand ((xs,ys):gs) = if (xs,ys) `elem` gs then Just (xs, You) else Nothing

play :: Game -> ([Int], Winner)
play gs = if isNothing wh then play (playOne gs) else fromMaybe undefined wh
  where
    wh = winningHand gs

playOne :: Game -> Game
playOne g@((x:xs,y:ys):gs)
  | (length xs >= x) && (length ys >= y) =
    case play [(take x xs,take y ys)] of
      (_, You)  -> (xs ++ [x,y], ys) : g
      (_, Crab) -> (xs, ys ++ [y,x]) : g
  | otherwise = if x > y
                then (xs ++ [x,y], ys) : g
                else (xs, ys ++ [y,x]) : g

score' :: [Int] -> Int
score' xs = sum (zipWith (*) ws xs)
  where
      l = length xs
      ws = enumFromThenTo l (l-1) 1

score :: ([Int], Winner) -> Int
score (xs,_) = score' xs

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

loopInput = "Player 1:\n43\n19\n\nPlayer 2:\n2\n29\n14"
l = input loopInput
lg = toList $ toTup l
playSix = foldr (.) id $ replicate 6 playOne
-- readable loop input
-- Player 1:
-- 43
-- 19

-- Player 2:
-- 2
-- 29
-- 14


inputTest = "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10"
tupTest = input inputTest
gameTest = toList $ toTup tupTest
-- readable input
-- Player 1:
-- 9
-- 2
-- 6
-- 3
-- 1

-- Player 2:
-- 5
-- 8
-- 4
-- 7
-- 10

-- end state:
-- Player 2 wins (obviously)
-- 3, 2, 10, 6, 8, 5, 9, 4, 7, 1