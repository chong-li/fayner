import qualified Data.IntMap.Strict as M
import Data.Maybe

main = interact $ output . solve . input

----- types --------------

type AtomRule = Either OrRule String
data AndRule = And Int (Maybe Int) [Int] deriving Show
type OrRule = [AndRule]
type RuleMap = M.IntMap AtomRule

----- helper functions ---

tupApply :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
tupApply f g (x, y) = (f x, g y)

fstApply :: (a -> b) -> (a, c) -> (b, c)
fstApply f = tupApply f id

sndApply :: (c -> d) -> (a, c) -> (a, d)
sndApply = tupApply id

tupAnd :: (Bool, Bool) -> Bool
tupAnd (True, True) = True
tupAnd _            = False

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

splitOnDelim :: Eq a => a -> [a] -> [[a]]
splitOnDelim x ys = 
  case splitOnFst x ys of
    (f, [])    -> [f]
    ([], s:ss) -> splitOnDelim x ss
    (f, s:ss)  -> f : splitOnDelim x ss

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

----- min-max updating funcs ---------

indAnd :: AndRule -> [Int]
indAnd (And _ _ is) = is

minAnd :: AndRule -> Int
minAnd (And mn _ _) = mn

maxAnd :: AndRule -> Maybe Int
maxAnd (And _ mx _) = mx

minOr :: OrRule -> Int
minOr = minimum . map minAnd

maxOr :: OrRule -> Maybe Int
maxOr xs = foldr1 monMax (map maxAnd xs)
  where
    monMax mx my = do
      x <- mx
      y <- my
      return $ max x y

minAtom :: AtomRule -> Int
minAtom (Right _) = 1
minAtom (Left xs) = minOr xs

maxAtom :: AtomRule -> Maybe Int
maxAtom (Right _) = Just 1
maxAtom (Left xs) = maxOr xs

stepAnd :: RuleMap -> [Int] -> AndRule
stepAnd rm is = And mn mx is
  where
    mn = sum $ map (minAtom . (rm M.!)) is
    mx = foldr (monAdd . maxAtom . (rm M.!)) (Just 0) is
      where
        monAdd monX monY = do
          x <- monX
          y <- monY
          return $ x+y

stepOr :: RuleMap -> OrRule -> OrRule
stepOr rm = map (stepAnd rm . indAnd)

updateOn :: Int -> RuleMap -> RuleMap
updateOn i rm =
  case rm M.! i of
    Right _ -> rm
    Left xs -> M.insert i (Left $ stepOr rm xs) rm

----- input -----------

input = tupApply (map (fixRule . splitRule)) id . rulesAndMessages . lines

rulesAndMessages :: [String] -> ([String], [String])
rulesAndMessages = sndApply tail . splitOnFst ""

cleanRule :: String -> String
cleanRule = filter (/='\"') . tail . tail

-- assumes it is a rule
splitRule :: String -> (Int, String)
splitRule = tupApply read cleanRule . splitOnFst ':'

fixRule :: (Int, String) -> (Int, String)
fixRule (8, _) = (8, "42 | 42 8")
fixRule (11,_) = (11, "42 31 | 42 11 31")
fixRule x      = x

----- solve -----------

solve = length . checkRules . fstApply (reaply 10 update . M.fromList . mapAtoms)

mapAtoms :: [(Int, String)] -> [(Int, AtomRule)]
mapAtoms = map (sndApply toAtom)

readInt a = read a :: Int

toAtom :: String -> AtomRule
toAtom xs
  | xs `elem` ["a","b"] = Right xs
  | otherwise           = Left (toAndRule <$> splitOnDelim '|' xs)

toAndRule :: String -> AndRule
toAndRule xs = And (length ars) Nothing ars
  where
    ars = map readInt $ words xs

update :: RuleMap -> RuleMap
update rm = foldr updateOn rm (M.keys rm)

isValidAtom :: RuleMap -> AtomRule -> String -> Bool
isValidAtom _ (Right r) cs = r == cs
isValidAtom rm (Left r) cs = isValidOr rm r cs

isValidOr :: RuleMap -> OrRule -> String -> Bool
isValidOr _  []     _  = False
isValidOr rm (r:rs) cs = isValidAnd rm (indAnd r) cs || isValidOr rm rs cs

isValidAnd :: RuleMap -> [Int] -> String -> Bool
isValidAnd _  []     [] = True
isValidAnd _  []     _  = False
isValidAnd _  _      [] = False
isValidAnd rm [i]    cs = isValid rm i cs
isValidAnd rm (i:is) cs = or [validateSplit j| j<-hRange, lcs-j+1 `elem` tRange]
  where
    (And hmn hmx _) = stepAnd rm [i]
    (And tmn tmx _) = stepAnd rm is
    lcs = length cs - 1
    hRange = enumFromTo hmn $ fromMaybe lcs hmx
    tRange = enumFromTo tmn $ fromMaybe lcs tmx
    validateSplit j = isValid rm i (take j cs) && isValidAnd rm is (drop j cs)

isValid :: RuleMap -> Int -> String -> Bool
isValid rm i = isValidAtom rm (rm M.! i)

checkRules :: (RuleMap, [String]) -> [String]
checkRules (rm, css) = filter (isValid rm 0) css

----- output ----------

output = (++ "\n") . show

----- test ------------

testOG = (solve . input) inputTestOG

inputTestOG = "0: 4 1 5\n1: 2 3 | 3 2\n2: 4 4 | 5 5\n3: 4 5 | 5 4\n4: \"a\"\n5: \"b\"\n\nababbb\nbababa\nabbbab\naaabbb\naaaabbb\nab\nba\naa\nbb"

test = (solve . input) inputTest

inputTest = "42: 9 14 | 10 1\n9: 14 27 | 1 26\n10: 23 14 | 28 1\n1: \"a\"\n11: 42 31\n5: 1 14 | 15 1\n19: 14 1 | 14 14\n12: 24 14 | 19 1\n16: 15 1 | 14 14\n31: 14 17 | 1 13\n6: 14 14 | 1 14\n2: 1 24 | 14 4\n0: 8 11\n13: 14 3 | 1 12\n15: 1 | 14\n17: 14 2 | 1 7\n23: 25 1 | 22 14\n28: 16 1\n4: 1 1\n20: 14 14 | 1 15\n3: 5 14 | 16 1\n27: 1 6 | 14 18\n14: \"b\"\n21: 14 1 | 1 14\n25: 1 1 | 1 14\n22: 14 14\n8: 42\n26: 14 22 | 1 20\n18: 15 15\n7: 14 5 | 1 21\n24: 14 1\n\nabbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa\nbbabbbbaabaabba\nbabbbbaabbbbbabbbbbbaabaaabaaa\naaabbbbbbaaaabaababaabababbabaaabbababababaaa\nbbbbbbbaaaabbbbaaabbabaaa\nbbbababbbbaaaaaaaabbababaaababaabab\nababaaaaaabaaab\nababaaaaabbbaba\nbaabbaaaabbaaaababbaababb\nabbbbabbbbaaaababbbbbbaaaababb\naaaaabbaabaaaaababaa\naaaabbaaaabbaaa\naaaabbaabbaaaaaaabbbabbbaaabbaabaaa\nbabaaabbbaaabaababbaabababaaab\naabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"

-- readable input

-- 42: 9 14 | 10 1
-- 9: 14 27 | 1 26
-- 10: 23 14 | 28 1
-- 1: "a"
-- 11: 42 31
-- 5: 1 14 | 15 1
-- 19: 14 1 | 14 14
-- 12: 24 14 | 19 1
-- 16: 15 1 | 14 14
-- 31: 14 17 | 1 13
-- 6: 14 14 | 1 14
-- 2: 1 24 | 14 4
-- 0: 8 11
-- 13: 14 3 | 1 12
-- 15: 1 | 14
-- 17: 14 2 | 1 7
-- 23: 25 1 | 22 14
-- 28: 16 1
-- 4: 1 1
-- 20: 14 14 | 1 15
-- 3: 5 14 | 16 1
-- 27: 1 6 | 14 18
-- 14: "b"
-- 21: 14 1 | 1 14
-- 25: 1 1 | 1 14
-- 22: 14 14
-- 8: 42
-- 26: 14 22 | 1 20
-- 18: 15 15
-- 7: 14 5 | 1 21
-- 24: 14 1

-- abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
-- bbabbbbaabaabba
-- babbbbaabbbbbabbbbbbaabaaabaaa
-- aaabbbbbbaaaabaababaabababbabaaabbababababaaa
-- bbbbbbbaaaabbbbaaabbabaaa
-- bbbababbbbaaaaaaaabbababaaababaabab
-- ababaaaaaabaaab
-- ababaaaaabbbaba
-- baabbaaaabbaaaababbaababb
-- abbbbabbbbaaaababbbbbbaaaababb
-- aaaaabbaabaaaaababaa
-- aaaabbaaaabbaaa
-- aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
-- babaaabbbaaabaababbaabababaaab
-- aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba