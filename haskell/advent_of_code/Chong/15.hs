import qualified Data.IntMap.Strict as M

main = interact $ output . solve . input

----- types ----------

type Map  = M.IntMap Int
type Play = (Int, Int)
type Game = (Play, Map)

----- input -----------

input a = (read a :: Int)

startingSeq = [17,1,3,16,19,0] :: [Int]

startingSeqTest0 = [0,3,6] :: [Int]
startingSeqTest1 = [3,2,1] :: [Int]
startingSeqTest2 = [3,1,2] :: [Int]

----- solve -----------

solve = solve' startingSeq

solve' :: [Int] -> Int -> Int
solve' xs target = play target $ startGame xs

rightEnum :: [a] -> [(a, Int)]
rightEnum xs = zip xs [2..]

-- M.singleton :: Key -> a -> IntMap a
-- M.insert    :: Key -> a -> IntMap a -> IntMap a
stepGame :: Game -> Play -> Game
stepGame ((num, count), m) p =
  case M.null m of
    True  -> (p, M.singleton num count)
    False -> (p, M.insert num count m)

startGame :: [Int] -> Game
startGame (x:xs) = foldl stepGame ((x, 1), M.empty) $ rightEnum xs

play :: Int -> Game -> Int
play target ((num, count), m)
    | count == target = num
    | otherwise       = play target $ stepGame ((num, count), m) nextPlay
  where
    nextPlay = playOne ((num, count), m)

-- M.lookup :: Key -> IntMap a -> Maybe a
playOne :: Game -> Play
playOne ((num, count), m) =
  case M.lookup num m of
    Nothing -> (0, count + 1)
    Just x  -> (count - x, count + 1)


----- output ----------

output = (++ "\n") . show


----- test ------------

test0 = solve' startingSeqTest0 (input inputTest)
test1 = solve' startingSeqTest1 (input inputTest)
test2 = solve' startingSeqTest2 (input inputTest)

tests = [test0 == 436, test1 == 438, test2 == 1836]

inputTest = "2020"