import qualified Data.IntMap.Strict as M

main = interact $ output . solve . input

----- types --------------

-- ConcatRules or PipeRules or Right String or Left Int
data RuleExp = And | Or | Exp [String] | Ind Int deriving Show

----- helper functions ---

notEmpty cs = cs /= ""
notColon c  = c /= ':'
notSpace c  = c /= ' '
notQuot  c  = c /= '\"'
notPipe  c  = c /= '|'

tupApply :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
tupApply f g (x, y) = (f x, g y)

fstApply :: (a -> b) -> (a, c) -> (b, c)
fstApply f = tupApply f id

sndApply :: (c -> d) -> (a, c) -> (a, d)
sndApply g = tupApply id g

----- input -----------

input = (tupApply (map splitRule) id) . rulesAndMessages . lines

rulesAndMessages :: [String] -> ([String], [String])
rulesAndMessages xs = (takeWhile notEmpty xs, tail $ dropWhile notEmpty xs)


getRuleNumber :: String -> Int
getRuleNumber = read . (takeWhile notColon)

getRuleString :: String -> String
getRuleString = (filter notQuot) . tail . (dropWhile notSpace) . (dropWhile notColon)

-- assumes it is a rule
splitRule :: String -> (Int, String)
splitRule cs = (getRuleNumber cs, getRuleString cs)


----- solve -----------

solve = length . matchMessages . (fstApply solveRule0)

solveRule0 = getExp . (buildRule 0) . M.fromList. mapRules

toRuleExp :: String -> [RuleExp]
toRuleExp cs
    | '|' `elem` cs = (toRuleExp (init $ left '|' cs)) ++ [Or] ++ (toRuleExp (tail $ right '|' cs))
    | ' ' `elem` cs = (toRuleExp (left ' ' cs)) ++ [And] ++ (toRuleExp (right ' ' cs))
    | all (`elem` ['0'..'9']) cs = [Ind $ read cs]
    | otherwise = [Exp [cs]]
    where
        left  c = takeWhile (/=c)
        right c = tail . (dropWhile (/=c))

mapRules = map (sndApply toRuleExp)

getRule :: Int -> M.IntMap [RuleExp] -> [RuleExp]
getRule = M.findWithDefault ([Exp [""]])

buildRule :: Int -> M.IntMap [RuleExp] -> [RuleExp]
buildRule i m = buildRule' (getRule i m)
  where
    buildRule' :: [RuleExp] -> [RuleExp]
    buildRule' [Exp s] = [Exp s]
    buildRule' [Ind i'] = buildRule i' m
    buildRule' ((Ind i'):res) = buildRule' $ (buildRule' [Ind i']) ++ res
    buildRule' ((Exp ss):And:(Exp ss'):res) = buildRule' $ (Exp cs):res
      where
          cs = [s ++ s' | s <-ss, s' <-ss']
    buildRule' (exp:And:(Ind i'):res) = buildRule' $ (exp:And:iExp) ++ res
      where
          iExp = buildRule' [Ind i']
    buildRule' [(Exp ss),Or,(Exp ss')] = [Exp (ss ++ ss')]
    buildRule' (exp:Or:res) = buildRule' $ [exp,Or] ++ (buildRule' res)

getExp :: [RuleExp] -> [String]
getExp [Exp x] = x
getExp _       = ["failed Exp value"]

matchMessages :: ([String], [String]) -> [String]
matchMessages (f, s) = filter (`elem` f) s

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "0: 4 1 5\n1: 2 3 | 3 2\n2: 4 4 | 5 5\n3: 4 5 | 5 4\n4: \"a\"\n5: \"b\"\n\nababbb\nbababa\nabbbab\naaabbb\naaaabbb"

-- readable input

-- 0: 4 1 5
-- 1: 2 3 | 3 2
-- 2: 4 4 | 5 5
-- 3: 4 5 | 5 4
-- 4: "a"
-- 5: "b"

-- ababbb
-- bababa
-- abbbab
-- aaabbb
-- aaaabbb