main = interact $ output . solve . input

----- input -----------

input = detachHead . (map readInt) . (splitWhen isDigit)

readInt a = (read a :: Int)

isDigit :: Char -> Bool
isDigit = (`elem` ['0'..'9'])

prependNewListOrToHead :: (a -> Bool) -> a -> [[a]] -> [[a]]
prependNewListOrToHead f x ([]:ys) =
    case f x of
        False -> []  : ys
        True  -> [x] : ys
prependNewListOrToHead f x (y:ys) =
    case f x of
        False -> [] : y  : ys
        True  -> (x : y) : ys

splitWhen :: (a -> Bool) -> [a] -> [[a]]
splitWhen f = foldr (prependNewListOrToHead f) [[]]

detachHead :: [a] -> (a, [a])
detachHead (x:xs) = (x, xs)
detachHead [] = error "can't detach inexistent head"

----- solve -----------

solve = prodTup . minimumDiff . toIdDiffs

diffMinutes :: Integral a => a -> a -> a
diffMinutes fstTime busID = busID - (fstTime `mod` busID)

toIdDiffs :: Integral a => (a, [a]) -> [(a, a)] 
toIdDiffs (fstTime, ids) = foldr f [] ids
  where
      f i xs = (i, diffMinutes fstTime i) : xs

minDiff :: Integral a => (a, a) -> (a, a) -> (a, a)
minDiff x y =
    case compare (snd x) (snd y) of
        GT -> y
        _  -> x

minimumDiff :: Integral a =>  [(a, a)] -> (a, a)
minimumDiff = foldr1 minDiff

prodTup :: Num a => (a, a) -> a
prodTup (x, y) = x * y

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "939\n7,13,x,x,59,x,31,19"