----- imports ---------
import qualified Data.IntMap.Strict as M

main = interact $ output . solve . input

----- data ------------

data Trit = Zero | One | X deriving Show

type Mask     = [Trit]
type Key      = Int
type Value    = Int
type Memory   = (Key, Value)
type CodeLine = Either Mask Memory
type Prog     = [CodeLine]

type Ram = M.IntMap Int
type Comp = (Mask, Ram)

----- input -----------
input :: String -> Prog
input = (map readLine) . (map words) . lines

readInt x = (read x :: Int)

readTrit :: Char -> Trit
readTrit 'X' = X
readTrit '0' = Zero
readTrit '1' = One

readMask :: String -> Mask
readMask = map readTrit

readAdress :: String -> Int
readAdress = readInt . (drop 4) . init

readMemory :: String -> String -> Memory
readMemory adress val = (readAdress adress, readInt val)

readLine :: [String] -> CodeLine
readLine ("mask":"=":mask:[]) = Left  (readMask mask)
readLine (adress:"=":val:[])  = Right (readMemory adress val)
readLine xs = error $ "unexpected input: " ++ concat xs

----- solve -----------

solve = accumulate . (compute newComp)

newComp :: Comp
newComp = ([], M.empty)

tritVal :: Trit -> Int
tritVal One  = 1
tritVal Zero = 0
tritVal X    = error "X is not convertible"

detTritFromVal :: Int -> Int -> Trit
detTritFromVal v exp =
    case (v `div` (2^exp)) `mod` 2 of
        0 -> Zero
        1 -> One

keyToMask :: Key -> Mask
keyToMask k = map (detTritFromVal k) [35,34..0]

maskToKey :: Mask -> Key
maskToKey = foldl (\x y -> 2*x + tritVal y) 0

maskGate :: (Trit, Trit) -> Trit
maskGate (Zero, y) = y
maskGate (x,    _) = x

applyMask :: Mask -> Key -> Mask
applyMask m k = map maskGate $ zip m $ keyToMask k

colapseX :: Mask -> [Mask]
colapseX [X]    = [[Zero],[One]]
colapseX [m]    = [[m]]
colapseX (X:ms) = colapseX (Zero:ms) ++ colapseX (One:ms)
colapseX (m:ms) = [m:cX| cX <- colapseX ms]
colapseX _      = [] -- for safety

keyGen :: Key -> Mask -> [Key]
keyGen k m = map maskToKey $ colapseX $ applyMask m k

writeMany :: [Key] -> Value -> Ram -> Ram
writeMany []     _ ram = ram
writeMany (k:ks) v ram = writeMany ks v (M.insert k v ram)

computeLine :: Comp -> CodeLine -> Comp
computeLine (m, ram) (Left m')   = (m', ram)
computeLine (m, ram) (Right (k, v)) = (m, newRam)
  where
    newRam = writeMany (keyGen k m) v ram

compute :: Comp -> Prog -> Comp
compute c [] = c
compute c (l:ls) = compute (computeLine c l) ls

accumulate :: Comp -> Int
accumulate (_, ram) = foldr (+) 0 ram

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "mask = 000000000000000000000000000000X1001X\nmem[42] = 100\nmask = 00000000000000000000000000000000X0XX\nmem[26] = 1"