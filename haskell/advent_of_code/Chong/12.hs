main = interact $ output . solve . input

----- data declarations --------

data Compass = E | S | W | N deriving (Show, Eq)
-- Boat Compass xCoord yCoord
data Boat = Boat Compass Int Int deriving Show
data Instruction = Move Compass Int | Rotate Int | Forward Int deriving Show

----- input --------------------

input = toInstructions . words

readInt x = (read x :: Int)

getRot :: Int -> Int
getRot x = x `div` 90 `mod` 4

toInstructions :: [String] -> [Instruction] 
toInstructions = map getInst

getInst :: String -> Instruction
getInst ('F':int) = Forward (readInt int)
getInst ('E':int) = Move E  (readInt int)
getInst ('S':int) = Move S  (readInt int)
getInst ('W':int) = Move W  (readInt int)
getInst ('N':int) = Move N  (readInt int)
getInst ('R':int) = Rotate  ((getRot . readInt) int)
getInst ('L':int) = Rotate  ((negate . getRot . readInt) int)
getInst unk       = error $ "unknown instruction: " ++ unk


----- solve --------------------

solve = manhatan . (`route` (Boat E 0 0)) -- 439

move :: Compass -> Int -> Boat -> Boat
move E i (Boat c x y) = Boat c (x+i) y
move S i (Boat c x y) = Boat c x    (y-i)
move W i (Boat c x y) = Boat c (x-i) y
move N i (Boat c x y) = Boat c x    (y+i)

forward :: Int -> Boat -> Boat
forward i (Boat c x y) = move c i (Boat c x y)

newCompass :: Int -> Compass -> [Compass] -> Compass
newCompass i c = (!! i) . (dropWhile (/=c)) . cycle

rotate :: Int -> Boat -> Boat
rotate i (Boat c x y)
    | i < 0     = Boat (newCompass (-i) c [N, W, S, E]) x y
    | otherwise = Boat (newCompass i    c [E, S, W, N]) x y

goOneInst :: Instruction -> Boat -> Boat
goOneInst (Move c i)  = move c i
goOneInst (Forward i) = forward i
goOneInst (Rotate i)  = rotate i

route :: [Instruction] -> Boat -> Boat
route []     b = b
route (x:xs) b = route xs (goOneInst x b)

testRoute :: [Instruction] -> [Boat] -> [Boat]
testRoute [] bs = bs
testRoute (x:xs) bs = testRoute xs (bs ++ [goOneInst x $ last bs])

manhatan :: Boat -> Int
manhatan (Boat _ x y) = abs x + abs y

----- output -------------------

output = (++ "\n\n") . show

----- test ---------------------

testOut = (solve . input) inputTest


inputTest = "F10\nN3\nF7\nR90\nF11\n"