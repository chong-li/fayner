import Data.List

main = interact $ output . solve . input

----- input -----------

input = map splitInstr . lines

splitInstr :: String -> [String]
splitInstr ('e':xs)     = "e" : splitInstr xs
splitInstr ('w':xs)     = "w" : splitInstr xs
splitInstr ('s':'e':xs) = "se" : splitInstr xs
splitInstr ('s':'w':xs) = "sw" : splitInstr xs
splitInstr ('n':'e':xs) = "ne" : splitInstr xs
splitInstr ('n':'w':xs) = "nw" : splitInstr xs
splitInstr []           = []
splitInstr _            = error "input out of spec"

----- solve -----------

solve = length . removePairs . sort . map getTile

newtype Hex = Hex (Integer, Integer, Integer) deriving (Show, Eq, Ord)

instance Num Hex where
    Hex (x,y,z) + Hex (x',y',z') = Hex (x+x',y+y',z+z')
    Hex (x,y,z) - Hex (x',y',z') = Hex (x-x',y-y',z-z')
    Hex (x,y,z) * Hex (x',y',z') = Hex (x*x',y*y',z*z')
    abs (Hex (x,y,z))            = Hex (abs x, abs y, abs z)
    signum (Hex (x,y,z))         = Hex (signum x, signum y, signum z)
    fromInteger x                = Hex (x,0,0)

toHexDir :: String -> Hex
toHexDir "e"  = Hex ( 1, 1, 0)
toHexDir "w"  = Hex (-1,-1, 0)
toHexDir "se" = Hex ( 1, 0, 1)
toHexDir "sw" = Hex ( 0,-1, 1)
toHexDir "ne" = Hex ( 0, 1,-1)
toHexDir "nw" = Hex (-1, 0,-1)
toHexDir _    = error "Invalid direction"

getTile :: [String] -> Hex
getTile = sum . map toHexDir

removePairs :: Eq a => [a] -> [a]
removePairs []  = []
removePairs [x] = [x]
removePairs (x:x':xs)
    | x == x'   = removePairs xs
    | otherwise = x : removePairs (x':xs)

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "sesenwnenenewseeswwswswwnenewsewsw\nneeenesenwnwwswnenewnwwsewnenwseswesw\nseswneswswsenwwnwse\nnwnwneseeswswnenewneswwnewseswneseene\nswweswneswnenwsewnwneneseenw\neesenwseswswnenwswnwnwsewwnwsene\nsewnenenenesenwsewnenwwwse\nwenwwweseeeweswwwnwwe\nwsweesenenewnwwnwsenewsenwwsesesenwne\nneeswseenwwswnwswswnw\nnenwswwsewswnenenewsenwsenwnesesenew\nenewnwewneswsewnwswenweswnenwsenwsw\nsweneswneswneneenwnewenewwneswswnese\nswwesenesewenwneswnwwneseswwne\nenesenwswwswneneswsenwnewswseenwsese\nwnwnesenesenenwwnenwsewesewsesesew\nnenewswnwewswnenesenwnesewesw\neneswnwswnwsenenwnwnwwseeswneewsenese\nneswnwewnwnwseenwseesewsenwsweewe\nwseweeenwnesenwwwswnew"

-- readable input

-- sesenwnenenewseeswwswswwnenewsewsw
-- neeenesenwnwwswnenewnwwsewnenwseswesw
-- seswneswswsenwwnwse
-- nwnwneseeswswnenewneswwnewseswneseene
-- swweswneswnenwsewnwneneseenw
-- eesenwseswswnenwswnwnwsewwnwsene
-- sewnenenenesenwsewnenwwwse
-- wenwwweseeeweswwwnwwe
-- wsweesenenewnwwnwsenewsenwwsesesenwne
-- neeswseenwwswnwswswnw
-- nenwswwsewswnenenewsenwsenwnesesenew
-- enewnwewneswsewnwswenweswnenwsenwsw
-- sweneswneswneneenwnewenewwneswswnese
-- swwesenesewenwneswnwwneseswwne
-- enesenwswwswneneswsenwnewswseenwsese
-- wnwnesenesenenwwnenwsewesewsesesew
-- nenewswnwewswnenesenwnesewesw
-- eneswnwswnwsenenwnwnwwseeswneewsenese
-- neswnwewnwnwseenwseesewsenwsweewe
-- wseweeenwnesenwwwswnew