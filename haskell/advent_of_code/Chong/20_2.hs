import Data.Maybe
import Data.List (foldl')

main = interact $ output . solve . input

----- helper functions -----

splitOnFst :: Eq a => a -> [a] -> ([a], [a])
splitOnFst x ys = splitOnFst' x ([], ys)
  where
    splitOnFst' x (zs, []) = (zs, [])
    splitOnFst' x (zs, y:ys) =
      if x == y
      then (zs, y:ys)
      else splitOnFst' x (zs ++ [y], ys)

splitOnDelim :: Eq a => a -> [a] -> [[a]]
splitOnDelim x ys = 
  case splitOnFst x ys of
    (f, [])    -> [f]
    ([], s:ss) -> splitOnDelim x ss
    (f, s:ss)  -> f : splitOnDelim x ss

reaply :: Int -> (a -> a) -> a -> a
reaply k f = foldr (.) id (replicate k f)

----- input -----------

seaMonster = lines "                  # \n#    ##    ##    ###\n #  #  #  #  #  #"
monsterLen = 20 :: Int

input = map toRawTile . splitOnDelim "" . lines

toRawTile :: [String] -> (Int, [String])
toRawTile (idStr:gridStr) = (idInt,gridStr)
  where
      [_,n] = words $ init idStr
      idInt = read n


----- solve -----------

solve = notMonsters . toGridTile . concatMap concatRow . tileUp . process . map toTile

data Directions a = Dirs {up    :: [a]
                         ,right :: [a]
                         ,down  :: [a]
                         ,left  :: [a]
                         } deriving Show

data Tile a = Tile {tile    :: Int
                   ,state   :: Int
                   ,edges   :: Directions a
                   ,matches :: Directions Int
                   ,grid    :: [[a]]
                   } deriving (Show)

newtype Image = Im (Tile Char)

instance Show Image where
  show (Im t) = sh $ grid t
    where
      sh [] = "\n"
      sh (r:rs) = r ++ "\n" ++ sh rs

toTile :: (Int,[[a]]) -> Tile a
toTile (t,bss) = Tile t 0 (Dirs u r d l) (Dirs [] [] [] []) (removeEdges bss)
  where
      u = head bss
      r = map last bss
      d = last bss
      l =  map head bss
      removeEdges = map (tail . init) . tail . init

--- detecting matches ----------

nextState :: Tile a -> Tile a
nextState t
    | state t `mod` 4 == 3 = t {state = (state t + 1) `mod` 8
                               ,edges = flipEdges $ edges t
                               ,grid  = flipGrid $ grid t
                               }
    | otherwise = t {state = (state t + 1) `mod` 8
                    ,edges = rotateEdges $ edges t
                    ,grid  = rotateGrid $ grid t
                    }

allStates :: Tile a -> [Tile a]
allStates x = take 8 $ x : allStates (nextState x)

rotateEdges :: Directions a -> Directions a
rotateEdges xs = Dirs l u r d
    where
        Dirs d r u l = flipEdges xs

flipEdges :: Directions a -> Directions a
flipEdges (Dirs u r d l) = Dirs d (reverse r) u (reverse l)

transpose :: [[a]] -> [[a]]
transpose ([]:_) = []
transpose x = map head x : transpose (map tail x)

rotateGrid :: [[a]] -> [[a]]
rotateGrid = map reverse . transpose

flipGrid :: [[a]]->[[a]]
flipGrid = transpose . rotateGrid

match :: Eq a => Tile a -> Tile a -> Maybe (Tile a, Tile a)
match x y = case dropWhile isNothing $ map (matchAsIs x) $ allStates y of
    [] -> Nothing
    ms -> head ms

-- matchAsIs' :: Eq a => Tile a -> Tile a -> Maybe (Tile a, Tile a)
-- matchAsIs' x@(Tile tx sx esx msx gx) y@(Tile ty sy esy msy gy)
--     | upx == doy = Just (Tile tx sx esx [ty:mux, mrx, mdx, mlx] gx
--                         ,Tile ty sy esy [muy, mry, tx:mdy, mly] gy)
--     | upy == dox = Just (Tile tx sx esx [mux, mrx, ty:mdx, mlx] gx
--                         ,Tile ty sy esy [tx:muy, mry, mdy, mly] gy)
--     | rix == ley = Just (Tile tx sx esx [mux, ty:mrx, mdx, mlx] gx
--                         ,Tile ty sy esy [muy, mry, mdy, tx:mly] gy)
--     | riy == lex = Just (Tile tx sx esx [mux, mrx, mdx, ty:mlx] gx
--                         ,Tile ty sy esy [muy, tx:mry, mdy, mly] gy)
--     | otherwise  = Nothing
--   where
--       [upx,rix,dox,lex] = esx
--       [mux,mrx,mdx,mlx] = msx
--       [upy,riy,doy,ley] = esy
--       [muy,mry,mdy,mly] = msy

matchAsIs :: Eq a => Tile a -> Tile a -> Maybe (Tile a, Tile a)
matchAsIs x y
    | ux == dy = Just (x {matches = mx {up    = tile y : up   mx}}
                      ,y {matches = my {down  = tile x : down my}}
                      )
    | dx == uy = Just (x {matches = mx {down  = tile y : down mx}}
                      ,y {matches = my {up    = tile x : up   my}}
                      )
    | rx == ly = Just (x {matches = mx {right = tile y : right mx}}
                      ,y {matches = my {left  = tile x : left  my}}
                      )
    | lx == ry = Just (x {matches = mx {left  = tile y : left  mx}}
                      ,y {matches = my {right = tile x : right my}}
                      )
    | otherwise  = Nothing
  where
      Dirs ux rx dx lx = edges x
      Dirs uy ry dy ly = edges y
      mx = matches x
      my = matches y

matchHeadAsIs :: Eq a => [Tile a] -> [Tile a]
matchHeadAsIs []     = []
matchHeadAsIs (t:ts) = aux t [] ts
  where
      aux x xs [] = x:xs
      aux x xs (y:ys) = let (x',y') = fromMaybe (x,y) $ matchAsIs x y in
          aux x' (y':xs) ys

process' :: Eq a => [Tile a] -> [Tile a] -> [Tile a] -> [Tile a] -> [Tile a]
process' ps [] [] []         = ps
process' [] [] [] (u:us)     = process' [] [u] [] us
process' ps (l:ls) fs []     = process' (l:ps) (matchHeadAsIs ls) [] fs
process' ps (l:ls) fs (u:us) = case match l u of
    Nothing       -> process' ps (l:ls) (u:fs) us
    Just (l', u') -> process' ps (l':u':ls) fs us

process = process' [] [] []

---- building the image -------------

tileUp :: [Tile a] -> [[Tile a]]
tileUp ts = tileUp' [getLastRow ts]
  where
    tileUp' gr = case getPrevRow gr ts of
      []     -> gr
      newRow -> tileUp' (newRow : gr)

getLastRow :: [Tile a] -> [Tile a]
getLastRow = sortRow . filter (null . down . matches)

sortRow :: [Tile a] -> [Tile a]
sortRow [] = []
sortRow ts = r0 : next r0
  where
    r0 = head $ filter (null . left . matches) ts
    next r = case filter (\x -> [tile x] == (right . matches) r) ts of
      nextTile : _ -> nextTile : next nextTile
      []           -> []

getPrevRow :: [[Tile a]] -> [Tile a] -> [Tile a]
getPrevRow gr ts = sortRow nextRow
  where
    nextRowTiles = concatMap (up . matches) $ head gr
    nextRow = filter (\x -> tile x `elem` nextRowTiles) ts

concatRow :: [Tile a] -> [[a]]
concatRow (t:ts) = foldl' aux (grid t) ts
  where
    aux gr = zipWith (++) gr . grid

toGridTile :: [String] -> Tile Char
toGridTile css = toTile (t, css')
  where
    t = sum $ map (length . filter (=='#')) css
    lr = map (\x-> "." ++ x ++ ".") css
    css' = (head lr : lr ) ++ [head lr]

----- checking for monsters ---------- 

checkMonsterChar :: (Char, Char) -> Bool
checkMonsterChar ('#', '#') = True
checkMonsterChar ('#', _)   = False
checkMonsterChar _          = True

searchMonsterRow :: [String] -> Int
searchMonsterRow []    = 0
searchMonsterRow [_]   = 0
searchMonsterRow [_,_] = 0   -- at least 3 rows to search
searchMonsterRow rs    = tot + searchMonsterRow rs''
  where
    z = zipWith zip seaMonster
    isMonster = and $ concatMap (map checkMonsterChar) $ z rs
    (tot, rs') = if isMonster then (1, map (drop 20) rs) else (0, map tail rs)
    rs'' = if monsterLen < head (map length rs') then rs' else []

searchMonster :: [String] -> Int
searchMonster (r0:r1:r2:rs) = searchMonsterRow [r0,r1,r2] + searchMonster (r1:r2:rs)
searchMonster _             = 0

monsters :: Tile Char -> Int
monsters = maximum . map (searchMonster . grid) . allStates

notMonsters :: Tile Char -> Int
notMonsters t = tile t - 15 * monsters t

----- output ----------

output = (++ "\n") . show

----- test ------------

test = (solve . input) inputTest

inputTest = "Tile 2311:\n..##.#..#.\n##..#.....\n#...##..#.\n####.#...#\n##.##.###.\n##...#.###\n.#.#.#..##\n..#....#..\n###...#.#.\n..###..###\n\nTile 1951:\n#.##...##.\n#.####...#\n.....#..##\n#...######\n.##.#....#\n.###.#####\n###.##.##.\n.###....#.\n..#.#..#.#\n#...##.#..\n\nTile 1171:\n####...##.\n#..##.#..#\n##.#..#.#.\n.###.####.\n..###.####\n.##....##.\n.#...####.\n#.##.####.\n####..#...\n.....##...\n\nTile 1427:\n###.##.#..\n.#..#.##..\n.#.##.#..#\n#.#.#.##.#\n....#...##\n...##..##.\n...#.#####\n.#.####.#.\n..#..###.#\n..##.#..#.\n\nTile 1489:\n##.#.#....\n..##...#..\n.##..##...\n..#...#...\n#####...#.\n#..#.#.#.#\n...#.#.#..\n##.#...##.\n..##.##.##\n###.##.#..\n\nTile 2473:\n#....####.\n#..#.##...\n#.##..#...\n######.#.#\n.#...#.#.#\n.#########\n.###.#..#.\n########.#\n##...##.#.\n..###.#.#.\n\nTile 2971:\n..#.#....#\n#...###...\n#.#.###...\n##.##..#..\n.#####..##\n.#..####.#\n#..#.#..#.\n..####.###\n..#.#.###.\n...#.#.#.#\n\nTile 2729:\n...#.#.#.#\n####.#....\n..#.#.....\n....#..#.#\n.##..##.#.\n.#.####...\n####.#.#..\n##.####...\n##..#.##..\n#.##...##.\n\nTile 3079:\n#.#.#####.\n.#..######\n..#.......\n######....\n####.#..#.\n.#...#.##.\n#.#####.##\n..#.###...\n..#.......\n..#.###..."