main = interact $ output . solve . input

----- types -----------

data Op   = S | P deriving Show
data Brac = OB | CB deriving Show
data Calc = Val Int | Op Op | Brac Brac deriving Show

----- input -----------

input = (map parseInput) . lines

parseInput :: String -> [Char]
parseInput = filter (/=' ')

----- solve -----------

solve = sum . (map (getVal . calc . toCalcList))

toCalc :: Char -> Calc
toCalc x
    | x == '('  = Brac OB
    | x == ')'  = Brac CB
    | x == '+'  = Op S
    | x == '*'  = Op P
    | otherwise = Val (read [x])

toCalcList :: String -> [Calc]
toCalcList = map toCalc


consume :: [Calc] -> Calc -> [Calc]
consume ((Op S):(Val b):cs)         (Val a)   = consume cs (Val (a + b))
consume ((Val a):(Brac OB):cs)      (Brac CB) = consume cs (Val a)
consume ((Val a):(Op P):(Val b):cs) (Op P)    = consume ((Val (a*b)):cs) (Op P)
consume ((Val a):(Op P):(Val b):cs) (Brac CB) = consume ((Val (a*b)):cs) (Brac CB)
consume cs                          c         = c:cs

calc :: [Calc] -> Calc
calc cs = head $ foldl consume [Brac OB] (cs ++ [Brac CB])

getVal :: Calc -> Int
getVal (Val x) = x

----- output ----------

output = (++ "\n") . show

----- test ------------

test = (solve . input) inputTest

inputTest = "1 + (2 * 3) + (4 * (5 + 6))\n((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2\n5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"
ouputTest = [51, 23340, 669060]

-- readable input
-- 1 + (2 * 3) + (4 * (5 + 6))
-- ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
-- 5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))