main = interact solve2

solve1 = (++ "\n") . show . maximum . binStrToInt . decode . lines
solve2 = (++ "\n") . show . findGap . sort . binStrToInt . decode . lines
--559

decodeChar :: Char -> Char
decodeChar c
    | c == 'B' || c == 'R' = '1'
    | otherwise = '0'

decodeString :: String -> String
decodeString = map decodeChar

decode :: [String] -> [String]
decode = map decodeString

binaryStringToInt :: String -> Integer
binaryStringToInt cs = foldr f 0 (zip [9,8..0] cs)
    where f :: (Integer, Char) -> Integer -> Integer
          f (_  , '0') acc = acc
          f (exp, '1') acc = acc + (2^exp)

binStrToInt :: [String] -> [Integer]
binStrToInt = map binaryStringToInt

sort :: Ord a => [a] -> [a]
sort []     = []
sort (x:xs) = (sort [y | y <- xs, y<=x]) ++ [x] ++ (sort [y | y <- xs, y>x])

findGap :: [Integer] -> Integer
findGap (x0:x1:xs)
    | x0 + 2 == x1  = x0+1
    | otherwise     = findGap (x1:xs)