import Data.Maybe

main = interact $ output . solve . input

----- types -----------
type Rule   = (String, [Range])
type Ticket = [Int]
type Range  = (Int, Int)

----- input -----------

input :: String -> ([Rule], Ticket, [Ticket])
input = applyParsers . splitParts . lines

readInt a = read a :: Int

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn el = reverse . splitOn' el [[]]

splitOn' :: Eq a => a -> [[a]] -> [a] -> [[a]]
splitOn' el xs     []     = xs
splitOn' el (x:xs) (y:ys) =
  if el == y then splitOn' el ([] : x : xs) ys else splitOn' el ((x ++ [y]):xs) ys

splitParts :: [String] -> [[String]]
splitParts = splitOn ""

applyParsers :: [[String]] -> ([Rule], Ticket, [Ticket])
applyParsers [a, b, c] = (parseRules a, (head . parseTickets) b, parseTickets c)

parseTickets :: [String] -> [Ticket]
parseTickets = map parseTicket . tail

parseTicket :: String -> Ticket
parseTicket xs = map readInt $ splitOn ',' xs

parseRules :: [String] -> [Rule]
parseRules = map parseRule

parseRule :: String -> Rule
parseRule xs = (name, parseRanges ranges)
  where
    [name, ranges] = splitOn ':' xs

parseRanges :: String -> [Range]
parseRanges (' ':xs) = parseRanges xs
parseRanges xs       = map parseRange $ filter (/="or") $ words xs

parseRange :: String -> Range
parseRange xs = pairs $ map readInt $ splitOn '-' xs
  where
    pairs [a, b] = (a, b)

----- solve -----------

solve :: ([Rule], Ticket, [Ticket]) -> Int
solve (rs, t, ts) = (prodDeps t) $ match $ startMatcher $ filterInvalidTickets rs (t:ts)

inRange :: Int -> Range -> Ordering
inRange x (a, b)
    | a <= x && x <= b = EQ
    | x < a            = LT
    | x > b            = GT
    | otherwise        = error "lower range bound greater then upper range bound"

satisfiesRule :: Int -> Rule -> Bool
satisfiesRule _ (_, [])     = False
satisfiesRule v (_, (range:ranges)) =
  case v `inRange` range of
    LT -> False
    EQ -> True
    GT -> satisfiesRule v ("", ranges)

satisfiesSomeRule :: Int -> [Rule] -> Bool
satisfiesSomeRule v []           = False
satisfiesSomeRule v (r:rs) = v `satisfiesRule` r || v `satisfiesSomeRule` rs

validTicket :: Ticket -> [Rule] -> Maybe Ticket
validTicket t rs =
  case all (`satisfiesSomeRule` rs) t of
    True  -> Just t
    False -> Nothing

filterInvalidTickets :: [Rule] -> [Ticket] -> ([Rule], [Ticket])
filterInvalidTickets rs ts = (rs, catMaybes $ map (`validTicket` rs) ts)

--- futures

--- ideia:
--- definir rss = repeat rs :: [[Rule]]
--- daí, faz fold em zip t rss pra cada t em ts
--- a ideia é a saida ser um novo rss removendo as impossibilidades

-- rss
eliminate :: [[Rule]] -> Ticket -> [[Rule]]
eliminate rss is = foldr f [] $ zip rss is
  where
    f :: ([Rule], Int) -> [[Rule]] -> [[Rule]]
    f (rs, i) rss = [r | r <-rs, i `satisfiesRule` r]:rss

findFixed :: [[Rule]] -> [Rule]
findFixed rss = catMaybes $ map f rss
  where
    f rs = case length rs of
      0 -> error "eliminated evry rule"
      1 -> Just (head rs)
      _ -> Nothing

fixate :: [Rule] -> [[Rule]] -> [[Rule]]
fixate fixedRules rss = map f rss
  where
    f rs = case length rs of
      0 -> error "eliminated evry rule"
      1 -> rs
      _ -> [r | r <-rs, (r `elem` fixedRules) == False]

startMatcher :: ([Rule], [Ticket]) -> ([[Rule]], [Ticket]) 
startMatcher (rs, ts) = (repeat rs, ts)

matchRules :: ([[Rule]], [Ticket]) -> ([[Rule]], [Ticket])
matchRules (rs, ts) = (foldr f rs ts, ts)
  where
    f :: Ticket -> [[Rule]] -> [[Rule]]
    f t rss = fixate (findFixed eliminated) eliminated
      where
        eliminated = eliminate rss t

matchedAll :: [[Rule]] -> Bool
matchedAll rss = all (==1) $ map length rss

extractRuleName :: Rule -> String
extractRuleName (name, _) = name

match :: ([[Rule]], [Ticket]) -> [String]
match x
    | matchedAll rss = map extractRuleName $ concat rss
    | otherwise      = match (rss, ts)
  where
    (rss, ts) = matchRules x

prodDeps :: Ticket -> [String] -> Int
prodDeps is cs = foldr f 1 $ zip is cs
  where
    f :: (Int, String) -> Int -> Int
    f (i, cs) acc =
      case head $ words cs of
        "departure" -> acc * i
        _           -> acc

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest
raw = input inputTest

inputTest = "class: 0-1 or 4-19\ndeparture time: 0-5 or 8-19\ndeparture: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9\n20,5,1\n22,23,24"

-- readable inputTest:
-- class: 0-1 or 4-19
-- departure time: 0-5 or 8-19
-- departure: 0-13 or 16-19

-- your ticket:
-- 11,12,13

-- nearby tickets:
-- 3,9,18
-- 15,1,5
-- 5,14,9
-- 20,5,1
-- 22,23,24