----- imports ---------
import qualified Data.IntMap.Strict as M

main = interact $ output . solve . input

----- data ------------

data Trit = Zero | One | X deriving Show

type Mask = [Trit]
type Memory = (Int, Int)
type CodeLine = Either Mask Memory
type Prog = [CodeLine]

type Ram = M.IntMap Int
type Comp = (Mask, Ram)

----- input -----------
input :: String -> Prog
input = (map readLine) . (map words) . lines

readInt x = (read x :: Int)

readTrit :: Char -> Trit
readTrit 'X' = X
readTrit '0' = Zero
readTrit '1' = One

readMask :: String -> Mask
readMask = map readTrit

readAdress :: String -> Int
readAdress = readInt . (drop 4) . init

readMemory :: String -> String -> Memory
readMemory adress val = (readAdress adress, readInt val)

readLine :: [String] -> CodeLine
readLine ("mask":"=":mask:[]) = Left  (readMask mask)
readLine (adress:"=":val:[])  = Right (readMemory adress val)
readLine xs = error $ "unexpected input: " ++ concat xs


----- solve -----------

solve = accumulate . (compute newComp)

newComp :: Comp
newComp = ([], M.empty)

applyTrit :: (Int, Trit) -> Int -> Int
applyTrit (exp, X)    v = v
applyTrit (exp, One)  v = if (v `div` (2^exp)) `mod` 2 == 1
                          then v
                          else v + (2^exp)
applyTrit (exp, Zero) v = if (v `div` (2^exp)) `mod` 2 == 0
                          then v
                          else v - (2^exp)

applyMask :: Mask -> Int -> Int
applyMask m v = foldr applyTrit v $ zip [35,34..0] m

writeMemory :: Mask -> Ram -> Memory -> Ram 
writeMemory m ram (k, v) = M.insert k (applyMask m v) ram

computeLine :: Comp -> CodeLine -> Comp
computeLine (m, ram) (Left m')   = (m', ram)
computeLine (m, ram) (Right mem) = (m, writeMemory m ram mem)

compute :: Comp -> Prog -> Comp
compute c [] = c
compute c (l:ls) = compute (computeLine c l) ls


accumulate :: Comp -> Int
accumulate (_, ram) = foldr (+) 0 ram

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0"

inputTest2 = "mask = X101011X011X10101011000001X00XX0X000\nmem[60126] = 9674686\nmem[39254] = 523988\nmem[54849] = 40771927\nmem[29690] = 10110\nmem[10782] = 975308\nmem[43128] = 4347\nmask = 1X0X00101111100010100X001010XX0X0XXX\nmem[60704] = 43881206\nmem[63842] = 12369309\nmem[45876] = 33941457\nmem[7001] = 347\nmem[57168] = 5484326\nmem[9010] = 1526598"