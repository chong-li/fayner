joinSameID :: [String] -> [String]
joinSameID [x0] = [x0]
joinSameID (x0:"":xs) = [x0] ++ joinSameID xs
joinSameID (x0:x1:xs) = joinSameID $ (x0 ++ " " ++ x1) : xs

getFields1 :: [String] -> [[String]]
getFields1 xs = foldr aux [] xs
    where aux :: String -> [[String]] -> [[String]]
          aux x ys = ((take 3) <$> (words x)) : ys

whichFields :: [String] -> [String] -> [Bool]
whichFields xs ys = foldr (aux ys) [] xs
    where aux :: [String] -> String -> [Bool] -> [Bool]
          aux ys x bs = (x `elem` ys) : bs

areAllFields :: [String] -> [String] -> Bool
areAllFields fields xs = all (== True) (whichFields fields xs)

areP1Fields = areAllFields ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

sumValids :: [Bool] -> Int
sumValids = length . (filter (== True))

solve1 = (++ "\n") . show . sumValids . (map areP1Fields) . getFields1 . joinSameID . lines

data Field = Field String String

getField :: String -> Field
getField s = Field (take 3 s) (drop 4 s)

readMaybeInt ::  String -> Maybe Int
readMaybeInt s = case reads s of
                  [(val, "")] -> Just val
                  _           -> Nothing

isMaybeIntInRange :: Maybe Int -> Int -> Int -> Bool
isMaybeIntInRange Nothing _ _   = False
isMaybeIntInRange (Just i) l u  = l <= i && i <= u

isInt :: Maybe Int -> Bool
isInt Nothing = False
isInt _       = True

isHeight :: String -> String -> Bool
isHeight s "cm" = isMaybeIntInRange (readMaybeInt s) 150 193
isHeight s "in" = isMaybeIntInRange (readMaybeInt s) 59 76
isHeight _ _ = False

isHairColor :: String -> Bool
isHairColor ('#':s) = 6 == (length $ filter (`elem` "0123456789abcdef") s)
isHairColor _       = False

isFieldValid :: Field -> Bool
isFieldValid (Field "byr" s) = isMaybeIntInRange (readMaybeInt s) 1920 2002
isFieldValid (Field "iyr" s) = isMaybeIntInRange (readMaybeInt s) 2010 2020
isFieldValid (Field "eyr" s) = isMaybeIntInRange (readMaybeInt s) 2020 2030
isFieldValid (Field "hgt" s) = isHeight ((init.init) s) ((last $ init s):[last s])
isFieldValid (Field "hcl" s) = (length s == 7) && isHairColor s
isFieldValid (Field "ecl" s) = s `elem` ["amb","blu","brn","gry","grn","hzl","oth"]
isFieldValid (Field "pid" s) = (length s == 9) && isInt (readMaybeInt s)
isFieldValid _               = False

fieldName :: Field -> String
fieldName (Field s _) = s

getFields2 :: [String] -> [[Field]]
getFields2 xs = foldr aux [] xs
    where aux :: String -> [[Field]] -> [[Field]]
          aux x ys = ((getField) <$> (words x)) : ys

whichValidFields :: [String] -> [Field] -> [Bool]
whichValidFields xs ys = foldr (aux (filter isFieldValid ys)) [] xs
    where aux :: [Field] -> String -> [Bool] -> [Bool]
          aux ys x bs = (x `elem` (fieldName <$> ys)) : bs

areAllFields2 :: [String] -> [Field] -> Bool
areAllFields2 fields xs = all (== True) (whichValidFields fields xs)

areP2Fields = areAllFields2 ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

solve2 = (++ "\n") . show . sumValids . (map areP2Fields) . getFields2 . joinSameID . lines

main = interact solve2