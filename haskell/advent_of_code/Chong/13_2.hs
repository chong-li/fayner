main = interact $ output . solve . input

type Bus = (Integer, Integer)

----- input -----------

input = tail . filterJust . (map readMaybePair) . (zip [(-1)..]) . (splitWhen isDigitOrX)

readInteger a = (read a :: Integer)

readMaybePair :: (Integer, String) -> Maybe Bus
readMaybePair (i, "x") = Nothing
readMaybePair (i, dig) = Just (i, readInteger dig)

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust _       = True

filterJust :: [Maybe a] -> [a]
filterJust []              = []
filterJust ((Just x) : xs) = x : (filterJust xs)
filterJust (Nothing  : xs) = filterJust xs

isDigitOrX :: Char -> Bool
isDigitOrX = (`elem` 'x' : ['0'..'9'])

prependNewListOrToHead :: (a -> Bool) -> a -> [[a]] -> [[a]]
prependNewListOrToHead f x ([]:ys) =
    case f x of
        False -> []  : ys
        True  -> [x] : ys
prependNewListOrToHead f x (y:ys) =
    case f x of
        False -> [] : y  : ys
        True  -> (x : y) : ys

splitWhen :: (a -> Bool) -> [a] -> [[a]]
splitWhen f = foldr (prependNewListOrToHead f) [[]]

----- solve -----------

solve = solveEuler

solveChinese :: [Bus] -> Integer
solveChinese = timestamp . (foldr1 joinBuses)

solveSieve :: [Bus] -> Integer
solveSieve = timestamp . (foldr1 match)

solveEuler :: [Bus] -> Integer
solveEuler = timestamp . (foldr1 euler)

euc :: Integral a => a -> a -> (a, a, a)
euc a b = eucStep (a, 1, 0) (b, 0, 1)

eucStep :: Integral a => (a, a, a) -> (a, a, a) -> (a, a, a)
eucStep (a, s, t) (0 , _, _)   = (a, s, t)
eucStep (a, s, t) (a', s', t') = eucStep (a', s', t') (a'', s'', t'')
  where
      q   = a `div` a'
      a'' = a `mod` a'
      s'' = s - q*s'
      t'' = t - q*t'

joinBuses :: Bus -> Bus -> Bus
joinBuses (n, bn) (m, bm) =
    let (_, sn, sm) = euc bn bm
        b = bn * bm
    in ((m*bn*sn + n*bm*sm) `mod` b, b)

timestamp :: Bus -> Integer
timestamp (n, bn) = bn - n

----- output ----------

output = (++ "\n") . show

----- Sieve -- slow AF --

match :: Bus -> Bus -> Bus
match (a, b) (c, d) =
    let
        q  = d `div` b
        xs = [(a + q*b), (a + (q+1)*b)..]
        ys = [(c+d), (c+2*d)..]
    in
        (match' xs ys, b*d)

match' :: Ord a => [a] -> [a] -> a
match' (x:xs) (y:ys)
    | x < y = match' xs (y:ys)
    | x > y = match' (x:xs) ys
    | otherwise = x

----- Brute Sieve is Euler Sieve ----

-- pensa num exemplo que começa com "x, 7, x, 59". Tem o (7, 1) e o (59, 3)
-- precisa ser que, pra algum k, (59k+3) % 7 == 1, daí você pode distribuir o
-- %7 pra dentro e ficar com (3k+3) % 7 == 1
-- Como 7 é primo, existe algum k entre 0 e 6 que satisfaz a equação
-- Nesse caso você brute força um 4

-- Melhor ainda, de (3k+3) % 7 == 1 dá pra +4 dos dois lados e ficar com
-- 3k % 7 == 5
-- Como 7 é primo ZZ{7} é corpo o 3 vai ter inversa.
-- Inversa vai ser 3^5, a soma dos expoentes tem que dar 7-1 == 6
-- (5*3^5) % 7 == k . Aqui k sai igual a 4 também

-- Daí sai um novo busId pra substituir os 2: (7*59, 59*4+3) == (413, 239)
-- Enfim, desse jeito o brute force não é no tamanho dos produtos que vai
-- acumulando, mas sim nos valores de entrada
-- Deve rodar rápido um baseado em bruteforçar o menor dos caras

euler :: Bus -> Bus -> Bus
euler (i0, b0) (i1, b1)
    | b0 > b1   = euler (i1, b1) (i0, b0)
    | otherwise = (b1*z + i1, b0*b1)
    where
        x = b1 `mod` b0
        y = (i0-i1) `mod` b0
        z = (y * x^(b0-2)) `mod` b0


----- test ------------

test = (solve . input) inputTest

inputTest = "939\n7,13,x,x,59,x,31,19"