import qualified Data.Set as S

main = interact $ output . solve . input


----- types -----------

type Point = (Int, Int, Int, Int)
type Cubes = S.Set Point

----- input -----------
input :: String -> [(Point, Char)]
input = index . lines

enumerate :: [a] -> [(Int, a)]
enumerate = zip [0..]

makePointValuePair :: Int -> (Int, a) -> (Point, a)
makePointValuePair x (y, val) = ((x, y, 0, 0), val)

index :: [[a]] -> [(Point, a)]
index = concat . (map insertPoint) . enumerate . (map enumerate)
    where -- insertPoint :: (Int, [(Int, a)]) -> [(Point, a)]
        insertPoint (r, xs) = map (makePointValuePair r) xs

----- solve -----------

solve = (march 6) . buildInacs . initCubes . filterVoid

opt = [-1,0,1] :: [Int]

adjPoints :: Point -> [Point]
adjPoints (a,b,c,d) = [(a+x, b+y, c+z, d+w)
                        | x <-opt, y <-opt, z <-opt, w <-opt, (x,y,z,w)/=(0,0,0,0)]

activeAdj :: Cubes -> Point -> Int
activeAdj s p = length $ filter (==True) $ map (`S.member` s) (adjPoints p)

filterVoid :: [(Point, Char)] -> [Point]
filterVoid pas = [p | (p, a) <- pas, a == '#']

initCubes :: [Point] -> (Cubes, Cubes)
initCubes ps = (S.fromList ps, S.empty)

inactiveAdj :: Point -> (Cubes, Cubes) -> (Cubes, Cubes)
inactiveAdj p (ac, inac) = (ac, inac `S.union` new_inacs)
  where
    new_inacs = S.fromList (adjPoints p) `S.difference` ac

buildInacs :: (Cubes, Cubes) -> (Cubes, Cubes)
buildInacs c@(act, _) = foldr inactiveAdj c act

remainAct :: Cubes -> Cubes
remainAct act = foldr f S.empty act
  where -- f :: Point -> Cubes -> Cubes
    f p newAct
        | (activeAdj act p) `elem` [2,3] = S.insert p newAct
        | otherwise                      = newAct

activate :: Cubes -> Cubes -> Cubes
activate act inact = foldr f S.empty inact
  where -- f :: Point -> Cubes -> Cubes
    f p newInact
        | (activeAdj act p) == 3 = S.insert p newInact
        | otherwise                = newInact

marchCubes :: (Cubes, Cubes) -> (Cubes, Cubes)
marchCubes (act, inact) = buildInacs (newAct, S.empty)
  where
    newAct = (remainAct act) `S.union` (activate act inact)

march :: Int -> (Cubes, Cubes) -> Int
march 0 (act, _) = S.size act
march n c        = march (n-1) (marchCubes c)

----- output ----------

output = (++ "\n") . show


----- test ------------

test = (solve . input) inputTest

inputTest = ".#.\n..#\n###"

--- readable input
-- .#.
-- ..#
-- ###