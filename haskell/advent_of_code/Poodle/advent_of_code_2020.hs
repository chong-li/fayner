-- advent_of_code_2020.hs
module AoC2020 where

import System.IO (isEOF)
import Data.List (partition, isPrefixOf)
import Data.Bits (setBit, clearBit, Bits, shiftR, (.&.))

-- 01a - find the two entries that sum to 2020 and then multiply those two numbers together
getLines :: IO [[Char]]
getLines = do
             end <- isEOF
             if end then return []
             else do
               l <- getLine
               if l == "EOF"
               then return []
               else do
                 ls <- getLines
                 return (l:ls)

find2AndMultiply :: Int -> [Int] -> Int
find2AndMultiply _ [] = -1
find2AndMultiply _ [x] = -1
find2AndMultiply n (x:xs) = let r = aux x xs in if r < 0 then find2AndMultiply n xs else r
            where aux :: Int -> [Int] -> Int
                  aux _ [] = -1
                  aux x0 (x:xs) = if x0+x == n then x0*x else aux x0 xs

aoc01a :: IO Int
aoc01a = do
           ls <- getLines
           return $ find2AndMultiply 2020 $ map read ls

-- 01b - the product of the three entries that sum to 2020
find3AndMultiply :: Int -> [Int] -> Int
find3AndMultiply _ [] = -1
find3AndMultiply _ [x0] = -1
find3AndMultiply _ [x0,x1] = -1
find3AndMultiply n (x0:xs) = let r = find2AndMultiply (n-x0) xs 
                             in if r < 0
                                then find3AndMultiply n xs
                                else x0*r

aoc01b :: IO Int
aoc01b = do
           ls <- getLines
           return $ find3AndMultiply 2020 $ map read ls

-- 02a - validate passwords with rules 'min-max char: password', ex.: "1-3 a: abcde"
isDigit :: Char -> Bool
isDigit c = c >= '0' && c <= '9'

isLetter :: Char -> Bool
isLetter c = c >= 'a' && c <= 'z'

parsePWRule :: String -> (Int,Int,Char,String)
parsePWRule x0s = let (min,x1s) = span isDigit x0s
                      x2s = dropWhile (not.isDigit) x1s
                      (max,x3s) = span isDigit x2s
                      x4s = dropWhile (not.isLetter) x3s
                      ch = head x4s
                      pw = dropWhile (not.isLetter) (tail x4s)
                  in (read min, read max, ch, pw)

aoc02a :: IO Int
aoc02a = do
           ls <- getLines
           return $ foldr validCount 0 $ map parsePWRule ls
         where validCount t b = if validPW t then 1+b else b
               validPW (min,max,ch,pw) = let n = foldr (\c b -> if c == ch then 1+b else b) 0 pw
                                         in n >= min && n <= max

-- 02b - validate passwords with rules 'pos1-pos2 char: password',
--       with 'char' in only one of de two positions (starting from indice 1)
aoc02b :: IO Int
aoc02b = do
           ls <- getLines
           return $ foldr validCount 0 $ map parsePWRule ls
         where validCount t b = if validPW t then 1+b else b
               validPW (_,_,_,[]) = False
               validPW (pos1,pos2,ch,c:cs)
                 | pos1 == 1 = (c == ch) /= validPW (pos1-1,pos2-1,ch,cs)
                 | pos2 == 1 = c == ch
                 | otherwise = validPW (pos1-1,pos2-1,ch,cs)

-- 03a - count trees ('#') on slope right 3 and down 1, with lines repeating to right
countTreesOnSlope :: [[Char]] -> (Int,Int) -> Int -> Int -> Int -> Int
countTreesOnSlope [] _ _ _ _ = 0
countTreesOnSlope (l:ls) (r,d) x 0 m = if l!!x == '#'
                                       then 1 + countTreesOnSlope ls (r,d) (mod (x+r) m) (d-1) m
                                       else countTreesOnSlope ls (r,d) (mod (x+r) m) (d-1) m
countTreesOnSlope (l:ls) slope x y m = countTreesOnSlope ls slope x (y-1) m

aoc03a :: IO Int
aoc03a = do
           ls <- getLines
           let m = length $ head ls
           return $ countTreesOnSlope ls (3,1) 0 0 m

-- 03b - count trees on slopes (1,1),(3,1),(5,1),(7,1),(1,2) and multiply de results
aoc03b :: IO Int
aoc03b = do
           ls <- getLines
           let m = length $ head ls
               s0 = countTreesOnSlope ls (1,1) 0 0 m
               s1 = countTreesOnSlope ls (3,1) 0 0 m
               s2 = countTreesOnSlope ls (5,1) 0 0 m
               s3 = countTreesOnSlope ls (7,1) 0 0 m
               s4 = countTreesOnSlope ls (1,2) 0 0 m
           return $ s0*s1*s2*s3*s4

-- 04a - Count the number of valid passports - those that have all required fields. Treat 'cid' as optional
validPassportA :: (String,String,String,String,String,String,String,String) -> Bool
validPassportA (byr,iyr,eyr,hgt,hcl,ecl,pid,_) = byr /= "" && iyr /= "" && eyr /= "" && hgt /= "" &&
                                                 hcl /= "" && ecl /= "" && pid /= ""

organizePassports:: [String] -> (String,String,String,String,String,String,String,String) ->
                    [(String,String,String,String,String,String,String,String)]
organizePassports [] ("","","","","","","","") = []
organizePassports [] pp = pp:[]
organizePassports ("":ls) ("","","","","","","","") = organizePassports ls ("","","","","","","","")
organizePassports ("":ls) pp = pp: organizePassports ls ("","","","","","","","")
organizePassports (l:ls) pp0 =
        let pp1 = getPpFields l pp0
        in organizePassports ls pp1
        where getPpFields "" pp = pp
              getPpFields (' ':cs) pp = getPpFields cs pp
              getPpFields ('b':'y':'r':':':cs) (_,iyr,eyr,hgt,hcl,ecl,pid,cid) =
                let (byr,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('i':'y':'r':':':cs) (byr,_,eyr,hgt,hcl,ecl,pid,cid) =
                let (iyr,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('e':'y':'r':':':cs) (byr,iyr,_,hgt,hcl,ecl,pid,cid) =
                let (eyr,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('h':'g':'t':':':cs) (byr,iyr,eyr,_,hcl,ecl,pid,cid) =
                let (hgt,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('h':'c':'l':':':cs) (byr,iyr,eyr,hgt,_,ecl,pid,cid) =
                let (hcl,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('e':'c':'l':':':cs) (byr,iyr,eyr,hgt,hcl,_,pid,cid) =
                let (ecl,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('p':'i':'d':':':cs) (byr,iyr,eyr,hgt,hcl,ecl,_,cid) =
                let (pid,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)
              getPpFields ('c':'i':'d':':':cs) (byr,iyr,eyr,hgt,hcl,ecl,pid,_) =
                let (cid,tail) = span (/=' ') cs in getPpFields tail (byr,iyr,eyr,hgt,hcl,ecl,pid,cid)

aoc04a :: IO Int
aoc04a = do
           ls <- getLines
           return $ sum $ map (fromEnum . validPassportA) $ organizePassports ls ("","","","","","","","")

-- 04b - Count the number of valid passports - those that have all required fields and valid values.
--       Continue to treat cid as optional
validByr :: String -> Bool
validByr byr = if byr /= [] && all isDigit byr then let n = read byr in n >= 1920 && n <= 2002 else False

validIyr :: String -> Bool
validIyr iyr = if iyr /= [] && all isDigit iyr then let n = read iyr in n >= 2010 && n <= 2020 else False

validEyr :: String -> Bool
validEyr eyr = if eyr /= [] && all isDigit eyr then let n = read eyr in n >= 2020 && n <= 2030 else False

validHgt :: String -> Bool
validHgt hgt = let (num,unit) = span isDigit hgt
               in if num == [] then False
                  else let n = read num
                       in case unit of
                            "cm" -> n >= 150 && n <= 193
                            "in" -> n >= 59 && n <= 76
                            _ -> False

isHex :: Char -> Bool
isHex c = (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f')

validHcl :: String -> Bool
validHcl ('#':hex) = length hex == 6 && all isHex hex
validHcl _ = False

validEcl :: String -> Bool
validEcl ecl = elem ecl ["amb","blu","brn","gry","grn","hzl","oth"]

validPid :: String -> Bool
validPid pid = length pid == 9 && all isDigit pid

validPassportB :: (String,String,String,String,String,String,String,String) -> Bool
validPassportB (byr,iyr,eyr,hgt,hcl,ecl,pid,_) =
  validByr byr && validIyr iyr && validEyr eyr && validHgt hgt &&
  validHcl hcl && validEcl ecl && validPid pid

aoc04b :: IO Int
aoc04b = do
           ls <- getLines
           return $ sum $ map (fromEnum . validPassportB) $ organizePassports ls ("","","","","","","","")

-- 05a - What is the highest seat ID on a boarding pass? Ex: "BFFFBBFRRR" - row 70, column 7, seat ID 567 (70*8+7)
binaryToInt :: String -> Int -> Int
binaryToInt [] n = n
binaryToInt (c:cs) n
  | c == 'B' || c == 'R' || c == '1' = binaryToInt cs (2*n+1)
  | c == 'F' || c == 'L' || c == '0' = binaryToInt cs (2*n)

aoc05a :: IO Int
aoc05a = do
           ls <- getLines
           return $ maximum $ map (`binaryToInt` 0) ls

-- 05b - What is the ID of your seat? the seats with IDs +1 and -1 from yours will be in your list.
quickSort :: Ord a => [a] -> [a]
quickSort [] = []
quickSort (x:xs) = let (ls, gs) = partition (<x) xs
                  in (quickSort ls) ++ (x:(quickSort gs))

findMySeat :: [Int] -> Int
findMySeat (x0:x1:xs) = if x1 == x0+2 then x0+1 else findMySeat (x1:xs)

aoc05b :: IO Int
aoc05b = do
           ls <- getLines
           return $ findMySeat $ quickSort $ map (`binaryToInt` 0) ls

-- 06a - For each group, count the number of questions to which anyone answered "yes". What is the sum of those counts?
type AZtuple a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)

newAZtuple :: a -> AZtuple a
newAZtuple µ = (µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ,µ)

setAZtupleIn :: a -> AZtuple a -> Char -> AZtuple a
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'a' = (µ,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'b' = (a,µ,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'c' = (a,b,µ,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'd' = (a,b,c,µ,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'e' = (a,b,c,d,µ,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'f' = (a,b,c,d,e,µ,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'g' = (a,b,c,d,e,f,µ,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'h' = (a,b,c,d,e,f,g,µ,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'i' = (a,b,c,d,e,f,g,h,µ,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'j' = (a,b,c,d,e,f,g,h,i,µ,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'k' = (a,b,c,d,e,f,g,h,i,j,µ,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'l' = (a,b,c,d,e,f,g,h,i,j,k,µ,m,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'm' = (a,b,c,d,e,f,g,h,i,j,k,l,µ,n,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'n' = (a,b,c,d,e,f,g,h,i,j,k,l,m,µ,o,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'o' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,µ,p,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'p' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,µ,q,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'q' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,µ,r,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'r' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,µ,s,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 's' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,µ,t,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 't' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,µ,u,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'u' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,µ,v,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'v' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,µ,w,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'w' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,µ,x,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'x' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,µ,y,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'y' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,µ,z)
setAZtupleIn µ (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) 'z' = (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,µ)

sumAZtuple :: Num a => AZtuple a -> a
sumAZtuple (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) = a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p+q+r+s+t+u+v+w+x+y+z

organizeAnswers:: [String] -> AZtuple Int -> [AZtuple Int]
organizeAnswers [] azt = if sumAZtuple azt == 0
                         then []
                         else azt:[]
organizeAnswers ("":ls) azt = if sumAZtuple azt == 0
                              then organizeAnswers ls (newAZtuple 0)
                              else azt: organizeAnswers ls (newAZtuple 0)
organizeAnswers (l:ls) azt0 = let azt1 = foldl (setAZtupleIn 1) azt0 l
                              in organizeAnswers ls azt1

aoc06a :: IO Int
aoc06a = do
           ls <- getLines
           return $ sum $ map sumAZtuple $ organizeAnswers ls (newAZtuple 0)

-- 06b - For each group, count the number of questions to which everyone answered "yes". What is the sum of those counts?
operateAZtuples :: (a -> b -> c) -> AZtuple a -> AZtuple b -> AZtuple c
operateAZtuples
  fn 
  (a0,b0,c0,d0,e0,f0,g0,h0,i0,j0,k0,l0,m0,n0,o0,p0,q0,r0,s0,t0,u0,v0,w0,x0,y0,z0)
  (a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,m1,n1,o1,p1,q1,r1,s1,t1,u1,v1,w1,x1,y1,z1)
  = (fn a0 a1,fn b0 b1,fn c0 c1,fn d0 d1,fn e0 e1,fn f0 f1,fn g0 g1,fn h0 h1,fn i0 i1,
     fn j0 j1,fn k0 k1,fn l0 l1,fn m0 m1,fn n0 n1,fn o0 o1,fn p0 p1,fn q0 q1,fn r0 r1,
     fn s0 s1,fn t0 t1,fn u0 u1,fn v0 v1,fn w0 w1,fn x0 x1,fn y0 y1,fn z0 z1)

organizeAnswersB:: [String] -> AZtuple Int -> [AZtuple Int]
organizeAnswersB [] azt = if sumAZtuple azt == -26
                          then []
                          else azt:[]
organizeAnswersB ("":ls) azt = if sumAZtuple azt == -26
                               then organizeAnswersB ls (newAZtuple (-1))
                               else azt: organizeAnswersB ls (newAZtuple (-1))
organizeAnswersB (l:ls) azt0 = let azt1 = foldl (setAZtupleIn 1) (newAZtuple 0) l
                               in organizeAnswersB ls $ operateAZtuples (\x y->fromEnum (toEnum (abs x) && toEnum y)) azt0 azt1

aoc06b :: IO Int
aoc06b = do
           ls <- getLines
           return $ sum $ map sumAZtuple $ organizeAnswersB ls (newAZtuple 1)

-- 07a - How many bag colors can eventually contain at least one shiny gold bag?
takeColor :: String -> (String,String)
takeColor (' ':'b':'a':'g':'s':cs) = ("",cs)
takeColor (' ':'b':'a':'g':cs) = ("",cs)
takeColor (c:cs) = let (color, tail) = takeColor cs in (c:color,tail)

takeContents :: String -> [(Int,String)]
takeContents con = let aux0 = dropWhile (not.isDigit) con
                   in if aux0 == ""
                      then []
                      else let (n, aux1) = break isLetter aux0
                               (color, aux2) = takeColor aux1
                           in (read n, color) : takeContents aux2

bagRules :: [String] -> [(String,[(Int,String)])]
bagRules [] = []
bagRules (l:ls) = let (color, aux) = takeColor l
                      contents = takeContents aux
                  in (color, contents) : bagRules ls

canContain :: (String,[(Int,String)]) -> String -> Bool
canContain (_,contents) color = let (_,aux) = unzip contents
                                in elem color aux

allPossibleBags :: [String] -> [(String,[(Int,String)])] -> [(String,[(Int,String)])]
allPossibleBags [] _ = []
allPossibleBags _ [] = []
allPossibleBags (col:cols) rules = let (yes,no) = partition (`canContain` col) rules
                                       (yesCols,_) = unzip yes
                                   in yes ++ allPossibleBags (cols ++ yesCols) (no)

aoc07a :: IO Int
aoc07a = do
           ls <- getLines
           return $ length $ allPossibleBags ["shiny gold"] $ bagRules ls

-- 07b - How many individual bags are required inside your single shiny gold bag?
removeRule :: String -> [(String,[(Int,String)])] -> ((String,[(Int,String)]),[(String,[(Int,String)])])
removeRule _ [] = (error "Can't find! ;þ",[])
removeRule color ((col,cont):rules) = if col == color
                                      then ((col,cont),rules)
                                      else let (r,aux) = removeRule color rules
                                           in (r,(col,cont):aux)

allContents :: String -> [(String,[(Int,String)])] -> Int
allContents color rules = let ((_,contents),rules2) = removeRule color rules
                              aux :: (Int,String) -> Int -> Int
                              aux (i,col) count = count + i + i * allContents col rules2
                          in foldr aux 0 contents

aoc07b :: IO Int
aoc07b = do
           ls <- getLines
           return $ allContents "shiny gold" $ bagRules ls

-- 08a - Immediately before any instruction is executed a second time, what value is in the accumulator?
instruction :: String -> (Int,Int,Int)
instruction ('n':'o':'p':' ':cs) = let (_,i,_) = instruction cs in (0,i,0)
instruction ('a':'c':'c':' ':cs) = let (_,i,_) = instruction cs in (1,i,0)
instruction ('j':'m':'p':' ':cs) = let (_,i,_) = instruction cs in (2,i,0)
instruction ('+':cs) = (undefined,read cs,undefined)
instruction ('-':cs) = (undefined,- read cs,undefined)

bootCode :: [String] -> [(Int,Int,Int)]
bootCode [] = []
bootCode (l:ls) = instruction l : bootCode ls

changePile :: Int -> [(Int,Int,Int)] -> [(Int,Int,Int)] -> ([(Int,Int,Int)],[(Int,Int,Int)])
changePile 0 pile0 pile1 = (pile0,pile1)
changePile val (i:pile0) pile1 = changePile (val-1) pile0 (i:pile1)

runCode :: [(Int,Int,Int)] -> [(Int,Int,Int)] -> Int -> (Bool,Int)
runCode [] _ accumulator = (True,accumulator)
runCode ((_,_,1):_) _ accumulator = (False,accumulator)
runCode ((0,_,_):future) past accumulator = runCode future ((0,0,1):past) accumulator
runCode ((1,val,_):future) past accumulator = runCode future ((1,val,1):past) (accumulator+val)
runCode ((2,val,_):future) past accumulator
  | val >= 0 = let (newFuture,newPast) = changePile val ((2,val,1):future) past
               in runCode newFuture newPast accumulator
  | val < 0 = let (newPast,newFuture) = changePile (abs val) past ((2,val,1):future)
              in runCode newFuture newPast accumulator

aoc08a :: IO Int
aoc08a = do
           ls <- getLines
           return (snd (runCode (bootCode ls) [] 0))

-- 08b - Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp).
--       What is the value of the accumulator after the program terminates?
debugCode :: [(Int,Int,Int)] -> [(Int,Int,Int)] -> Int -> (Bool,Int)
debugCode [] _ accumulator = (True,accumulator)
debugCode ((_,_,1):_) _ accumulator = (False,accumulator)
debugCode ((0,val,_):future) past accumulator = let r = runCode ((2,val,0):future) past accumulator
                                                in if fst r then r
                                                   else debugCode future ((0,val,1):past) accumulator
debugCode ((1,val,_):future) past accumulator = debugCode future ((1,val,1):past) (accumulator+val)
debugCode ((2,val,_):future) past accumulator =
  let r = runCode ((0,val,0):future) past accumulator
  in if fst r then r
     else
       if val >= 0
       then let (newFuture,newPast) = changePile val ((2,val,1):future) past
            in debugCode newFuture newPast accumulator
       else let (newPast,newFuture) = changePile (abs val) past ((2,val,1):future)
            in debugCode newFuture newPast accumulator

aoc08b :: IO Int
aoc08b = do
           ls <- getLines
           return (snd (debugCode (bootCode ls) [] 0))

-- 09a - find the first number in the list (after the preamble) which is not the sum of two of the 25 numbers before it.
findFirstStep :: [Int] -> [Int] -> Int
findFirstStep [] _ = -1
findFirstStep (i:is) base = if find2AndMultiply i (take 25 base) < 0
                            then i
                            else findFirstStep is (i:base)

aoc09a :: IO Int
aoc09a = do
           ls <- getLines
           let is = map read ls
               (pre,seq) = splitAt 25 is
           return $ findFirstStep seq $ reverse pre

-- 09b - find a contiguous set of at least two numbers in your list which sum to the invalid number from step 1.
--       add together the smallest and largest number in this contiguous range.
canSumTo :: Int -> [Int] -> Int
canSumTo k is = aux k is maxBound minBound
  where aux :: Int -> [Int] -> Int -> Int -> Int
        aux _ [] _ _ = 0
        aux k (i:is) min0 max0
          | i < k = aux (k-i) is min1 max1
          | i == k && min1 /= max1 = min1+max1
          | otherwise = 0
          where min1 = min i min0
                max1 = max i max0

findWeakness :: Int -> [Int] -> Int
findWeakness _ [] = -1
findWeakness k is = let r = canSumTo k is
                    in if r > 0 then r
                       else findWeakness k (tail is)

aoc09b :: IO Int
aoc09b = do
           ls <- getLines
           let is = map read ls
               (pre,seq) = splitAt 25 is
               k = findFirstStep seq $ reverse pre
           return $ findWeakness k is

-- 10a - Using all adapters, what is the number of 1-jolt differences multiplied by the number of 3-jolt differences?
joltDifferences :: [Int] -> Int -> (Int,Int,Int)
joltDifferences [] _ = (0,0,1)
joltDifferences (i1:is) i0 = let (d1,d2,d3) = joltDifferences is i1
                           in case i1-i0 of
                              1 -> (d1+1,d2,d3)
                              2 -> (d1,d2+1,d3)
                              3 -> (d1,d2,d3+1)

aoc10a :: IO Int
aoc10a = do
           ls <- getLines
           let is = map read ls
               os = quickSort is
               (d1,d2,d3) = joltDifferences os 0
           return (d1*d3)

-- 10b - What is the total number of distinct ways you can arrange the adapters to connect the charging outlet to your device?
howManyArranges :: [Int] -> [Integer] -> Int -> Integer
howManyArranges [] (r:rs) _ = r
howManyArranges (x:xs) [r2,r1,r0] n = if x == n
                                      then howManyArranges xs [r2+r1+r0,r2,r1] (n+1)
                                      else howManyArranges (x:xs) [0,r2,r1] (n+1)

aoc10b :: IO Integer
aoc10b = do
           ls <- getLines
           let xs = quickSort $ map read ls
           return $ howManyArranges xs [1,0,0] 1

-- 11a - Simulate your seating area by applying the seating rules repeatedly until no seats change state.
--       How many seats end up occupied?
convertSeats :: [Char] -> [Int]
convertSeats [] = []
convertSeats (c:cs) = case c of
                        'L' -> 0: convertSeats cs
                        '.' -> 2: convertSeats cs

newSeated :: [Int] -> [Int] -> [Int] -> [Int]
newSeated _ (_:0:[]) _ = [1]
newSeated _ (_:m1:[]) _ = [m1]
newSeated (f0:f1:f2:fs) (m0:0:m2:ms) (b0:b1:b2:bs)
  | f0+f1+f2+m0+m2+b0+b1+b2 >= 10 = 1:newSeated (f1:f2:fs) (0:m2:ms) (b1:b2:bs)
  | otherwise = 0:newSeated (f1:f2:fs) (0:m2:ms) (b1:b2:bs)
newSeated (_:f1:f2:fs) (_:m1:m2:ms) (_:b1:b2:bs) = m1:newSeated (f1:f2:fs) (m1:m2:ms) (b1:b2:bs)

newFloor :: [Int] -> [Int] -> [Int] -> [Int]
newFloor _ (_:m1:[]) _ = [m1]
newFloor (f0:f1:f2:fs) (m0:0:m2:ms) (b0:b1:b2:bs)
  | f0 == 1 || f1 == 1 || f2 == 1 || m0 == 1 || m2 == 1 || b0 == 1 || b1 == 1 || b2 == 1 = 2:newFloor (f1:f2:fs) (0:m2:ms) (b1:b2:bs)
  | otherwise = 0:newFloor (f1:f2:fs) (0:m2:ms) (b1:b2:bs)
newFloor (_:f1:f2:fs) (_:m1:m2:ms) (_:b1:b2:bs) = m1:newFloor (f1:f2:fs) (m1:m2:ms) (b1:b2:bs)

runUntilStable :: Int -> [Int] -> IO [Int]
runUntilStable n st = do
                        let newStd = newSeated (replicate (n+1) 2 ++ st) (2:st) (2:(drop n st) ++ replicate n 2)
                            newFlr = newFloor (replicate (n+1) 2 ++ newStd) (2:newStd) (2:(drop n newStd) ++ replicate n 2)
                            free = countFree newFlr
                        if free == 0 then return newFlr
                        else runUntilStable n newFlr

countFree :: [Int] -> Int
countFree = sum . (map (\x -> if x==0 then 1 else 0))

countOccupied :: [Int] -> Int
countOccupied = sum . (map (`mod`2))

aoc11a :: IO Int
aoc11a = do
           ls <- getLines
           let n = 1 + length (head ls)
               state0 = concatMap ((2:).convertSeats) ls
           stateF <- runUntilStable n state0
           return $ countOccupied stateF

-- 11b - Given the new visibility method and the rule change for occupied seats becoming empty,
--       once equilibrium is reached, how many seats end up occupied?
type Seat = (Int,Int,[(Int,Int)])
type SeatsGrid = Trie (Trie Int)

convertSeatsB :: [Char] -> Trie Int -> Int -> Int -> (Trie Int,[Seat])
convertSeatsB [] trie _ _ = (trie,[])
convertSeatsB ('.':cs) trie i j = convertSeatsB cs trie i $!(j+1)
convertSeatsB ('L':cs) trie0 i j = let (trie1,vacantSeats) = convertSeatsB cs (setLeaf trie0 j 0) i $!(j+1)
                                   in (trie1,(i,j,[]):vacantSeats)

getSeatState :: SeatsGrid -> Int -> Int -> Maybe Int
getSeatState seatsGrid i j = case getLeaf seatsGrid i of
                               Nothing -> Nothing
                               Just line -> getLeaf line j

setSeatState :: SeatsGrid -> Int -> Int -> Int -> SeatsGrid
setSeatState seatsGrid i j value = let line = case getLeaf seatsGrid i of
                                                Nothing -> Null
                                                Just t -> t
                                   in setLeaf seatsGrid i $! (setLeaf $! line) j value

unsetSeatState :: SeatsGrid -> Int -> Int -> SeatsGrid
unsetSeatState seatsGrid i j = case getLeaf seatsGrid i of
                                 Nothing -> seatsGrid
                                 Just line -> case unsetLeaf line j of
                                                Null -> unsetLeaf seatsGrid i
                                                t -> setLeaf seatsGrid i t

findNeighbors :: SeatsGrid -> [Seat] -> Int -> Int -> [Seat]
findNeighbors _ [] _ _ = []
findNeighbors seatsGrid ((i,j,_):seats) m n = (i,j,check i j (-1,-1) ++ check i j (-1,0) ++ check i j (-1,1) ++
                                                   check i j ( 0,-1) ++                     check i j ( 0,1) ++
                                                   check i j ( 1,-1) ++ check i j ( 1,0) ++ check i j ( 1,1)
                                              ): findNeighbors seatsGrid seats m n
  where check :: Int -> Int -> (Int,Int) -> [(Int,Int)]
        check x0 y0 dir@(u,v)
          | x < 0 || y < 0 || x >= m || y >= n = []
          | seatState == Nothing = check x y dir
          | otherwise = [(x,y)]
          where x = x0 + u
                y = y0 + v
                seatState = getSeatState seatsGrid x y

newOccupied :: SeatsGrid -> [Seat] -> ([Seat],[Seat]) -> ([Seat],[Seat])
newOccupied _ [] r = r
newOccupied seatsGrid ((x,y,ns):seats) (vac,occ)
  | count < 5 = newOccupied seatsGrid seats (vac,(x,y,cns):occ)
  | otherwise = newOccupied seatsGrid seats ((x,y,cns):vac,occ)
  where (cns, count) = cleanNeighbors ns [] 0
        cleanNeighbors [] vns c = (vns,c)
        cleanNeighbors ((i,j):ns) vns c
          | getSeatState seatsGrid i j == Just 0 = cleanNeighbors ns ((i,j):vns) $! c+1
          | otherwise = cleanNeighbors ns vns c

seatsToRemove :: SeatsGrid -> [Seat] -> ([Seat],[Seat]) -> ([Seat],[Seat])
seatsToRemove _ [] r = r
seatsToRemove seatsGrid ((x,y,ns):seats) (vac,rm)
  | toRemove ns = seatsToRemove seatsGrid seats (vac,(x,y,ns):rm)
  | otherwise = seatsToRemove seatsGrid seats ((x,y,ns):vac,rm)
  where toRemove [] = False
        toRemove ((i,j):ns)
          | getSeatState seatsGrid i j == Just 1 = True
          | otherwise = toRemove ns

sit :: SeatsGrid -> [Seat] -> SeatsGrid
sit seatsGrid [] = seatsGrid
sit seatsGrid ((x,y,_):seats) = (sit $! setSeatState seatsGrid x y 1) seats

rmSeats :: SeatsGrid -> [Seat] -> SeatsGrid
rmSeats seatsGrid [] = seatsGrid
rmSeats seatsGrid ((x,y,_):seats) = (rmSeats $! unsetSeatState seatsGrid x y) seats

occupySeats :: SeatsGrid -> [Seat] -> Int -> IO Int
occupySeats _ [] occupiedCount = return occupiedCount
occupySeats seatsGrid vacantSeats occupiedCount = do
  let (vac,newOcc) = newOccupied seatsGrid vacantSeats ([],[])
      seatsGrid1 = sit seatsGrid newOcc
      (yetVac,remove) = seatsToRemove seatsGrid1 vac ([],[])
      seatsGrid2 = rmSeats seatsGrid1 remove
  occupySeats seatsGrid2 yetVac $! occupiedCount + length newOcc

aoc11b :: IO Int
aoc11b = do
           ls <- getLines
           let m = length ls
               n = length (head ls)
               aux =  zipWith (\cs i -> convertSeatsB cs Null i 0) ls [0..]
               (tries,vseatss) = unzip aux
               seatsGrid = foldl (\t (i,v) -> setLeaf t i v) Null (zip [0..] tries)
               vacantSeats = (findNeighbors seatsGrid (concat vseatss) $! m) $! n
           occupySeats seatsGrid vacantSeats 0

-- 12a - Figure out where the navigation instructions lead. What is the Manhattan distance
--       between that location and the ship's starting position?
moveN :: Int -> (Int,Int,Int) -> (Int,Int,Int)
moveN val (x,y,d) = (x,y+val,d)

moveS :: Int -> (Int,Int,Int) -> (Int,Int,Int)
moveS val (x,y,d) = (x,y-val,d)

moveE :: Int -> (Int,Int,Int) -> (Int,Int,Int)
moveE val (x,y,d) = (x+val,y,d)

moveW :: Int -> (Int,Int,Int) -> (Int,Int,Int)
moveW val (x,y,d) = (x-val,y,d)

turnL :: Int -> (Int,Int,Int) -> (Int,Int,Int)
turnL val (x,y,d) = (x,y,mod (d-val) 4)

turnR :: Int -> (Int,Int,Int) -> (Int,Int,Int)
turnR val (x,y,d) = (x,y,mod (d+val) 4)

moveF :: Int -> (Int,Int,Int) -> (Int,Int,Int)
moveF val (x,y,0) = moveE val (x,y,0)
moveF val (x,y,1) = moveS val (x,y,1)
moveF val (x,y,2) = moveW val (x,y,2)
moveF val (x,y,3) = moveN val (x,y,3)

navigation :: [String] -> [(Int,Int,Int)->(Int,Int,Int)]
navigation [] = []
navigation (l:ls) = let c = head l
                        val = read $ tail l
                    in case c of
                         'N' -> (moveN val):navigation ls
                         'S' -> (moveS val):navigation ls
                         'E' -> (moveE val):navigation ls
                         'W' -> (moveW val):navigation ls
                         'F' -> (moveF val):navigation ls
                         'L' -> (turnL (div val 90)):navigation ls
                         'R' -> (turnR (div val 90)):navigation ls

manhattanDistance :: (Int,Int,Int) -> Int
manhattanDistance (x,y,_) = abs x + abs y

aoc12a :: IO Int
aoc12a = do
           ls <- getLines
           let instrs = navigation ls
               final = foldl (flip ($)) (0,0,0) instrs
           return $ manhattanDistance final

-- 12b - Figure out where the navigation instructions actually lead.
--       What is the Manhattan distance between that location and the ship's starting position?
moveWayNS :: Int -> (Int,Int,Int,Int) -> (Int,Int,Int,Int)
moveWayNS val (x,y,wx,wy) = (x,y,wx,wy+val)

moveWayEW :: Int -> (Int,Int,Int,Int) -> (Int,Int,Int,Int)
moveWayEW val (x,y,wx,wy) = (x,y,wx+val,wy)

turnWay :: Int -> (Int,Int,Int,Int) -> (Int,Int,Int,Int)
turnWay   0 (x,y,wx,wy) = (x,y,wx,wy)
turnWay  90 (x,y,wx,wy) = (x,y,wy,-wx)
turnWay 180 (x,y,wx,wy) = (x,y,-wx,-wy)
turnWay 270 (x,y,wx,wy) = (x,y,-wy,wx)

moveShip :: Int -> (Int,Int,Int,Int) -> (Int,Int,Int,Int)
moveShip val (x,y,wx,wy) = (x+val*wx,y+val*wy,wx,wy)

navigationB :: [String] -> [(Int,Int,Int,Int)->(Int,Int,Int,Int)]
navigationB [] = []
navigationB (l:ls) = let c = head l
                         val = read $ tail l
                         f = case c of
                               'N' -> moveWayNS val
                               'S' -> moveWayNS (-val)
                               'E' -> moveWayEW val
                               'W' -> moveWayEW (-val)
                               'R' -> turnWay val
                               'L' -> turnWay (360-val)
                               'F' -> moveShip val
                     in f:navigationB ls

manhattanDistanceB :: (Int,Int,Int,Int) -> Int
manhattanDistanceB (x,y,_,_) = abs x + abs y

aoc12b :: IO Int
aoc12b = do
           ls <- getLines
           let instrs = navigationB ls
               final = foldl (flip ($)) (0,0,10,1) instrs
           return $ manhattanDistanceB final

-- 13a - What is the ID of the earliest bus you can take to the airport multiplied by
--       the number of minutes you'll need to wait for that bus?
cleanIDs :: String -> [Int]
cleanIDs "" = []
cleanIDs l = let (h,t) = span isDigit l
                 id = read h
             in id:cleanIDs (dropWhile (not.isDigit) t)

departTime :: Int -> Int -> Int
departTime t0 id = ceiling ((toEnum t0)/(toEnum id)) * id

aoc13a :: IO Int
aoc13a = do
           t <- getLine
           b <- getLine
           let t0 = read t
               ids = cleanIDs b
               ts = map (departTime t0) ids
               tids = zip ts ids
               (t1,id) = minimum tids
           return $ id*(t1-t0)

-- 13b - What is the earliest timestamp such that all of the listed bus IDs
--       depart at offsets matching their positions in the list?
idsOffsets :: String -> Integer -> [(Integer,Integer)]
idsOffsets "" _ = []
idsOffsets l i = let (h,t) = span (/=',') l
                   in if h == "x"
                      then idsOffsets (tail t) (i+1)
                      else let n = read h in (mod (-i) n, n):idsOffsets (drop 1 t) (i+1)

extendedEuclid :: Integer -> Integer -> (Integer,Integer)
extendedEuclid x y = ext x y 1 0 0 1
  where ext oldR 0 oldS _ oldT _ = (oldS,oldT)
        ext oldR r oldS s oldT t =
          let q = div oldR r
          in ext r (oldR - q*r) s (oldS - q*s) t (oldT - q*t)

chineseRemainder :: [(Integer,Integer)] -> Integer
chineseRemainder [(m0,p0),(m1,p1)] =
  let (c0,c1) = extendedEuclid p0 p1
      p2 = p0*p1
      m = m1*c0*p0 + m0*c1*p1
  in mod m p2
chineseRemainder ((m0,p0):(m1,p1):idOffs) =
  let (c0,c1) = extendedEuclid p0 p1
      p2 = p0*p1
      m = m1*c0*p0 + m0*c1*p1
      m2 = mod m p2
  in chineseRemainder ((m2,p2):idOffs)

aoc13b :: IO Integer
aoc13b = do
           b <- getLine
           let idOffs = idsOffsets b 0
           return $ chineseRemainder idOffs

-- 14a - Execute the initialization program.
--       What is the sum of all values left in memory after it completes?
type Mask = [Int]->[Int]
type MemAdr = Int
type MemVal = Int
type Mem = [(MemAdr,MemVal)]
type State = (Mask, Mem)

setMask :: Mask -> State -> State
setMask mask (_,mem) = (mask,mem)

setMem :: MemAdr -> MemVal -> State -> State
setMem adr val0 (mask,mem) = let [val1] = mask [val0]
                              in (mask, write adr val1 mem)
  where write :: MemAdr -> MemVal -> Mem -> Mem
        write adr val [] = [(adr,val)]
        write adr val ((i,v):mem)
          | adr == i = (i,val): mem
          | otherwise = (i,v): write adr val mem

newMask :: String -> Int -> Mask
newMask [] _ = id
newMask (c:cs) i = case c of
                     'X' -> newMask cs (i-1)
                     '1' -> (\[x] -> [x `setBit` i]) . newMask cs (i-1)
                     '0' -> (\[x] -> [x `clearBit` i]) . newMask cs (i-1)

parseProgram :: [String] -> Int -> [State->State]
parseProgram [] _ = []
parseProgram (l:ls) v = case l of
    'm':'a':'s':'k':' ':'=':' ':cs -> let mask = (newMaskVersion v) cs 35
                                      in (setMask mask): parseProgram ls v
    'm':'e':'m':'[':cs -> let (adr,c1s) = span isDigit cs
                              val = dropWhile (not.isDigit) c1s
                          in ((setMemVersion v) (read adr) (read val)): parseProgram ls v

runProgram :: [State->State] -> State -> State
runProgram comands state0 = foldl (flip ($)) state0 comands

sumMem :: State -> Int
sumMem (_,mem) = sum $ snd $ unzip mem

aoc14a :: IO Int
aoc14a = do
           ls <- getLines
           let p = parseProgram ls 1
               state0 = (id,[])
               stateF = runProgram p state0
           return $ sumMem stateF

-- 14b - Execute the initialization program using an emulator for a version 2 decoder chip.
--       What is the sum of all values left in memory after it completes?
newMaskVersion :: Int -> String -> Int -> Mask
newMaskVersion 1 = newMask
newMaskVersion 2 = newMaskB

newMaskB :: String -> Int -> Mask
newMaskB [] _ = id
newMaskB (c:cs) i = case c of
                      '0' -> newMaskB cs (i-1)
                      '1' -> newMaskB cs (i-1) . (map (`setBit` i))
                      'X' -> newMaskB cs (i-1) . (`forkBit` i)
  where forkBit :: [Int] -> Int -> [Int]
        forkBit [] _ = []
        forkBit (x:xs) i = clearBit x i : setBit x i : forkBit xs i

setMemVersion :: Int -> MemAdr -> MemVal -> State -> State
setMemVersion 1 = setMem
setMemVersion 2 = setMemB

setMemB :: MemAdr -> MemVal -> State -> State
setMemB adr0 val (mask,mem) = let adrs = mask [adr0]
                              in (mask, write adrs val mem)
  where write :: [MemAdr] -> MemVal -> Mem -> Mem
        write [] _ mem = mem
        write adrs val [] = zip adrs (repeat val)
        write (a:adrs) val mem@((i,v):ms)
          | a < i = (a,val): write adrs val mem
          | a == i = (i,val): write adrs val ms
          | a > i = (i,v): write (a:adrs) val ms

aoc14b :: IO Int
aoc14b = do
           ls <- getLines
           let p = parseProgram ls 2
               state0 = (id,[])
               stateF = runProgram p state0
           return $ sumMem stateF

-- 15a - Given your starting numbers, what will be the 2020th number spoken?
seq15a :: [Int] -> [Int]
seq15a start = start ++ seq (reverse start)
  where seq (x:xs) = let r = repeatingDistance 1 x xs in  r: seq (r:x:xs)
        repeatingDistance _ _ [] = 0
        repeatingDistance i x (y:ys)
          | y == x = i
          | otherwise = repeatingDistance (i+1) x ys


aoc15a :: IO Int
aoc15a = do
           l <- getLine
           let start = read ('[':l++"]") :: [Int]
           return (seq15a start !! (2020-1))

-- 15b - Given your starting numbers, what will be the 30000000th number spoken?
data Trie a = Null | Leaf !a | Node (Trie a) (Trie a) (Trie a) (Trie a)
                                    (Trie a) (Trie a) (Trie a) (Trie a)
                                    (Trie a) (Trie a) (Trie a) (Trie a)
                                    (Trie a) (Trie a) (Trie a) (Trie a)
  deriving Show

setLeaf :: Trie a -> Int -> a -> Trie a
setLeaf (Leaf _) 0 value = Leaf value
setLeaf (Leaf v) index value = (setLeaf $!(setLeaf Null index value)) 0 v
setLeaf Null 0 value = Leaf value
setLeaf trie index0 value = let index1 = shiftR index0 4
                                key = index0 .&. 15
                                Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf = case trie of
                                  Null -> Node Null Null Null Null Null Null Null Null
                                               Null Null Null Null Null Null Null Null
                                  _ -> trie
                            in case key of
                                 00 -> (Node $!((setLeaf t0 $! index1) value)) t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf
                                 01 -> (Node t0 $!((setLeaf t1 $! index1) value)) t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf
                                 02 -> (Node t0 t1 $!((setLeaf t2 $! index1) value)) t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf
                                 03 -> (Node t0 t1 t2 $!((setLeaf t3 $! index1) value)) t4 t5 t6 t7 t8 t9 ta tb tc td te tf
                                 04 -> (Node t0 t1 t2 t3 $!((setLeaf t4 $! index1) value)) t5 t6 t7 t8 t9 ta tb tc td te tf
                                 05 -> (Node t0 t1 t2 t3 t4 $!((setLeaf t5 $! index1) value)) t6 t7 t8 t9 ta tb tc td te tf
                                 06 -> (Node t0 t1 t2 t3 t4 t5 $!((setLeaf t6 $! index1) value)) t7 t8 t9 ta tb tc td te tf
                                 07 -> (Node t0 t1 t2 t3 t4 t5 t6 $!((setLeaf t7 $! index1) value)) t8 t9 ta tb tc td te tf
                                 08 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 $!((setLeaf t8 $! index1) value)) t9 ta tb tc td te tf
                                 09 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 $!((setLeaf t9 $! index1) value)) ta tb tc td te tf
                                 10 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 $!((setLeaf ta $! index1) value)) tb tc td te tf
                                 11 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta $!((setLeaf tb $! index1) value)) tc td te tf
                                 12 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb $!((setLeaf tc $! index1) value)) td te tf
                                 13 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc $!((setLeaf td $! index1) value)) te tf
                                 14 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td $!((setLeaf te $! index1) value)) tf
                                 15 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te $!((setLeaf tf $! index1) value))

-- feito pro 11b
unsetLeaf :: Trie a -> Int -> Trie a
unsetLeaf (Leaf _) 0 = Null
unsetLeaf leaf@(Leaf _) index = leaf
unsetLeaf Null _ = Null
unsetLeaf (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf) index0
  | reduceNode node = n0
  | otherwise = node
  where index1 = shiftR index0 4
        key = index0 .&. 15
        node@(Node n0 n1 n2 n3 n4 n5 n6 n7 n8 n9 na nb nc nd ne nf) =
          case key of
            00 -> (Node $! unsetLeaf t0 $! index1) t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf
            01 -> (Node t0 $! unsetLeaf t1 $! index1) t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf
            02 -> (Node t0 t1 $! unsetLeaf t2 $! index1) t3 t4 t5 t6 t7 t8 t9 ta tb tc td te tf
            03 -> (Node t0 t1 t2 $! unsetLeaf t3 $! index1) t4 t5 t6 t7 t8 t9 ta tb tc td te tf
            04 -> (Node t0 t1 t2 t3 $! unsetLeaf t4 $! index1) t5 t6 t7 t8 t9 ta tb tc td te tf
            05 -> (Node t0 t1 t2 t3 t4 $! unsetLeaf t5 $! index1) t6 t7 t8 t9 ta tb tc td te tf
            06 -> (Node t0 t1 t2 t3 t4 t5 $! unsetLeaf t6 $! index1) t7 t8 t9 ta tb tc td te tf
            07 -> (Node t0 t1 t2 t3 t4 t5 t6 $! unsetLeaf t7 $! index1) t8 t9 ta tb tc td te tf
            08 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 $! unsetLeaf t8 $! index1) t9 ta tb tc td te tf
            09 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 $! unsetLeaf t9 $! index1) ta tb tc td te tf
            10 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 $! unsetLeaf ta $! index1) tb tc td te tf
            11 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta $! unsetLeaf tb $! index1) tc td te tf
            12 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb $! unsetLeaf tc $! index1) td te tf
            13 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc $! unsetLeaf td $! index1) te tf
            14 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td $! unsetLeaf te $! index1) tf
            15 -> (Node t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 ta tb tc td te $! unsetLeaf tf $! index1)
        reduceNode (Node Null Null Null Null Null Null Null Null Null Null Null Null Null Null Null Null) = True
        reduceNode (Node (Leaf _) Null Null Null Null Null Null Null Null Null Null Null Null Null Null Null) = True
        reduceNode _ = False

getLeaf :: Trie a -> Int -> Maybe a
getLeaf Null _ = Nothing
getLeaf (Leaf v) 0 = Just v
getLeaf (Leaf _) _ = Nothing
getLeaf (Node t0 t1 t2 t3 t4 t5 t6 t7
              t8 t9 ta tb tc td te tf) index0 =
                        let index1 = shiftR index0 4
                            key = index0 .&. 15
                        in case key of
                             00 -> getLeaf t0 $! index1
                             01 -> getLeaf t1 $! index1
                             02 -> getLeaf t2 $! index1
                             03 -> getLeaf t3 $! index1
                             04 -> getLeaf t4 $! index1
                             05 -> getLeaf t5 $! index1
                             06 -> getLeaf t6 $! index1
                             07 -> getLeaf t7 $! index1
                             08 -> getLeaf t8 $! index1
                             09 -> getLeaf t9 $! index1
                             10 -> getLeaf ta $! index1
                             11 -> getLeaf tb $! index1
                             12 -> getLeaf tc $! index1
                             13 -> getLeaf td $! index1
                             14 -> getLeaf te $! index1
                             15 -> getLeaf tf $! index1

seq15b :: [Int] -> [Int]
seq15b start = let l = length start
                   (star,[t]) = splitAt (l-1) start
               in start ++ sequence (listToTrie star) t l
  where listToTrie :: [Int] -> Trie Int
        listToTrie xs = foldl (\t (x,i) -> (setLeaf $! t) x i) Null (zip xs [1..])

        sequence :: Trie Int -> Int -> Int -> [Int]
        sequence trie x i = let d = case getLeaf trie x of
                                      Nothing -> 0
                                      Just v -> i-v
                            in  d:(((sequence $!(setLeaf trie x i)) $! d) $! (i+1))

aoc15b :: IO Int
aoc15b = do
           l <- getLine
           let start = read ('[':l++"]")
           return (seq15b start !! (30000000-1))

-- 16a - Adding together all of the invalid values produces your ticket scanning error rate.
--       Consider the validity of the nearby tickets you scanned. What is your ticket scanning error rate?
type FieldRule = (Bool,Int,Int,Int,Int)

parseTicketRules :: ([FieldRule],[String]) -> ([FieldRule],[String])
parseTicketRules (ts,"":ls) = (ts,ls)
parseTicketRules (ts,l0:ls) = let (field,l1) = span (not.isDigit) l0
                                  departure = isPrefixOf "departure" field
                                  (r0,l2) = span isDigit l1 
                                  (r1,l3) = span isDigit $ tail l2
                                  (r2,l4) = span isDigit $ dropWhile (not.isDigit) l3
                                  r3 = tail l4
                                  t = (((((,,,,) $! departure) $! read r0) $! read r1) $! read r2) $! read r3
                              in parseTicketRules (t:ts,ls)

checkTicketRule :: Int -> FieldRule -> Bool
checkTicketRule v (_,r0,r1,r2,r3) = (r0 <= v && v <= r1) || (r2 <= v && v <= r3)

aoc16a :: IO Int
aoc16a = do
           ls <- getLines
           let (rules,_:_:_:_:t0:tickets) = parseTicketRules ([],ls)
               values = read $ '[':t0 ++ concatMap (',':) tickets ++ "]" :: [Int]
           return $ sum $ filter (\v -> not $ foldr ((||).(checkTicketRule v)) False rules) values

-- 16b - Now that you've identified which tickets contain invalid values, discard those tickets entirely.
--       Use the remaining valid tickets to determine which field is which.
--       Once you work out which field is which, look for the six fields on your ticket that start with the word departure.
--       What do you get if you multiply those six values together?
type Ticket = [Int]

readTicket :: String -> Ticket
readTicket t = read $ '[':t ++ "]"

validTickets :: [Ticket] -> [FieldRule] -> [Ticket] -> [Ticket]
validTickets [] _ vts = vts
validTickets (t:tickets) rules vts
  | valid = validTickets tickets rules (t:vts)
  | otherwise = validTickets tickets rules vts
  where valid = foldr (\v b -> foldr ((||).(checkTicketRule v)) False rules && b) True t

findRulesByField :: [FieldRule] -> [Ticket] -> [[FieldRule]] -> [[FieldRule]]
findRulesByField _ ([]:tickets) rlss = reverse rlss
findRulesByField rules tickets rlss = findRulesByField rules tails (rls:rlss)
  where values = map head tickets
        tails = map tail tickets
        findRules :: [FieldRule] -> [FieldRule] -> [FieldRule]
        findRules [] rls = rls
        findRules (r:rs) rls = if foldr ((&&).(`checkTicketRule` r)) True values
                                  then findRules rs (r:rls)
                                  else findRules rs rls
        rls = findRules rules []

cleanRulesByField :: [(Int,[FieldRule])] -> [(Int,FieldRule)] -> [(Int,FieldRule)] 
cleanRulesByField [] crbf = crbf
cleanRulesByField ((i,[r]):orbf) crbf = cleanRulesByField newORBF ((i,r):crbf)
  where newORBF = map (\(i,xs) -> (i,filter (/=r) xs)) orbf


aoc16b :: IO Int
aoc16b = do
           ls <- getLines
           let (rules,_:myT:_:_:aux) = parseTicketRules ([],ls)
               myTicket = readTicket myT
               tickets = validTickets (map readTicket aux) rules []
               rulesByField = findRulesByField rules tickets []
               (_,orbf) = unzip $ quickSort $ zip (map length rulesByField) $ zip [0..] rulesByField
               (_,orderedRules) = unzip $ quickSort $ cleanRulesByField orbf []
           return $ product $ zipWith (\(b,_,_,_,_) v -> if b then v else 1) orderedRules myTicket

-- 17a - Starting with your given initial configuration, simulate six cycles.
--       How many cubes are left in the active state after the sixth cycle?
type CubesGrid = Trie (Trie (Trie Int))



aoc17a :: IO Int
aoc17a = do
           ls <- getLines
           return 0

-- 17b - 

-- Main para testes
main = do
         putStrLn "Escolha o problema. Ex: 03b"
         opt <- getLine
         putStrLn "Insira a entrada para o problema."
         case opt of
           "01a" -> do r <- aoc01a; print r
           "01b" -> do r <- aoc01b; print r
           "02a" -> do r <- aoc02a; print r
           "02b" -> do r <- aoc02b; print r
           "03a" -> do r <- aoc03a; print r
           "03b" -> do r <- aoc03b; print r
           "04a" -> do r <- aoc04a; print r
           "04b" -> do r <- aoc04b; print r
           "05a" -> do r <- aoc05a; print r
           "05b" -> do r <- aoc05b; print r
           "06a" -> do r <- aoc06a; print r
           "06b" -> do r <- aoc06b; print r
           "07a" -> do r <- aoc07a; print r
           "07b" -> do r <- aoc07b; print r
           "08a" -> do r <- aoc08a; print r
           "08b" -> do r <- aoc08b; print r
           "09a" -> do r <- aoc09a; print r
           "09b" -> do r <- aoc09b; print r
           "10a" -> do r <- aoc10a; print r
           "10b" -> do r <- aoc10b; print r
           "11a" -> do r <- aoc11a; print r
           "11b" -> do r <- aoc11b; print r
           "12a" -> do r <- aoc12a; print r
           "12b" -> do r <- aoc12b; print r
           "13a" -> do r <- aoc13a; print r
           "13b" -> do r <- aoc13b; print r
           "14a" -> do r <- aoc14a; print r
           "14b" -> do r <- aoc14b; print r
           "15a" -> do r <- aoc15a; print r
           "15b" -> do r <- aoc15b; print r
           "16a" -> do r <- aoc16a; print r
           "16b" -> do r <- aoc16b; print r
           "17a" -> do r <- aoc17a; print r
--           "17b" -> do r <- aoc17b; print r
--           "18a" -> do r <- aoc18a; print r
--           "18b" -> do r <- aoc18b; print r
--           "19a" -> do r <- aoc19a; print r
--           "19b" -> do r <- aoc19b; print r
--           "20a" -> do r <- aoc20a; print r
--           "20b" -> do r <- aoc20b; print r
--           "21a" -> do r <- aoc21a; print r
--           "21b" -> do r <- aoc21b; print r
--           "22a" -> do r <- aoc22a; print r
--           "22b" -> do r <- aoc22b; print r
--           "23a" -> do r <- aoc23a; print r
--           "23b" -> do r <- aoc23b; print r
--           "24a" -> do r <- aoc24a; print r
--           "24b" -> do r <- aoc24b; print r
--           "25a" -> do r <- aoc25a; print r
--           "25b" -> do r <- aoc25b; print r
