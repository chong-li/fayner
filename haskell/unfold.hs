import Data.List

data Tree a = Leaf | Node (Tree a) a (Tree a) deriving Show

f :: Integral a => a -> Maybe (a, a)
f x = if x <= 5
      then Just (x, x+1)
      else Nothing

myUnfoldr :: (b -> Maybe (a, b)) -> b -> [a]
myUnfoldr f x = case f x of
  Nothing      -> []
  Just (y, x') -> y : myUnfoldr f x'

maybeSpan1 :: [a] -> Maybe (a, [a])
maybeSpan1 []     = Nothing
maybeSpan1 (x:xs) = Just (x, xs)

xs = unfoldr f 0
myXs = myUnfoldr f 0

testList = [3,6,2,8,4]
ys = unfoldr maybeSpan1 testList

myIterate f = myUnfoldr (\x -> Just (x, f x))
zs = iterate (+1) 0
myZs = myIterate (+1) 0

while :: (a -> Bool) -> (a -> a) -> a -> a
while condition exec = head . dropWhile condition . iterate exec

traceExec :: (a -> Bool) -> (a -> a) -> a -> [a]
traceExec condition exec = takeWhile condition . iterate exec

traceExec' :: (a -> Bool) -> (a -> a) -> a -> [a]
traceExec' condition exec = unfoldr buildFunc
  where
    buildFunc val = if condition val
                    then Just (val, exec val)
                    else Nothing


unfoldrTree :: (a -> Maybe (a, b, a)) -> a -> Tree b
unfoldrTree f n = case f n of
  Nothing        -> Leaf
  Just (l, v, r) -> Node (unfoldrTree f l) v (unfoldrTree f r)


pivot :: Ord a => [a] -> Maybe ([a], a, [a])
pivot [] = Nothing
pivot (x:xs) = Just (l, x, r)
  where
    (l, r) = partition (<= x) xs

ts = unfoldrTree pivot testList