def add_param_in_arg(p): # arg aqui representa a p
    
    def r(*args): # a new_arg representa o que?
        return p(p, *args)
    
    return r

def take_param(f, *args):
    return f(f(*args))

def combinator(fact_form):

    def fact_partial(p, *args):
        r = add_param_in_arg(p)
        return fact_form(r, *args)
  
    return fact_partial

def Z(func):
  
    func = combinator(func)

    def final(*args):
        return func(func, *args)

    return final

# -----

def Y (func):

    def partial(p, *pargs):

        def new_r(*rargs):
            return p(p, *rargs)

        return func(new_r, *pargs)

    def partial_applied_to_partial(*args):
        return partial(partial, *args)

    return partial_applied_to_partial

def make_partial_version (func):
  
    def partial(p,n):

        def new_r(m):
            return p(p, m)

        return func(new_r, n)
  
    def partial(p,n):
        r = (lambda m: p(p,m))
        return func(r, n)
    return partial

# def p(f: function, n: int)-> int:
#   pass

# def r(n: int)-> int:
#   pass



def fact_partial (p, n):
    if n == 0:
        return 1
    else:
        return n*p(p, n-1)

print(fact_partial(fact_partial, 5))


# ------

# Queremos transformar a fact_form em fact_partial
@Z
def recursor(r, condition, base, rec_step):
  def recursion(*args):
    if condition(*args):
      return base
    else:
      return rec_step(r, *args)
  
  return recursion

def condition(n):
  return (n == 0)

base = 1


def rec_step(r, n):
  return n*r(n-1)

a = recursor(condition, base, rec_step) (5)
print(f'Com recursor: {a}')

@combinator
def fact_form (r, n):
  if n == 0:
    return 1
  else:
    return n * (r(n-1))

print('fact_form(5):', fact_form(fact_form,5))

@Z
def fact (r, n):
  if n == 0:
    return 1
  else:
    return n * (r(n-1))

print('fact(5):', fact(5))

# ------


    
#     def partial_applied_to_partial(n)
#       return partial(partial, n)

#     return partial_applied_to_partial

# def fact_partial (p, n):
#   if n == 0:
#       return 1
#   else:
#     return n*p(p, n-1)

# @Y

# def fact (r, n):
#   if n == 0:
#     return 1
#   else:
#     return n * r(n-1)

# print(fact_form(5))