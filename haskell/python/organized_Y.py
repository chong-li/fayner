from typing import Callable, Any, NewType

# --- Types ---

# Definining new types only to try to make it clearer

# Let's say we want to define, in the end, a function with domain A and codomain B

A = int
B = int
NormalMap = NewType('NormalMap', Callable[[A],B])

# Ideally, SelfApplicable would mean Callable[[SelfApplicable, ...], B]
# We can't be recursive here, so we put "Callable" as the 2nd "SelfApplicable".
# Also, the "..." sould work (meaning "any number of parameters"), but it does't. And it seems to work without it.

SelfApplicable = NewType('SelfApplicable', Callable[[Callable], B])

# --- Functions ---

def self_apply_and_wait(p: SelfApplicable) -> NormalMap: 
  # This is not p(p). This is \x -> p(p,x).
  # The "wait" means "it is a map, wating for x".

  def r(*args: A) -> B:
    return p(p, *args)
    
  return r

def partial_version(func_form: Callable[[NormalMap],B]) -> Callable[[SelfApplicable],B]:

  def func_partial(p: SelfApplicable, *args: A) -> B:
    r = self_apply_and_wait(p)
    return func_form(r, *args)
  
  return func_partial

def Y(func:Callable[[NormalMap],B]) -> NormalMap:
  partial = partial_version(func)
  final = self_apply_and_wait(partial)
  return final

# --- Original versions of factorial, for comparison ---

def fact_usual (n: int) -> int:
  if n == 0:
    return 1
  else:
    return n * fact_usual(n-1)

def fact_form (r: NormalMap, n: int) -> int:
  if n == 0:
    return 1
  else:
    return n * r(n-1)

def fact_partial (p: SelfApplicable, n: int) -> int:
  if n == 0:
    return 1
  else:
    return n * p(p,n-1)

# --- Testing ---

@partial_version
def fact_form (r: NormalMap, n: int) -> int:
  if n == 0:
    return 1
  else:
    return n * (r(n-1))

n = 5
print(f"fact_partial(fact_partial, {n}) = {fact_form(fact_form, n)}\n")

@Y
def fact_form (r: NormalMap, n: int) -> int:
  if n == 0:
    return 1
  else:
    return n * (r(n-1))

n = 5
print(f"fact({n}) = {fact_form(n)}\n")