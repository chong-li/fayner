#####################################
##  Com comentários do Chong       ##
##                                 ##
##  novas funções contém a string  ##
##            _alt_                ##
##                                 ##
## trechos com alteração contém    ##
## headers fáceis de ver           ##
#####################################

# As versões que não são sobre frações dão erro de arredondamento. Por acaso, eu tinha escrito as versões com frações junto.

# A função alpha é aque leva (0,1) em (1/2, 1), que o Arthur chamou de h.
# alpha tem dominio (0,1) e imagem em (1/2, 1)
def alpha(x):
    return 1/(2-x)


# alpha frac tem dominio { (p,q) | p e q sao naturais e p < q}, isto é, "frações" em (0, 1)
def alpha_frac(p,q):
    return q, 2*q - p

############ Chong section begin ######################

## Quando 2 funções fazem a mesma coisa com entradas diferentes, vale a pena pensar
## se é possível fazer com uma função só
## usar um só if pra selecionar isso logo de cara não é má ideia
def alpha_alt_1(p, q=None):
    if q is None:
        return 1/(2-p)
    else:
        return q, 2*q - p

## mas também tem outras maneiras
def alpha_alt_2(p, q=None):
    is_rational = (q is not None)
    q = q if is_rational else 1

    p, q = q, 2*q - p

    return (p, q) if is_rational else p/q

# A grande vantagem dessa última é que a conta está escrita em um lugar só
# Se precisar mudar, muda só nela. Se precisar debugar, não vai ser inconsistente

############ Chong section end ######################


# A função beta é a que leva (0,1) em (0, 1/2), que o Arthur chamou de 1-h, porque é 1-h mesmo.
# beta tem dominio (0,1) e imagem em (0,1/2), e é igual a 1 - alpha
def beta(x):
    return (x-1)/(x-2)

# beta frac tem dominio { (p,q) | p e q sao naturais e p < q}, isto é, "frações" em (0, 1)
def beta_frac(p,q):
    return q-p,2*q-p

############ Chong section begin ######################

# Se beta é função de alpha, usa alpha!
def beta_alt_1(p, q=None):
    if q is None:
        return 1-alpha(p)
    else:
        p, q = alpha(p, q)
        return q-p, q

def beta_alt_2(p, q=None):
    is_rational = (q is not None)
    q = q if is_rational else 1

    p, q = alpha_alt_2(p, q)
    p, q = q-p, q
    return (p, q) if is_rational else p/q

## Note o padrão! beta_alt_2 é quase idêntica à alpha_alt_2
## Onde tem um padrão tem uma automação

def closure_for_alt_2_funcs(f):
    def func(p, q=None):
        is_rational = (q is not None)
        q = q if is_rational else 1

        p, q = f(p, q)
        return (p, q) if is_rational else p/q

    return func

## Reproduz o padrão de criar funções que servem pra ambos os casos como uma função
## Não sei se você já tinha se deparado com isso antes, chama-se closure em inglês
## Em português o Breno me disse que chama fechamento

############ Chong section end ######################

# Nada demais, essas são as inversas de alpha e beta:
def alpha_inv(x):
    return 2 - 1/x

def alpha_frac_inv(p,q):
    return 2*p-q, p

def beta_inv(x):
    return 2 - 1/(1-x)

def beta_frac_inv(p,q):
    return q-2*p, q-p

############ Chong section begin ######################
alpha_inv_alt_2 = closure_for_alt_2_funcs(alpha_frac_inv)

beta_inv_alt_2 = closure_for_alt_2_funcs(beta_frac_inv)

############ Chong section end ######################


# Da pra combinar essas duas inversas, que têm dominios (1/2, 1) e (0, 1/2), respectivamente, em uma função só, que eu estou chamando de gamma, com domínio (0,1):
## Chong's comment: g de gamma para g de Arthur... nice!!
def gamma(x):
    if x> 0.5:
        return alpha_inv(x)
    else:
        return beta_inv(x)

def gamma_frac(p,q):
    if 2*p > q:
        return alpha_frac_inv(p,q)
    else:
        return beta_frac_inv(p,q)

############ Chong section begin ######################

## Já sabe o esquema!
def gamma_frac_alt_2(p, q):
    if 2*p > q:
        return alpha_inv_alt_2(p,q)
    else:
        return beta_inv_alt_2(p,q)

gamma_alt_2 = closure_for_alt_2_funcs(gamma_frac_alt_2)

############ Chong section end ######################



# Agora eu vou definir a função g de Arthur, usando listas de 0's e 1's:
# g recebe uma lista de 0's e 1's
def g(l):
    if l == []:
        return 0.5
    else:
        bit = l.pop()
        if bit == 0:
            return alpha(g(l))
        if bit == 1:
            return beta(g(l))

def g_frac(l):
    if l == []:
        return 1,2
    else:
        bit = l.pop()
        x,y = g_frac(l)
        if bit == 0:
            a,b = alpha_frac(x,y)
        if bit == 1:
            a,b = beta_frac(x,y)
        # Achtung! Pedindo pra ter um erro de a,b not defined
        # solução mais simples: esquece os x,y e usa tudo a, b
        # melhor ainda, retorne direto func_frac(x, y) que nem na g
        return a,b

############ Chong section begin ######################
# Dessa vez teve umas coisinhas que eu também quis comentar na função diretamente em vez de
# só reescrever pra mostrar outra forma de fazer. Olha lá

# Aqui tem outra coisa legal! A partir de python 3.6 tem um padrão que me é muito querido

def g_frac_alt_1(l):
    if l == []:
        return 1, 2
    else:
        # Você pode lidar com listas como head, tail. head vai ser um elemento da lista e
        # tail a lista sem o head
        # Só que isso é categoricamente diferente porque você está pegando
        # do começo da lista em vez do fim dela, deixa esse como curiosidade só
        bit, *l = l
        # Aqui dá pra deixar mais elegante retornando direto do if
        # Eu gosto de fazer esses retornos pra deixar claro que nada mais será feito
        a, b = g_frac_alt_2(l)
        if bit == 0:
            return alpha_frac(a, b)
        else: # if bit == 1:
            return beta_frac(a, b)

# g_alt_2 = closure_for_alt_2_funcs(g_frac_alt_2) não funciona mais porque mudou o input
# Aí, como não dá pra diferenciar pelo input, tem que colocar uma flag de parâmetro mesmo

def g_alt_2(l, is_rational=True):
    if l == []:
        return 1, 2 if is_rational else 1/2
    else:
        bit = l.pop()
        a, b = g_alt_2(l, is_rational)
        if bit == 0:
            return alpha_alt_2(a, b) if is_rational else alpha_alt_2(a/b)
        else: # if bit == 1:
            return beta_alt_2(a, b) if is_rational else beta_alt_2(a/b)


############ Chong section end ######################

# Agora sim vou definir a inversa da g, que usa a gamma que eu defini acima:
def g_inv(x):
    if x == 0.5:
        return []
    else:
        if x> 0.5:
            bit = 0
        if x<0.5:
            bit = 1
        l = g_inv(gamma(x))
        l.append(bit)
        return l

def g_frac_inv(p,q):
    if 2*p == q:
        return []
    else:
        if 2*p > q:
            bit = 0
        else:
            bit = 1
        a,b = gamma_frac(p,q)
        l = g_frac_inv(a,b)
        l.append(bit)
        return l

############ Chong section begin ######################

# Olha lá, não previ compostas acabei me ferrando
# Não consigo continuar usando o fechamento que defini antes
# Vou ver se penso numa forma mais geral e "composable" de fazer

def g_inv_alt_2(p, q=None):
    is_rational = (q is not None)
    q = 1 if q is None else q

    if 2*p == q:
        return []
    else:
        bit = 0 if 2*p > q else 1

    p, q = gamma_alt_2(p, q)
    l = g_inv_alt_2( *(p, q) if is_rational else p/q )
    l.append(bit)

    return l

## if __name__ == '__main__': Esse if só roda se o script é chamado diretamente
## se você quer testar outras coisas ou usar sem testar mesmo
## pode fazer import desse script e usar as funções sem ter que imprimir os teste
if __name__ == '__main__':

    # Dois testes, pra ver se está funcionando mesmo:
    p,q = 8,11
    print(g_frac_inv(p,q))
    print(g_inv_alt_2(p,q))

    l = [1,1,1,0]
    print(g_frac(l))
    l = [1,1,1,0]
    print(g_alt_2(l))
