
class Rational():
    """Classe Rational. Implementa funções para testar a ideia da `função g de Arthur` com inputs nos números Racionais (QQ)"""

    def __init__(self, numerator=0, denominator=1):
        self.p = numerator
        self.q = denominator

    def __str__(self):
        return f'{self.p}/{self.q}' # === '{}/{}'.format(p, q)

    def alpha(self):
        """alpha: (0,1) → (1/2, 1). A função que Arthur chamou de h
        É definida por p/q → q/(2q-p)

        Se Real, denominador do input é considerado 1 e valor de retorno é a divisão dos valores"""

        self.p, self.q = self.q, 2 * self.q - self.p
        return self

    def beta(self):
        """beta: (0,1) → (0, 1/2). A função que Arthur chamou de 1-h, porque é 1-h mesmo
        É definida por p/q → q-p/(2*q-p)

        Se Real, denominador do input é considerado 1 e valor de retorno é a divisão dos valores"""

        self.p, self.q = self.q - self.p, 2 * self.q - self.p
        return self

    def alpha_inv(self):
        """alpha inversa: (1/2, 1) → (0, 1)
        É definida por p/q → (2*p-q)/p

        Se Real, denominador do input é considerado 1 e valor de retorno é a divisão dos valores"""

        self.p, self.q = 2 * self.p - self.q, self.p
        return self

    def beta_inv(self):
        """beta inversa: (0, 1/2) → (0, 1)
        É definida por p/q → (q-2*p)/(q-p)

        Se Real, denominador do input é considerado 1 e valor de retorno é a divisão dos valores"""

        self.p, self.q = self.q - 2 * self.p, self.q - self.p
        return self

    def gamma(self):
        """Função auxiliar. gamma implementa a lógica `piecewise` necessária pra juntar alpha_inv e beta_inv em função com domínio em (0, 1) e imagem em (0, 1)"""

        if 2 * self.p > self.q:
            return self.alpha_inv()
        else:
            return self.beta_inv()

    def g(self, l):
        """função g de Arthur, usando listas de 0's e 1's
        g recebe uma lista de 0's e 1's. Como método de classe em python deveria se chamar `from_list`

        Parameters
        ----------
        self: Rational or Real
            no attribute values are relevant on input. They will be overwritten and self is returned
        l: list of ints

        Returns
        ----------
        self: Rational or Real
        """
        if l == []:
            self.p, self.q = 1, 2
            return self
        else:
            bit = l.pop()
            self.g(l)
            if bit == 0:
                return self.alpha()
            else: # bit == 1
                return self.beta()

    @classmethod
    def from_list(cls, l):
        # Gambiarra para que classes que herdam (como Real) não tenham que sobrecarregar (overload)
        # O jeito "pythonico" é reimplementar métodos como esse
        # Mas aqui, como 'cls' é a classe, quando for Rational vai ser Rational, Real vai ser Real
        # Gambiarra só funciona porque nesse caso é possível instanciar com parâmetros iguais
        obj = cls()
        return obj.g(l)

    @classmethod
    def from_string(cls, s):
        """Recebe uma string de 0s e 1s e retorna um Rational ou um Real. Outra implementação da função g de Arthur"""
        obj = cls()
        obj.p, obj.q = 1, 2

        # Sem usar recursão. Percorre a "lista" e segue atualizando o objeto em cada passo
        for bit in s:
            if bit == '0':
                obj.alpha()
            else:
                obj.beta()

        return obj

    # TODO: reimplement g_inv, without recursion and with a str_len param
    def to_string(self, str_len=2048):
        """Reimplementa a inversa de g e converte para string com tamanho máximo dado"""
        reversed_binary = []
        epsilon = 10**-14

        while (str_len>0) and abs(2 * self.p - self.q) > epsilon:
            str_len -= 1
            if 2 * self.p > self.q:
                reversed_binary.append('0')
                self.alpha_inv()
            else:
                reversed_binary.append('1')
                self.beta_inv()

        return ''.join(reversed(reversed_binary))


    def g_inv(self):
        """Inversa da g de Arthur.

        Paramenters
        -----------
        epsilon: float
            Precisão com a qual se permite descrever self em bits"""

        epsilon = 10**-14
        if abs(2 * self.p - self.q) < epsilon:
            return []
        else:
            if 2 * self.p > self.q:
                bit = 0
            else:
                bit = 1
            self.gamma()
            l = self.g_inv()
            l.append(bit)
            return l

class Real(Rational):
    """Classe Real. Herda de Rational. Implementa as pouquíssimas diferenças necessárias para utilizar apenas um valor tanto em entradas como em saídas"""
    def __init__(self, r=0):
        super(Real, self).__init__(r, 1.0)

    def __repr__(self):
        return super(Real, self).__str__()

    def __str__(self):
        return str(self.p/self.q)

def real_to_fraction(num, tol=10**-14):
    ## nope! pra conseguir calcular aproximações, vai precisar decidir
    ## uma distância do num, não do 1//2. Não sei ainda se dá mesmo pra adaptar
    return Rational.from_string( Real(num).to_string(tol=tol) )

if __name__ == '__main__':

    # # teste com a classe Rational
    # print('Rational:')
    # print('g_inv(8/11) =', Rational(8, 11).g_inv())
    # print('g([1, 1, 0, 0]) =', Rational().g([1, 1, 0, 0]))
    # print('---------------')
    #
    # # teste com a classe Rational
    # print('Rational:')
    # print('g_inv(5/7) =', Rational(5, 7).g_inv())
    # print('g([1, 0, 0]) =', Rational().g([1, 0, 0]))
    # print('---------------')
    #
    # print('Rational:')
    # print('g_inv(3/4) =', Rational(3, 4).g_inv())
    # print('g([0, 0]) =', Rational.from_list([0, 0]))
    # print('---------------')
    #
    # # teste com a classe Real
    # print('Real:')
    # print('g_inv(8/11) = ', Real(8/11).g_inv())
    # print('g([1, 1, 0, 0]) =', Real.from_list([1, 1, 0, 0]))
    # print('---------------')
    #
    # # teste da parte com métodos de classe
    # print('Real:')
    # num = 2**(0.5)/2
    # print(num)
    # g = Real(num).to_string()
    # print('to_string =', g)
    # print("and back? ", Real.from_string(g))
    # print('---------------')

    # aproximando frações

    # for tol in range(-4, 1):
    #     r = real_to_fraction(num, tol=10**tol)
    #     print(f'tol={tol}', r, r.p/r.q)

    from math import pi
    num = pi/5
    print(num)
    print('-------')
    real = Real(num)
    s = real.to_string(tol=10**-3)
    for i in range(20):
        rat = Rational.from_string(s[-i:])
        print(rat, rat.p/rat.q)
