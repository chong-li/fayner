##### Vamo ver se a gente consegue fazer um multiple dispatcher?
class MultipleDispatcher():
    namespace = {}

def dispatch(*argtypes):

    def function_adder(func):
        name = func.__name__

        func_namespace = MultipleDispatcher.namespace.get(name, {})
        func_namespace[argtypes] = func
        MultipleDispatcher.namespace[name] = func_namespace

        def dispatched(*func_args):

            type_sig = tuple([type(arg) for arg in func_args])
            f_namespace = MultipleDispatcher.namespace[name]
            if type_sig in f_namespace:
                f = MultipleDispatcher.namespace[name][type_sig]
            elif tuple() in f_namespace:
                f = MultipleDispatcher.namespace[name][tuple()]
            else:
                raise TypeError('No matching signature')

            return f(*func_args)

        return dispatched

    return function_adder


if __name__ == '__main__':
    @dispatch()
    def alpha(p, q):
        return q, 2*q-p

    @dispatch(float)
    def alpha(x):
        p, q = alpha(x, 1.)
        return (p/q, )

    @dispatch()
    def alpha_inv(p, q):
        return 2*p-q, p

    @dispatch(float)
    def alpha_inv(x):
        p, q = alpha_inv(x, 1.)
        return (p/q, )

    @dispatch()
    def beta(p, q):
        return q-p, 2*q-p

    @dispatch(float)
    def beta(x):
        p, q = beta(x, 1.)
        return (p/q, )

    @dispatch()
    def beta_inv(p, q):
        return q-2*p, q-p

    @dispatch(float)
    def beta_inv(x):
        p, q = beta_inv(x, 1.)
        return (p/q, )

    @dispatch(tuple)
    def distance(t):
        return t[0]-0.5 if len(t)==1 else 2*t[0]-t[1]

    def g(bits, return_type):
        x = (0.5, ) if return_type==float else (1, 2)

        for bit in bits:
            x = beta(*x) if bit else alpha(*x)

        return x if return_type!=float else x[0]

    @dispatch(tuple)
    def g_inv(num):
        rev_bin = []
        epsilon = 10**-15

        dist = distance(num)
        while abs(dist) > epsilon:

            if dist > 0:
                rev_bin.append(0)
                num = alpha_inv(*num)
            else:
                rev_bin.append(1)
                num = beta_inv(*num)

            dist = distance(num)

        return list(reversed(rev_bin))

    @dispatch(int, int)
    def g_inv(p, q):
        return g_inv((p, q))

    @dispatch(float)
    def g_inv(x):
        return g_inv((x, ))


    p, q = g([1,0,1], int)
    print(f'({p}, {q})', p/q)
    print(g([1,0,1], float))

    print(g_inv(2, 7))
    print(g_inv(2/7))
