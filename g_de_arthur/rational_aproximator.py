import math

class Rational():
    """Rational class. Implements base functions to compute rational aproximations by "successive differences" method, inspired by Matt Parker's Maths Puzzles #9

    Aproximations using this method are only decent using an unitary interval. Therefore it has been chosen to keep all fractions within the [0,1) interval and numbers have the form n+p/q where n is an integer, both p and q are (supposed) integers and 0 <= p < q

    If created by cls.from_string() it will also hold that gcd(p, q) == 1
    """

    def __init__(self, numerator=0, denominator=1):
        self.n = 0
        self.p = abs(numerator)
        self.q = abs(denominator)

        while self.p >= self.q:
            self.p -= self.q
            self.n += 1

        num_sign, den_sign = math.copysign(1, numerator), math.copysign(1, denominator)
        sign = math.copysign(1, num_sign*den_sign)
        if sign < 0:
            self.n = -1 -self.n
            self.p = self.q - self.p

    def __str__(self):
        return f'{self.n}+{self.p}/{self.q}'

    def flip(self):
        """flip: (1/2, 1) → (0, 1)
                    p / q → (2·p-q) / p

            `flip` represents the transformation occuring to the intermediate value on take-away tiangles when said value is closer to the maximum value then to the minimum one.
            Part of a piecewise function that, when composed with itself multiple times, (non-monotonically) aproximates values to 1/2. The other part of this function is `no_flip`.

            aka α_inverse"""

        self.p, self.q = (2*self.p - self.q), self.p
        return self

    def no_flip(self):
        """no_flip: (0, 1/2) → (0, 1)
                    p / q → (q-2·p) / (q-p)

            `no flip` represents the transformation occuring to the intermediate value on take-away tiangles when said value is closer to the minimum value then to the maximum one.
            Part of a piecewise function that, when composed with itself multiple times, (non-monotonically) aproximates values to 1/2. The other part of this function is `flip`.

            aka β_inverse"""

        self.p, self.q = (self.q - 2*self.p), self.q - self.p
        return self

    def flip_inv(self):
        """flip_inv: (0, 1) → (1/2, 1)
                    p / q → q / (2·q-p)

            flip_inv = 1 - no_flip_inv

            Inverse of flip function. In conjunction with `no_flip_inv` can be used to traverse the binary tree of values reacheable by successive aplications of `flip`and `no_flip` functions.

            aka α"""

        self.p, self.q = self.q, 2*self.q - self.p
        return self

    def no_flip_inv(self):
        """no_flip_inv: (0, 1) → (0, 1/2)
                p / q → (q-p) / (2·q-p)

            no_flip_inv = 1 - flip_inv

            Inverse of flip function. In conjunction with `flip_inv` can be used to traverse the  binary tree of values reacheable by successive aplications of `flip`and `no_flip` functions.

            aka β"""

        self.p, self.q = (self.q - self.p), 2*self.q - self.p
        return self

    @classmethod
    def from_string(cls, s):
        """Given a string f'{integer_part}.{binary_encoded_tree_path}' creates and returns an object of the class. That object will contain the given integer part and the fraction given by the traversal of the binary tree. 0 encodes no-flips branches and any other char encodes flip branches"""

        n, s = s.split('.')
        n = int(n)

        obj = cls()
        obj.n, obj.p, obj.q = n, 1, 2

        for bit in s:
            if bit == '0':
                obj.no_flip_inv()
            else:
                obj.flip_inv()

        return obj

    def to_string(self, max_len=2048):
        """Encodes the object in a string with format f'{integer_part}.{binary_encoding_of_flip_or_no_flip_applications}'
        `flips` are encoded as '1' and `no_flips` are encoded as 0

        Parameters
        ----------
        max_len: int, default=2048
            maximum numbers of chars contained in binary portion of output string. This equals the number of steps given in the direction of 1/2. Setting this too low may result in information loss and the aplication of cls.from_string(output_string) may not recreate the original object"""

        # list to record steps
        reversed_binary = []
        # precision to consider a number close enough to 1/2
        ϵ = 10**-15
        # creating new object to keeo it unaltered
        r = Rational(self.p, self.q)

        for _ in range(max_len):

            if 2*r.p > r.q:
                reversed_binary.append('1')
                r.flip()
            else:
                reversed_binary.append('0')
                r.no_flip()

            if abs(2*r.p - r.q) < ϵ:
                break

        binary = ''.join(reversed(reversed_binary))
        return '.'.join([str(self.n), binary])


class Real(Rational):
    """Real class. Subclass of Rational. Reals are basically identical to Rationals. The only differences are:
        1. String representation. Real and Rationals contain the same 3 atributes. Except, when printing a Real, those will be computed into 1 singular float value
        2. to_Rational method. A Real can be converted into a Rational. This will be an aproximation with tradeoffs and caveats"""

    def __init__(self, r=0):
        super(Real, self).__init__(r, 1)

    def __repr__(self):
        return super(Real, self).__str__()

    def __str__(self):
        return str(self.n + self.p/self.q)

    def is_precise(max_denominator, denominator):
        return max_denominator is None or max_denominator >= denominator

    def is_denominator_low(min_precision, distance):
        return min_precision is None or distance < min_precision

    def to_Rational(self, max_denominator=None, min_precision=None):
        """Creates and returns a Rational object. The Rational will have an aproximate value to self. Characteristics of the approximation can be controled by parameters

        Parameters
        ----------
        max_denominator: int, float or None, default=None
            Maximum value for the denominator on the output Rational. Should be positive if not None. Any float is functionally equivalent to its floor
        min_precision: float or None, default=None
            Minimum precision to return as valid approximatting Rational. Any float 0.0 or smaller forces None to be returned. If not None, will return the lowest denominator approximation found to be within given precision (max_denominator restriction still applies)

        Returns
        -------
        Rational or None:
            Returns the closest aproximation that satisfies restrictions or None if no such approximation was found"""

        ## The process is simply to produce a string representation of self
        ## Since this string format is shared, Rational.from_string() will return an aproximate Rational
        ## The rest of the code is to manage constraints
        ## Since approximations are non monotonic, we just have to pick the best given constraints

        s = self.to_string()
        val = self.n+self.p/self.q

        dists = []
        for i in range(1+s.find('.'), len(s)+1):

            r = Rational.from_string(s[:i])
            dist = abs(val - (r.n+r.p/r.q))

            if Real.is_precise(min_precision,dist) and Real.is_denominator_low(max_denominator,r.q):
                dists.append((dist, r))

        if dists==[]:
            return None
        else:
            key_func = lambda x: x[0] if min_precision is None else lambda x: x[1].q
            return sorted(dists, key=key_func)[0][1]

def test_Real_to_Rational(num, **kwargs):

    x = Real(num)
    print('original number:\n\t', num)
    print()

    rational = x.to_Rational(**kwargs)
    print('rational approximation:\n\t', rational)
    if rational is not None:
        print('distance:\n\t', abs(rational.n + rational.p/rational.q - num))
    print()

    s = x.to_string()
    rational = Rational.from_string(s)
    print('probably exact to machine precision:\n\t', rational)
    print('distance:\n\t', abs(rational.n + rational.p/rational.q - num))

    print('---------------------------------------')

if __name__ == '__main__':

    args = [
        (-1/3, {}),
        (1/3, {}),
        (math.pi, {}),
        (math.pi, {'min_precision': 0.001}),
        (math.pi, {'max_denominator': 500}),
        (math.pi, {'max_denominator': 500, 'min_precision': 0.001}),
        (math.pi-3, {}),
        (math.pi/4, {'max_denominator': 100}),
        (11**6/(13*math.pi), {})
    ]

    for num, kwargs in args:
        test_Real_to_Rational(num, **kwargs)
