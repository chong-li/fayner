class Matriz():
    """Matriz usando dicionários. Indexa elementos com duplas de inteiros. Emula comportamento de pandas.DataFrame.iloc"""
    def __init__(self, data):
        assert type(data)==list
        for row in data:
            assert type(row)==list
        self.data = {(r, c): element for r, row in enumerate(data) for c, element in enumerate(row)}

    def __getitem__(self, x):
        return self.data[x]

if __name__ == '__main__':
    dados = [
        [1, 2, 3],
        [4, 5, 6]
    ]

    m = Matriz(dados)
    print(m[1, 2])
